Sphinx C#
=========

`sphinx-cs` is a Sphinx extension that adds support for defined C# constructs such as classes, methods and fields. It can also generate documentation from the metadata files obtained by using the DocFX tool.