from setuptools import setup

setup(
    name='sphinx-cs',
    version='0.1.0',
    url='https://gitlab.com/alexjbinnie/sphinx-cs',
    author='Alex Jamieson-Binnie',
    author_email='alexjbinnie@gmail.com',
    description='Sphinx integration for C#',
    license='MIT',
    packages=['sphinxcs'],
    classifiers=[
        'Framework :: Sphinx :: Extension'
        'License :: OSI Approved :: MIT License'
    ],
    install_requires=[
        'antlr4-python3-runtime',
        'pyyaml'
    ],
)
