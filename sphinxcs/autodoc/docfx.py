# Copyright (c) 2020 Alex Jamieson-Binnie
# This library is licensed under MIT
import html
import re
import urllib
from typing import Dict, Any, Optional, Match, List

from sphinxcs.autodoc.output import RstOutputHelper
from sphinxcs.utils.signature_parser import parse_indexer_signature

summary_xref_re = re.compile(r'<xref href="([^"]*)" data-throw-if-not-resolved="false"></xref>')
summary_see_re = re.compile(r'<see cref="!:([^"]*)"></see>')
summary_code_re = re.compile(r'<pre><code>((?:.|\n)*?)</code></pre>')
summary_c_re = re.compile(r'<code(?:.*?)>(.*?)</code>')


class DocFXCSObject:
    """
    Representation of a single item in a DocFX Metadata file, which represents an element of a C#
    codebase such as a class, method or property.
    """

    docfx: Dict[str, Any] = None
    references: List[Any]
    autodoc: 'DocFXAutodoc'

    def __init__(self, autodoc: 'DocFXAutodoc', docfx_item: Dict[str, Any], docfx_references: List[Any]):
        self.docfx = docfx_item
        self.autodoc = autodoc
        self.references = docfx_references
        self.children = []
        self.parent = None

    @staticmethod
    def parse_summary(summary: str) -> str:
        """
        Parse the summary string, replacing any references with the sphinx role :cs:any:, which will be
        resolved to a cross reference within sphinx.

        :param summary: The raw summary string from the DocFX yaml file
        :return: A cleaned summary string with sphinx cross references inserted
        """

        def to_xref(match: Match[str]):
            return f':cs:any:`~{urllib.parse.unquote(match.group(1))}`'

        def to_code_block(match: Match[str]):
            str = '\n.. code-block:: csharp\n\n'
            for line in match.group(1).split('\n'):
                str += f'    {line}\n'
            return str

        def to_code_str(match: Match[str]):
            return f':code:`{match.group(1)}`'

        summary = summary.strip()
        summary = html.unescape(summary)
        summary = summary_xref_re.sub(to_xref, summary)
        summary = summary_see_re.sub(to_xref, summary)
        summary = summary_code_re.sub(to_code_block, summary)
        summary = summary_c_re.sub(to_code_str, summary)
        return summary

    @property
    def uid(self):
        """
        :return: The unique identifier for this DocFX object. This is unique for the project.
        """
        return self.docfx["uid"]

    @property
    def parent_uid(self) -> Optional[str]:
        """
        :return: The unique identifier of the parent of this item, or None if this item has no parent.
        """
        return self.docfx["parent"] if "parent" in self.docfx else None

    def add_child(self, obj: 'DocFXCSObject'):
        """
        Add a child item, so when this item is rendered to rst it will render its children as its contents.
        """
        self.children.append(obj)

    @property
    def rst_directive(self):
        """
        The sphinxcs directive that will be used to represent this object
        """
        return None

    @property
    def signature(self):
        """
        The signature used for the sphinxcs directive that represents this item. In most cases, the raw syntax
        provided in the DocFX metadata is used, as this is how the declaration for this item is written in
        the source file. In the case that this is not available (as is the case for namespaces), a signature is
        generated from the item's name preprended by any modifiers indicated in the 'modifiers.csharp' field of
        the metadata.
        """

        if 'syntax' in self.docfx and 'content' in self.docfx['syntax']:
            sig = str(self.docfx['syntax']['content'])
            sig = sig.replace("\n", " ")
        else:
            sig = str(self.docfx['name'])
            if 'modifiers.csharp' in self.docfx:
                for modifier in reversed(self.docfx['modifiers.csharp']):
                    if modifier != 'class' and modifier != 'interface':
                        sig = str(modifier) + ' ' + sig

        return sig

    def after_generate_directive(self, output: RstOutputHelper):
        """
        Callback after the directive is written to the RST file, but before any content is written
        """
        pass

    def after_summary(self, output: RstOutputHelper):
        """
        Callback after the summary has been written to the RST file, but before any children are written
        """
        if 'inheritance' in self.docfx and self.docfx['inheritance'][-1] != "System.Object":
            output.print(f":inherits: :cs:any:`~{self.docfx['inheritance'][-1]}`")
        if 'implements' in self.docfx:
            for implements in self.docfx['implements']:
                output.print(f":implements: :cs:any:`~{self.find_reference(implements)}`")

        if 'syntax' in self.docfx:
            if 'parameters' in self.docfx['syntax']:
                for parameter in self.docfx['syntax']['parameters']:
                    option = "param"
                    desc = ""
                    if 'type' in parameter:
                        option += f" {self.find_reference(parameter['type'])}"
                    if 'id' in parameter:
                        option += f" {parameter['id']}"
                    if 'description' in parameter:
                        desc = self.parse_summary(parameter['description']).replace('\n', ' ')
                    output.print(f":{option}: {desc}")
            if 'return' in self.docfx['syntax']:
                returns = self.docfx['syntax']['return']
                option = "returns"
                desc = ""
                if 'type' in returns:
                    option += f" {self.find_reference(returns['type'])}"
                if 'description' in returns:
                    desc = self.parse_summary(returns['description']).replace('\n', ' ')
                output.print(f":{option}: {desc}")

        if 'exceptions' in self.docfx:
            for exception in self.docfx['exceptions']:
                option = "throws"
                desc = ""
                if 'type' in exception:
                    option += f" {self.find_reference(exception['type'])}"
                if 'description' in exception:
                    desc = self.parse_summary(exception['description']).replace('\n', ' ')
                output.print(f":{option}: {desc}")

        output.print("")
        pass

    def generate_rst(self, output: RstOutputHelper):
        """
        Generate the RST output for this item. This will first print the directive that defines this object,
        followed by the comments provided in the metadata and then recursively generate the RST for each child.
        """

        # Indicate the parent namespace of the class, so it can be resolved correctly in sphinx
        if self.parent is None and self.parent_uid is not None:
            output.print(f".. cs:setscope:: {self.parent_uid}")
            output.print("")

        # Generate the sphinxcs directive
        output.print(f".. cs:{self.rst_directive}:: {self.signature}")

        self.after_generate_directive(output)

        output.indent()

        output.print("")

        # Generate the summary
        if 'summary' in self.docfx:
            summary = self.parse_summary(self.docfx['summary'])
            for line in summary.split('\n'):
                output.print(line)
            output.print("")

        if 'remarks' in self.docfx:
            output.print("\n")
            summary = self.parse_summary(self.docfx['remarks'])
            for line in summary.split('\n'):
                output.print(line)
            output.print("")

        if 'example' in self.docfx:
            output.print("\n")
            for example in self.docfx['example']:
                summary = self.parse_summary(example)
                for line in summary.split('\n'):
                    output.print(line)
                output.print("")

        self.after_summary(output)

        # Generate the RST for all children
        for child in self.children:
            child.generate_rst(output)

        output.outdent()

    def find_reference(self, uid: str):
        """
        Ensure that a uid exists in the references. May be used in the future to gather more information
        about a uid.
        """
        for reference in self.references:
            if reference['uid'] == uid:
                return uid
        raise ValueError(f"Can't resolve reference {uid}")


class DocFXCSType(DocFXCSObject):
    """
    Representation of a DocFX metadata item pertaining to a type (class, struct or interface)
    """

    pass


class DocFXCSClass(DocFXCSType):
    """
    Representation of a DocFX metadata item pertaining to a class
    """

    @property
    def rst_directive(self):
        return 'class'

    pass


class DocFXCSNamespace(DocFXCSObject):
    """
    Representation of a DocFX metadata item pertaining to a namespace
    """

    @property
    def rst_directive(self):
        return 'namespace'

    def after_generate_directive(self, output: RstOutputHelper):
        # Print Table of Contents for this namespace
        output.print("")
        output.print(".. toctree::")
        output.indent()
        output.print(":maxdepth: 4")
        output.print("")

        for child in self.docfx['children']:
            output.print(child.replace('`', '-'))

        output.outdent()

    pass


class DocFXCSProperty(DocFXCSObject):
    """
    Representation of a DocFX metadata item pertaining to a property
    """

    @property
    def rst_directive(self):
        # Check if the property is actually an indexer, to use the specific :cs:indexer: directive
        sig = self.docfx['syntax']['content']
        if 'this' not in sig:
            return 'property'
        try:
            parse_indexer_signature(sig)
            return 'indexer'
        except SyntaxError:
            return 'property'

    pass


class DocFXCSConstructor(DocFXCSObject):
    """
    Representation of a DocFX metadata item pertaining to a constructor
    """

    @property
    def rst_directive(self):
        return 'constructor'

    pass


class DocFXCSMethod(DocFXCSObject):
    """
    Representation of a DocFX metadata item pertaining to a method
    """

    @property
    def rst_directive(self):
        return 'method'

    pass


class DocFXCSInterface(DocFXCSType):
    """
    Representation of a DocFX metadata item pertaining to an interface
    """

    @property
    def rst_directive(self):
        return 'interface'

    pass


class DocFXCSStruct(DocFXCSType):
    """
    Representation of a DocFX metadata item pertaining to a struct
    """

    @property
    def rst_directive(self):
        return 'struct'

    pass


class DocFXCSEvent(DocFXCSObject):
    """
    Representation of a DocFX metadata item pertaining to an event
    """

    @property
    def rst_directive(self):
        return 'event'

    pass


class DocFXCSField(DocFXCSObject):
    """
    Representation of a DocFX metadata item pertaining to a field
    """

    @property
    def rst_directive(self):
        if isinstance(self.parent, DocFXCSEnum):
            return "enummember"
        return 'field'

    pass


class DocFXCSOperator(DocFXCSObject):
    """
    Representation of a DocFX metadata item pertaining to an operator overload
    """

    @property
    def rst_directive(self):
        if 'implicit' in self.docfx['syntax']['content'] or 'explicit' in self.docfx['syntax']['content']:
            return 'conversion'
        return 'operator'

    pass


class DocFXCSEnum(DocFXCSObject):
    """
    Representation of a DocFX metadata item pertaining to an enum
    """

    @property
    def rst_directive(self):
        return 'enum'

    pass


class DocFXCSDelegate(DocFXCSObject):
    """
    Representation of a DocFX metadata item pertaining to a delegate
    """

    @property
    def rst_directive(self):
        return 'delegate'

    pass


DOCFX_TYPE_TO_OBJECT = {
    "Class": DocFXCSClass,
    "Namespace": DocFXCSNamespace,
    "Property": DocFXCSProperty,
    "Constructor": DocFXCSConstructor,
    "Method": DocFXCSMethod,
    "Interface": DocFXCSInterface,
    "Event": DocFXCSEvent,
    "Struct": DocFXCSStruct,
    "Field": DocFXCSField,
    "Operator": DocFXCSOperator,
    "Enum": DocFXCSEnum,
    "Delegate": DocFXCSDelegate
}
