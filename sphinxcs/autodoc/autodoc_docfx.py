# Copyright (c) 2020 Alex Jamieson-Binnie
# This library is licensed under MIT

import os
from typing import Dict, Any, Callable, Hashable, List, Union

import yaml

from sphinxcs.autodoc.docfx import DocFXCSObject, DOCFX_TYPE_TO_OBJECT
from sphinxcs.autodoc.output import RstOutputHelper


class DocFXAutodoc:
    """
    Represents the context of a single execution of the autodoc command. Call run() to actually generate the files
    """
    namespaces = []
    source_folder: str
    output_folder: str
    project_name: str

    def __init__(self, project_name: str, source_folder: str, output_folder: str):
        self.project_name = project_name
        self.source_folder = source_folder
        self.output_folder = output_folder

    def run(self):
        """
        Search through the DocFX metadata files in source_folder and generate corresponding .rst files in
        output_folder that can be parsed by the sphinxcs extension.
        """

        src_toc_yml = os.path.join(self.source_folder, 'toc.yml')
        output_toc_rst = os.path.join(self.output_folder, 'toc.rst')

        self.parse_yml_file(src_toc_yml, output_toc_rst, self.parse_toc_yml)

        filenames = list(os.listdir(self.source_folder))

        for filename in filenames:
            if not filename.endswith(".yml"):
                continue

            if filename == 'toc.yml':
                continue

            filepath = os.path.join(self.source_folder, filename)
            output_filename = filename.replace(".yml", ".rst")
            output_filepath = os.path.join(self.output_folder, output_filename)
            print(filepath)
            self.parse_yml_file(filepath, output_filepath, self.parse_docfx_file)

    def parse_toc_yml(self, content: Union[Dict[Hashable, Any], List], output: RstOutputHelper):
        """
        Parse the content of the toc.yml metadata file to get the name of all namespaces
        """

        output.print(f"{self.project_name}")
        output.print("=" * len(self.project_name))
        output.print("")
        output.print(".. toctree::")
        output.indent()
        output.print(":maxdepth: 4")
        output.print("")

        for child in content:
            self.namespaces.append(child["uid"])

        for namespace in self.namespaces:
            output.print(namespace)

        output.outdent()

    def parse_yml_file(self, filepath: str, output_filepath: str,
                       parser: Callable[[Union[Dict[Hashable, Any], List], RstOutputHelper], None]):
        """
        Parse a DocFX yml file at filepath and generate a corresponding .rst file at output_filepath using
        the provided parser
        """

        with open(filepath, 'r') as file:
            filecontent = file.read()

        content = yaml.safe_load(filecontent)

        with open(output_filepath, "w+") as file:
            output = RstOutputHelper(lambda s: file.write(s + "\n"))

            parser(content, output)

    def parse_docfx_file(self, content: Union[Dict[Hashable, Any], List], output: RstOutputHelper):
        """
        Parse a DocFX yml metadata file representing an object such as a class or method
        """

        objects = {}

        for item in content['items']:
            obj = self._get_docfx_object(item, content['references'])
            objects[obj.uid] = obj

        for obj in objects.values():
            if obj.parent_uid in objects:
                obj.parent = objects[obj.parent_uid]
                obj.parent.add_child(obj)

        title = next(iter(objects.values())).docfx['name']
        output.print(title)
        output.print("=" * len(title))

        for obj in objects.values():
            if obj.parent is None:
                obj.generate_rst(output)

    def _get_docfx_object(self, docfx_item: Dict[str, Any], docfx_references: List[Any]) -> DocFXCSObject:
        """
        Convert the contents of a DocFX yml metadata file into a DocFXCSObject, based upon the type of the metadata.
        """

        if "type" not in docfx_item:
            raise ValueError(f"DocFX item {docfx_item['uid']} does not have type")

        if "langs" not in docfx_item or "csharp" not in docfx_item["langs"]:
            raise ValueError(f"DoxFX item {docfx_item['uid']} is not marked as C#")

        type: str = docfx_item["type"]

        if type not in DOCFX_TYPE_TO_OBJECT:
            raise ValueError(f"Unsupported DocFX type {type}")

        return DOCFX_TYPE_TO_OBJECT[type](self, docfx_item, docfx_references)

def autodoc_docfx(project_name: str, source_folder: str, output_folder: str):
    DocFXAutodoc(project_name=project_name, source_folder=source_folder,
                 output_folder=output_folder).run()
