# Copyright (c) 2020 Alex Jamieson-Binnie
# This library is licensed under MIT

from typing import Callable, Any


class RstOutputHelper:
    """
    Generic output that provides a print method for printing new lines to a file, as well as indent() and output()
    commands
    """

    level = 0

    _print: Callable[[str], None] = None

    def __init__(self, write):
        self._print = write

    def print(self, line: Any):
        """
        Print a single line to the output
        """
        self._print(' ' * (self.level * 4) + str(line))

    def indent(self):
        """
        Increase the indent level by 1 (corresponding to four spaces)
        """
        self.level = self.level + 1

    def outdent(self):
        """
        Decrease the indent level by 1 (corresponding to four spaces)
        """
        self.level = self.level - 1
