from docutils import nodes
from sphinx import addnodes
from sphinx.util.docfields import TypedField

from sphinxcs.roles.crossreference import CrossReference
from sphinxcs.utils.signature_parser import parse_xref_target
from sphinxcs.utils.types import CSharpGenericTypeParameter


class CsTypedField(TypedField):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def make_xref(self,
                  rolename,  # type: unicode
                  domain,  # type: unicode
                  target,  # type: unicode
                  innernode=addnodes.literal_emphasis,  # type: nodes.Node
                  contnode=None,  # type: nodes.Node
                  env=None,  # type: BuildEnvironment
                  ):
        # type: (...) -> nodes.Node
        if not rolename:
            return contnode or innernode(target, target)
        refnode = addnodes.pending_xref('', refdomain=domain, refexplicit=False,
                                        reftype=rolename, reftarget=target)

        cross_reference = parse_xref_target(target)

        if isinstance(cross_reference, CrossReference):
            title = cross_reference.get_display_name(False)
        elif isinstance(cross_reference, CSharpGenericTypeParameter):
            title = cross_reference.get_display_name(False)
        else:
            title = target

        refnode += contnode or nodes.literal(title, title, classes=['xref', 'cs', 'cs-any'])
        if env:
            env.domains[domain].process_field_xref(refnode)
        return refnode

    def make_field(self,
                   types,  # type: Dict[unicode, List[nodes.Node]]
                   domain,  # type: unicode
                   items,  # type: Tuple
                   env=None,  # type: BuildEnvironment
                   ):
        # type: (...) -> nodes.field
        def handle_item(fieldarg, content):
            # type: (unicode, unicode) -> nodes.paragraph
            par = nodes.paragraph()

            if fieldarg in types:
                # NOTE: using .pop() here to prevent a single type node to be
                # inserted twice into the doctree, which leads to
                # inconsistencies later when references are resolved
                fieldtype = types.pop(fieldarg)
                if len(fieldtype) == 1 and isinstance(fieldtype[0], nodes.Text):
                    typename = u''.join(n.astext() for n in fieldtype)
                    par.extend(self.make_xrefs(self.typerolename, domain, typename,
                                               addnodes.literal_emphasis, env=env))
                else:
                    par += fieldtype

            par += nodes.Text(' ')

            par.extend(self.make_xrefs(self.rolename, domain, fieldarg,
                                       addnodes.literal_strong, env=env))

            if content and len(content[0].children) > 0:
                par += nodes.Text(' -- ')
                par += content
            return par

        fieldname = nodes.field_name('', self.label)
        if len(items) == 1 and self.can_collapse:
            fieldarg, content = items[0]
            bodynode = handle_item(fieldarg, content)
        else:
            bodynode = self.list_type()
            for fieldarg, content in items:
                bodynode += nodes.list_item('', handle_item(fieldarg, content))
        fieldbody = nodes.field_body('', bodynode)
        return nodes.field('', fieldname, fieldbody)
