from sphinxcs.directive.signature.csobject import CSObject
from sphinxcs.utils.signature_parser import parse_indexer_signature


class CSIndexer(CSObject):
    """
    Sphinx directive for defining a signature of a C# indexer.
    """

    def handle_signature(self, sig, signode):
        signature = parse_indexer_signature(sig)

        self.create_signature(signode,
                              name='this[',
                              modifiers=signature.modifiers + ['delegate'],
                              type=signature.type.get_display_name(True),
                              parameters=[signature.parameter])

        signature.qualify(self.get_current_scope())

        return signature
