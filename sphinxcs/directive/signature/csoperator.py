from sphinxcs.directive.signature.csobject import CSObject
from sphinxcs.utils.signature_parser import parse_operator_signature


class CSOperator(CSObject):
    """
    Sphinx directive for defining a signature of a C# operator.
    """

    def handle_signature(self, sig, signode):
        signature = parse_operator_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True),
                              modifiers=signature.modifiers + ['operator'],
                              type=signature.type.get_display_name(True),
                              parameters=signature.parameters)

        signature.qualify(self.get_current_scope())

        return signature

    def should_index(self):
        return False
