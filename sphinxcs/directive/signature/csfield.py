from sphinxcs.directive.signature.csobject import CSObject
from sphinxcs.utils.signature_parser import parse_field_signature


class CSField(CSObject):
    """
    Sphinx directive for defining a signature of a C# field.
    """

    def handle_signature(self, sig, signode):
        signature = parse_field_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True),
                              modifiers=signature.modifiers,
                              type=signature.type.get_display_name(True))

        signature.qualify(self.get_current_scope())

        return signature
