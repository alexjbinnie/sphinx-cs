from sphinxcs.directive.signature.csobject import CSObject
from sphinxcs.utils.signature_parser import parse_delegate_signature


class CSDelegate(CSObject):
    """
    Sphinx directive for defining a signature of a C# delegate.
    """

    def handle_signature(self, sig, signode):
        signature = parse_delegate_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True),
                              modifiers=signature.modifiers + ['delegate'],
                              type=signature.type.get_display_name(True),
                              parameters=signature.parameters)

        signature.qualify(self.get_current_scope())

        return signature
