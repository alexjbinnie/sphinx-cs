from sphinxcs.directive.signature.csobject import CSObject
from sphinxcs.utils.signature_parser import parse_event_signature


class CSEvent(CSObject):
    """
    Sphinx directive for defining a signature of a C# event.
    """

    def handle_signature(self, sig, signode):
        signature = parse_event_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True),
                              modifiers=signature.modifiers + ['event'],
                              type=signature.type.get_display_name(True))

        signature.qualify(self.get_current_scope())

        return signature
