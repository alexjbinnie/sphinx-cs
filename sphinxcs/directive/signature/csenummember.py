from sphinxcs.directive.signature.csobject import CSObject
from sphinxcs.utils.signature_parser import parse_enummember_signature


class CSEnumMember(CSObject):
    """
    Sphinx directive for defining a signature of a C# enum member.
    """

    def handle_signature(self, sig, signode):
        signature = parse_enummember_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True))

        signature.qualify(self.get_current_scope())

        return signature
