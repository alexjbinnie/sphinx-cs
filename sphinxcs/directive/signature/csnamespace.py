from sphinxcs.directive.signature.csscope import CSScope
from sphinxcs.utils.signature_parser import parse_namespace_signature


class CSNamespace(CSScope):
    """
    Sphinx directive for defining a signature of a C# namespace.
    """

    def handle_signature(self, sig, signode):
        signature = parse_namespace_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True),
                              modifiers=['namespace'])

        signature.qualify(self.get_current_scope())

        return signature
