from typing import List, Optional

from docutils import nodes
from sphinx import addnodes
from sphinx.directives import ObjectDescription
from sphinx.util import docutils

from sphinxcs.directive.csgroupedfield import CsGroupedField
from sphinxcs.directive.cstypedfield import CsTypedField
from sphinxcs.directive.groupedlistfield import GroupedListField
from sphinxcs.utils.signatures import Signature
from sphinxcs.utils.types import CSharpParameter, CSharpType


def make_parameter_list_node(parameters: List[CSharpParameter]) -> docutils.nodes.Node:
    param_list = addnodes.desc_parameterlist()
    for param in parameters:
        param_list += addnodes.desc_parameter(str(param), str(param))
    return param_list


class CSObject(ObjectDescription):
    """
    Sphinx directive for defining a signature of a C# 'object', such as a class, method or property'.
    """

    def run(self):  # type: () -> List[nodes.Node]
        result = super().run()
        result[1]["objtype"] = "cs-" + result[1]["objtype"]
        return result

    def create_signature(self, signode, name: str = None, type: str = None, modifiers: List[str] = None,
                         parameters: List[CSharpParameter] = None, further: str = None):
        if modifiers:
            for modifier in modifiers:
                signode += addnodes.desc_annotation(f'{modifier} ', f'{modifier} ')

        if type:
            signode += addnodes.desc_type(f'{type} ', f'{type} ')

        if name:
            if type:
                signode += addnodes.desc_name(f' {name}', f' {name}')
            else:
                signode += addnodes.desc_name(f'{name}', f'{name}')

        if parameters:
            signode += make_parameter_list_node(parameters)

        if further:
            signode += addnodes.desc_addname(further)

    doc_field_types = [
        GroupedListField('inherits', label='Inherits',
                         names=('inherits', 'baseclass'),
                         can_collapse=True),
        GroupedListField('implements', label='Implements',
                         names=('implements', 'interface'),
                         can_collapse=True),
        CsTypedField('param', label='Parameters', names=('param', 'arg'), can_collapse=True, typerolename='any'),
        CsGroupedField('exceptions', label="Throws", names=('exception', 'throw', 'throws'), rolename='any',
                       can_collapse=True),
        CsGroupedField('returns', label="Returns", names=('return', 'returns'), rolename='any', can_collapse=True)

    ]

    option_spec = dict(ObjectDescription.option_spec)

    def should_index(self):
        """
        :return: Should this object be indexed and linkable?
        """
        return True

    def add_target_and_index(self, signature: Signature, sig, signode):
        if not self.should_index():
            return
        signode['ids'].append(signature.unique_name)
        if 'noindex' not in self.options:
            domain = self.env.domains[self.domain]
            domain.note_object(signature)

    def get_current_scope(self) -> Optional[CSharpType]:
        if 'cs:scope' in self.env.temp_data:
            return self.env.temp_data['cs:scope']
        else:
            return None
