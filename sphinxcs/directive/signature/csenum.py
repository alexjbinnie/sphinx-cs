from sphinxcs.directive.signature.csobject import CSObject
from sphinxcs.utils.signature_parser import parse_enum_signature


class CSEnum(CSObject):
    """
    Sphinx directive for defining a signature of a C# enum.
    """

    def handle_signature(self, sig, signode):
        signature = parse_enum_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True),
                              modifiers=signature.modifiers + ['enum'])

        signature.qualify(self.get_current_scope())

        return signature
