from sphinxcs.directive.signature.csscope import CSScope
from sphinxcs.utils.signature_parser import parse_interface_signature


class CSInterface(CSScope):
    """
    Sphinx directive for defining a signature of a C# interface.
    """

    def handle_signature(self, sig, signode):
        signature = parse_interface_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True),
                              modifiers=signature.modifiers + ['interface'])

        signature.qualify(self.get_current_scope())

        return signature
