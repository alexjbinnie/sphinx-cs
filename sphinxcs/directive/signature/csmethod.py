from sphinxcs.directive.signature.csobject import CSObject
from sphinxcs.utils.signature_parser import parse_method_signature


class CSMethod(CSObject):
    """
    Sphinx directive for defining a signature of a C# method.
    """

    def handle_signature(self, sig, signode):
        signature = parse_method_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True),
                              modifiers=signature.modifiers,
                              type=signature.type.get_display_name(True),
                              parameters=signature.parameters)

        signature.qualify(self.get_current_scope())

        return signature
