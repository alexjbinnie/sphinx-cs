from sphinxcs.directive.signature.csscope import CSScope
from sphinxcs.utils.signature_parser import parse_struct_signature


class CSStruct(CSScope):
    """
    Sphinx directive for defining a signature of a C# struct.
    """

    def handle_signature(self, sig, signode):
        signature = parse_struct_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True),
                              modifiers=signature.modifiers + ['struct'])

        signature.qualify(self.get_current_scope())

        return signature
