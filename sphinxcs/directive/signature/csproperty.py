from sphinxcs.directive.signature.csobject import CSObject
from sphinxcs.utils.signature_parser import parse_property_signature


class CSProperty(CSObject):
    """
    Sphinx directive for defining a signature of a C# property.
    """

    def handle_signature(self, sig, signode):
        signature = parse_property_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True),
                              modifiers=signature.modifiers + ['delegate'],
                              type=signature.type.get_display_name(True),
                              further=f' {signature.accessor}')

        signature.qualify(self.get_current_scope())

        return signature
