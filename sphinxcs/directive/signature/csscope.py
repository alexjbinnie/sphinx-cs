from typing import List

from sphinx import addnodes
from sphinx.util.docfields import DocFieldTransformer

from sphinxcs.directive.signature.csobject import CSObject
from sphinxcs.utils.signatures import Signature
from sphinxcs.utils.types import CSharpType, QualifiedCSharpType


class CSScope(CSObject):
    """
    Sphinx directive for a signature which appends its typename to the scope for all its contents.
    """

    def run(self):
        # Code copied
        if ':' in self.name:
            self.domain, self.objtype = self.name.split(':', 1)
        else:
            self.domain, self.objtype = '', self.name
        self.indexnode = addnodes.index(entries=[])

        node = addnodes.desc()
        node.document = self.state.document
        node['domain'] = self.domain
        # 'desctype' is a backwards compatible attribute
        node['objtype'] = node['desctype'] = self.objtype
        node['noindex'] = noindex = ('noindex' in self.options)

        self.names = []  # type: List[unicode]
        signatures = self.get_signatures()

        if len(signatures) > 1:
            raise SyntaxError("C# declarations with scope can only have a single signature")

        sig = signatures[0]

        # add a signature node for each signature in the current unit
        # and add a reference target for it
        signode = addnodes.desc_signature(sig, '')
        signode['first'] = False
        node.append(signode)
        try:
            # name can also be a tuple, e.g. (classname, objname);
            # this is strictly domain-specific (i.e. no assumptions may
            # be made in this base class)
            name = self.handle_signature(sig, signode)
        except ValueError:
            # signature parsing failed
            signode.clear()
            signode += addnodes.desc_name(sig, sig)
            name = None

        if name:
            self.names = [name]
            if not noindex:
                # only add target and index entry if this is the first
                # description of the object with this name in this desc block
                self.add_target_and_index(name, sig, signode)

        contentnode = addnodes.desc_content()
        node.append(contentnode)
        if self.names:
            # needed for association of version{added,changed} directives
            self.env.temp_data['object'] = self.names[0]
        self.before_contents(name)
        self.state.nested_parse(self.content, self.content_offset, contentnode)
        DocFieldTransformer(self).transform_all(contentnode)
        self.env.temp_data['object'] = None
        self.after_contents(name)
        return [self.indexnode, node]

    def before_contents(self, name: Signature):
        self.push_scope(name.raw_type_name)
        super().before_content()

    def after_contents(self, name: Signature):
        self.pop_scope(name.raw_type_name)
        super().after_content()

    def push_scope(self, scope: CSharpType):
        current_scope = self.get_current_scope()
        if current_scope is None:
            current_scope = scope
        else:
            current_scope = QualifiedCSharpType.concatenate(current_scope, scope)
        self.env.temp_data['cs:scope'] = current_scope

    def pop_scope(self, scope: CSharpType):
        current_scope = self.env.temp_data['cs:scope']
        if current_scope is None:
            current_scope = scope
        else:
            current_scope = QualifiedCSharpType.strip(current_scope, scope)
        self.env.temp_data['cs:scope'] = current_scope
