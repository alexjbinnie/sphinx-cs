from sphinxcs.directive.signature.csobject import CSObject
from sphinxcs.utils.signature_parser import parse_constructor_signature


class CSConstructor(CSObject):
    """
    Sphinx directive for defining a signature of a C# constructor.
    """

    def handle_signature(self, sig, signode):
        signature = parse_constructor_signature(sig)

        self.create_signature(signode,
                              name=signature.type_name.get_display_name(True),
                              modifiers=signature.modifiers,
                              parameters=signature.parameters)

        signature.qualify(self.get_current_scope())

        return signature
