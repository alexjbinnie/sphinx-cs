from docutils import nodes
from sphinx import addnodes
from sphinx.util.docfields import GroupedField

from sphinxcs.roles.crossreference import CrossReference
from sphinxcs.utils.signature_parser import parse_xref_target
from sphinxcs.utils.types import CSharpGenericTypeParameter


class CsGroupedField(GroupedField):

    def make_xref(self,
                  rolename,  # type: unicode
                  domain,  # type: unicode
                  target,  # type: unicode
                  innernode=addnodes.literal_emphasis,  # type: nodes.Node
                  contnode=None,  # type: nodes.Node
                  env=None,  # type: BuildEnvironment
                  ):
        # type: (...) -> nodes.Node
        if not rolename:
            return contnode or innernode(target, target)
        refnode = addnodes.pending_xref('', refdomain=domain, refexplicit=False,
                                        reftype=rolename, reftarget=target)

        cross_reference = parse_xref_target(target)

        if isinstance(cross_reference, CrossReference):
            title = cross_reference.get_display_name(False)
        elif isinstance(cross_reference, CSharpGenericTypeParameter):
            title = cross_reference.get_display_name(False)
        else:
            title = target

        refnode += contnode or nodes.literal(title, title, classes=['xref', 'cs', 'cs-any'])
        if env:
            env.domains[domain].process_field_xref(refnode)
        return refnode

    def make_field(self,
                   types,  # type: Dict[unicode, List[nodes.Node]]
                   domain,  # type: unicode
                   items,  # type: Tuple
                   env=None,  # type: BuildEnvironment
                   ):
        # type: (...) -> nodes.field
        fieldname = nodes.field_name('', self.label)
        listnode = self.list_type()
        for fieldarg, content in items:
            par = nodes.paragraph()
            par.extend(self.make_xrefs(self.rolename, domain, fieldarg,
                                       addnodes.literal_strong, env=env))
            if content and len(content[0].children) > 0:
                par += nodes.Text(' -- ')
                par += content
            listnode += nodes.list_item('', par)

        if len(items) == 1 and self.can_collapse:
            fieldbody = nodes.field_body('', listnode[0][0])
            return nodes.field('', fieldname, fieldbody)

        fieldbody = nodes.field_body('', listnode)
        return nodes.field('', fieldname, fieldbody)
