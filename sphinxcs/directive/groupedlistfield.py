from docutils import nodes
from sphinx import addnodes
from sphinx.util.docfields import Field


class GroupedListField(Field):
    """
    A doc field that is grouped; i.e., all fields of that type will be
    transformed into one field with its body being a bulleted list.  It always
    has an argument.  The argument can be linked using the given *rolename*.
    GroupedField should be used for doc fields that can occur more than once.
    If *can_collapse* is true, this field will revert to a Field if only used
    once.

    Example::

       :raises ErrorClass: description when it is raised
    """
    is_grouped = True
    list_type = nodes.bullet_list

    def __init__(self, name, names=(), label=None, rolename=None,
                 can_collapse=False):
        Field.__init__(self, name, names, label, False, rolename)
        self.can_collapse = can_collapse

    def make_field(self,
                   types,  # type: Dict[unicode, List[nodes.Node]]
                   domain,  # type: unicode
                   items,  # type: Tuple
                   env=None,  # type: BuildEnvironment
                   ):
        # type: (...) -> nodes.field
        fieldname = nodes.field_name('', self.label)
        listnode = self.list_type()
        for fieldarg, content in items:
            par = nodes.paragraph()
            par.extend(self.make_xrefs(self.rolename, domain, fieldarg,
                                       addnodes.literal_strong, env=env))
            par += content
            listnode += nodes.list_item('', par)

        if len(items) == 1 and self.can_collapse:
            fieldbody = nodes.field_body('', listnode[0][0])
            return nodes.field('', fieldname, fieldbody)

        fieldbody = nodes.field_body('', listnode)
        return nodes.field('', fieldname, fieldbody)
