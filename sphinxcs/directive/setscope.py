from docutils.parsers.rst import Directive

from sphinxcs.utils.signature_parser import parse_namespace_signature


class SetScope(Directive):
    """
    Directive to set the current scope of the following commands. This both qualifies any declarations that
    follow and helps inform linking.

    Therefore, the following generates a class with the fully qualified name of MyScope.MyClass

    .. setscope:: MyScope

    .. class:: public void MyScope

    """

    has_content = False
    required_arguments = 1

    def run(self):
        env = self.state.document.settings.env

        scope = parse_namespace_signature(self.arguments[0]).raw_type_name
        env.temp_data['cs:scope'] = scope

        return []
