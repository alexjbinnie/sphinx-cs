# Copyright (c) 2020 Alex Jamieson-Binnie
# This library is licensed under MIT

from antlr4 import ParserRuleContext

from sphinxcs.antlr.CSharpParser import CSharpParser
from sphinxcs.utils.types import *


def parse_modifiers(modifiers: CSharpParser.ModifiersContext) -> List[str]:
    """
    Convert a `modifiers` rule into a set of modifier names
    """

    mods = []
    for mod in modifiers.modifier():
        mods.append(mod.getText())
    return mods


def parse_type(context: CSharpParser.Type_Context) -> CSharpType:
    """
    Convert a `type_` rule into a CSharpType
    """

    if context.type_tuple():
        typename = parse_tuple(context.type_tuple())
    else:
        typename = parse_typename(context.type_name())

    if context.type_nullable_decorator():
        typename = NullableType(typename)

    if context.type_array_decorator():
        typename = ArrayType(typename)

    return typename


def parse_parameter(context: CSharpParser.ParameterContext) -> CSharpParameter:
    """
    Convert a `parameter` rule into a CSharpParameter
    """

    return CSharpParameter(context.parameter_name().getText(), parse_type(context.type_()))


def parse_parameter_list(context: CSharpParser.Parameter_listContext) -> List[CSharpParameter]:
    """
    Convert a `parameter_list` rule into a list of CSharpParameter
    """

    parameters = []
    for param in context.parameter():
        parameters.append(parse_parameter(param))
    return parameters


def parse_typename(context: CSharpParser.Type_nameContext) -> CSharpType:
    """
    Convert a `type_name` rule into a CSharpType
    """

    name_parts = context.unqualified_type()
    if len(name_parts) == 1:
        return parse_unqualified_type(name_parts[0])
    return QualifiedCSharpType([parse_unqualified_type(part) for part in name_parts])


def parse_class_base(context: CSharpParser.Class_baseContext) -> List[CSharpType]:
    """
    Convert a `class_base` rule into a list of CSharpType
    """

    inherits = []
    for t in context.type_():
        inherits.append(parse_type(t))
    return inherits


def parse_unqualified_type(context: CSharpParser.Unqualified_typeContext) -> UnqualifiedCSharpType:
    """
    Convert a `unqualified_type` rule into a UnqualifiedCSharpType
    """

    type_name = context.unqualified_type_name().getText()
    generic_args = []
    if context.type_parameter_list():
        generic_args = parse_type_parameter_list(context.type_parameter_list())
    return UnqualifiedCSharpType(type_name, generic_args)


def get_attributes(attributes: CSharpParser.AttributesContext) -> List[str]:
    """
    Convert a `attributes` rule into a list of attribute names
    """

    attr = []
    for mod in attributes.attribute():
        attr.append(mod.getText())
    return attr


def parse_type_parameter_list(context: CSharpParser.Type_parameter_listContext) -> List[CSharpType]:
    """
    Convert a `type_parameter_list` rule into a list of CSharpType
    """

    params = []
    for type_param in context.type_parameter():
        params.append(parse_type(type_param.type_()))
    return params


def parse_tuple(context: CSharpParser.Type_tupleContext) -> CSharpTupleType:
    """
    Convert a `type_tuple` rule into a TupleCSharpType
    """

    params = []
    for cstype in context.type_():
        params.append(parse_type(cstype))
    return CSharpTupleType(params)


class Signature(ABC):
    """
    A C# signature that defines something such as a class
    """

    _context: ParserRuleContext
    type_name: Optional[CSharpType]  # The type including the local scope
    raw_type_name: Optional[CSharpType]  # The type as written in the signature

    def __init__(self, context: ParserRuleContext):
        self._context = context
        try:
            type_name = parse_typename(self._context.type_name())
        except:
            type_name = None
        self._set_type_name(type_name)
        super().__init__()
        self._context = None

    def _set_type_name(self, _type: CSharpType):
        """
        Sets both the type_name and raw_type_name. Only to be used for signatures which alter the type name
        at construction
        """
        self.type_name = _type
        self.raw_type_name = _type

    @property
    @abstractmethod
    def objtype(self):
        """
        :return: An objtype string used when indexing in sphinxcs. Should normally be the corresponding directive name.
        """
        pass

    @property
    def unique_name(self):
        """
        :return: An unique name that defines this signature, including fully qualified name
        """
        return self.type_name.get_linkable_name()

    def qualify(self, scope: Optional[CSharpType]):
        if not scope:
            return

        self.type_name = QualifiedCSharpType.concatenate(scope, self.type_name)


class SignatureModifiers:
    """
    A mixin for a class derived from Signature, that parses a `modifiers` rule present in the signature
    """

    modifiers: List[str]

    def __init__(self):
        if self._context.modifiers():
            self.modifiers = parse_modifiers(self._context.modifiers())
        else:
            self.modifiers = []
        super().__init__()


class SignatureAttributes:
    """
    A mixin for a class derived from Signature, that parses a `attributes` rule present in the signature
    """

    attributes: List[str]

    def __init__(self):
        if self._context.attributes():
            self.attributes = get_attributes(self._context.attributes())
        else:
            self.attributes = []
        super().__init__()


class SignatureType:
    """
    A mixin for a class derived from Signature, that parses a `type_` rule present in the signature
    """

    type: CSharpType

    def __init__(self):
        self.type = parse_type(self._context.type_())
        super().__init__()


class SignatureInheritance:
    """
    A mixin for a class derived from Signature, that parses a `inherits` rule present in the signature
    """

    inherits: List[CSharpType]

    def __init__(self):
        if self._context.class_base():
            self.inherits = parse_class_base(self._context.class_base())
        else:
            self.inherits = []
        super().__init__()


class SignatureDefaultValue:
    """
    A mixin for a class derived from Signature, that parses a `literal` rule present in the signature
    """

    default_value: str

    def __init__(self):
        self.default_value = self._context.literal().getText()
        super().__init__()


class SignatureParameter:
    """
    A mixin for a class derived from Signature, that parses a `parameter` rule present in the signature
    """

    parameter: CSharpParameter

    def __init__(self):
        self.parameter = parse_parameter(self._context.parameter())
        super().__init__()


class SignatureParameterList:
    """
    A mixin for a class derived from Signature, that parses a `parameter_list` rule present in the signature
    """

    parameters: List[CSharpParameter]

    def __init__(self):
        if self._context.parameter_list():
            self.parameters = parse_parameter_list(self._context.parameter_list())
        else:
            self.parameters = []

        super().__init__()


class TypeSignature(Signature, SignatureAttributes, SignatureModifiers, SignatureInheritance):
    """
    A signature of a C# type, such as a class or interface.
    """

    pass


class ClassSignature(TypeSignature):
    """
    A signature of a C# class.
    """

    def __init__(self, context: CSharpParser.Class_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "class"


class InterfaceSignature(TypeSignature):
    """
    A signature of a C# interface.
    """

    def __init__(self, context: CSharpParser.Interface_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "interface"


class StructSignature(TypeSignature):
    """
    A signature of a C# struct.
    """

    def __init__(self, context: CSharpParser.Struct_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "struct"


class EnumSignature(Signature, SignatureAttributes, SignatureModifiers):
    """
    A signature of a C# enum.
    """

    def __init__(self, context: CSharpParser.Struct_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "enum"


class NamespaceSignature(Signature, SignatureAttributes):
    """
    A signature of a C# namespace.
    """

    def __init__(self, context: CSharpParser.Struct_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "namespace"


class EventSignature(Signature, SignatureAttributes, SignatureModifiers, SignatureType):
    """
    A signature of a C# event.
    """

    def __init__(self, context: CSharpParser.Struct_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "event"


class PropertySignature(Signature, SignatureAttributes, SignatureModifiers, SignatureType):
    """
    A signature of a C# property.
    """

    accessor: str

    def __init__(self, context: CSharpParser.Property_signatureContext):
        super().__init__(context)
        self.accessor = '{ ' + context.accessor_declarations().getText() + ' }'

    @property
    def objtype(self):
        return "property"


class FieldSignature(Signature, SignatureAttributes, SignatureModifiers, SignatureType):
    """
    A signature of a C# field.
    """

    def __init__(self, context: CSharpParser.Struct_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "field"


class EnumMemberSignature(Signature, SignatureAttributes, SignatureDefaultValue):
    """
    A signature of a C# enum member.
    """

    def __init__(self, context: CSharpParser.Struct_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "enummember"


class MethodSignature(Signature, SignatureAttributes, SignatureModifiers, SignatureType,
                      SignatureParameterList):
    """
    A signature of a C# method.
    """

    def __init__(self, context: CSharpParser.Struct_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "method"

    @property
    def unique_name(self):
        return self.type_name.get_linkable_name() + '(' + ', '.join(
            ([t.type.get_linkable_name() for t in self.parameters])) + ')'


class ConstructorSignature(Signature, SignatureAttributes, SignatureModifiers,
                           SignatureParameterList):
    """
    A signature of a C# constructor.
    """

    def __init__(self, context: CSharpParser.Struct_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "constructor"

    @property
    def unique_name(self):
        return self.type_name.get_linkable_name() + '(' + ', '.join(
            ([t.type.get_linkable_name() for t in self.parameters])) + ')'


class DelegateSignature(Signature, SignatureAttributes, SignatureModifiers, SignatureType,
                        SignatureParameterList):
    """
    A signature of a C# delegate.
    """

    def __init__(self, context: CSharpParser.Struct_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "delegate"

    @property
    def unique_name(self):
        return self.type_name.get_linkable_name() + '(' + ', '.join(
            ([t.type.get_linkable_name() for t in self.parameters])) + ')'


class IndexerSignature(Signature, SignatureAttributes, SignatureModifiers, SignatureType, SignatureParameter):
    """
    A signature of a C# indexer.
    """

    def __init__(self, context: CSharpParser.Struct_signatureContext):
        super().__init__(context)

    @property
    def objtype(self):
        return "indexer"

    @property
    def unique_name(self):
        return self.type_name.get_linkable_name() + f"[{self.parameter.type.get_linkable_name()}]"


class OperatorSignature(Signature, SignatureAttributes, SignatureModifiers, SignatureType, SignatureParameterList):
    """
    A signature of a C# operator overload.
    """

    def __init__(self, context: CSharpParser.Operator_signatureContext):
        super().__init__(context)
        operator_name = context.operator_name().getText()
        self._set_type_name(UnqualifiedCSharpType(operator_name, []))

    @property
    def objtype(self):
        return "operator"

    @property
    def unique_name(self):
        return self.type_name.get_linkable_name() + '(' + ', '.join(
            ([t.type.get_linkable_name() for t in self.parameters])) + ')'


class ConversionSignature(Signature, SignatureAttributes, SignatureModifiers, SignatureType, SignatureParameter):
    """
    A signature of a C# conversion.
    """

    def __init__(self, context: CSharpParser.Operator_signatureContext):
        super().__init__(context)
        self._set_type_name(self.type)

    @property
    def objtype(self):
        return "operator"

    @property
    def unique_name(self):
        return self.type_name.get_linkable_name() + f"({self.parameter.type.get_linkable_name()})"
