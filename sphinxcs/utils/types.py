# Copyright (c) 2020 Alex Jamieson-Binnie
# This library is licensed under MIT

from abc import ABC, abstractmethod
from typing import List, Optional, Union


class CSharpType(ABC):
    """
    A base class for a C# type name, such as MyClass, MyOtherClass<TParam>.MyInnerClass or (MyTupleItem, MyNullableItem?)
    """

    def __str__(self):
        return self.get_display_name(True)

    @abstractmethod
    def get_display_name(self, qualified: bool) -> str:
        """
        :param bool qualified: Should the name be fully qualified (ABC.DEF<GHI.JKL>) or unqualified (DEF<JKL>)
        :return: Get the display name of this type, with qualified indicating if the whole name should be used
        """
        pass

    @abstractmethod
    def get_linkable_name(self, generic_seperator: str = '`'):
        """
        :return: The name that a document on MSDN would have
        """

    pass


class NullableType(CSharpType):
    """
    A nullable C# type, which wraps an inner type like InnerType?. Nullable types do not have uid's.
    """

    def get_linkable_name(self, generic_seperator: str = '`'):
        return self._type.get_linkable_name(generic_seperator)

    def get_display_name(self, qualified: bool):
        return self._type.get_display_name(qualified) + '?'

    _type: CSharpType

    def __init__(self, cstype: CSharpType):
        self._type = cstype


class ArrayType(CSharpType):
    """
    An array C# type, which wraps an inner type like InnerType[]. Nullable types do not have uid's.
    """

    def get_linkable_name(self, generic_seperator: str = '`'):
        return self._type.get_linkable_name(generic_seperator)

    def get_display_name(self, qualified: bool):
        return self._type.get_display_name(qualified) + '[]'

    _type: CSharpType

    def __init__(self, cstype: CSharpType):
        self._type = cstype


class UnqualifiedCSharpType(CSharpType):
    """
    An unqualified type names with possible generic arguments.
    """

    def get_display_name(self, qualified: bool):
        name = self._typename
        if self.has_explicit_generic_args:
            name += '<' + ', '.join([t.get_display_name(qualified) for t in self._generic_args]) + '>'
        return name

    def get_linkable_name(self, generic_seperator: str = '`'):
        name = self._typename
        if self.generic_arguments_count > 0:
            name += generic_seperator + str(self.generic_arguments_count)
        return name

    _typename: str
    _generic_args: Union[List[CSharpType], int]

    def __init__(self, typename: str, generic_args: Union[int, List[CSharpType]]):
        self._typename = typename
        self._generic_args = generic_args

    @property
    def type_identifier(self) -> str:
        """
        :return: The string name of this type, ignoring generics
        """
        return self._typename

    @property
    def has_explicit_generic_args(self) -> bool:
        """
        :return: Does this type has explicit generic arguments
        """
        return isinstance(self._generic_args, List) and self.generic_arguments_count > 0

    @property
    def generic_arguments(self) -> List[CSharpType]:
        """
        :return: Does this type has explicit generic arguments
        """
        return self._generic_args

    @property
    def generic_arguments_count(self) -> int:
        """
        :return: The number of generic arguments this type has
        """
        if isinstance(self._generic_args, List):
            return len(self._generic_args)
        return self._generic_args

    @classmethod
    def are_equal_ignoring_generics(cls, a: 'UnqualifiedCSharpType', b: 'UnqualifiedCSharpType') -> bool:
        if a.type_identifier != b.type_identifier:
            return False
        if a.generic_arguments_count != b.generic_arguments_count:
            return False
        return True


class CSharpTupleType(CSharpType):
    """
    A tuple C# type, such as (Type1, Type2, ...). Tuple types do not have uid's.
    """

    def get_linkable_name(self, generic_seperator: str = '`'):
        raise SyntaxError("Can't generate link name for Tuple Type")

    def get_display_name(self, qualified: bool):
        return '(' + ', '.join(t.get_display_name(qualified) for t in self._types) + ')'

    _types: List[CSharpType]

    def __init__(self, types: List[CSharpType]):
        self._types = list(types)


class CSharpGenericTypeParameter(CSharpType):
    """
    A type parameter such as TParam, which although a possible argument for a type, is not really one
    """

    def get_linkable_name(self, generic_seperator: str = '`'):
        raise ValueError("Cannot get linkable name for Generic Type Parameter")

    def get_display_name(self, qualified: bool):
        return self._name

    _name: str

    def __init__(self, name: str):
        self._name = name


class QualifiedCSharpType(CSharpType):
    """
    A qualified C# type, such as Type1.Type2...
    """

    def get_linkable_name(self, generic_seperator: str = '`'):
        return '.'.join([t.get_linkable_name(generic_seperator) for t in self._types])

    def get_display_name(self, qualified: bool):
        if qualified:
            return '.'.join(t.get_display_name(True) for t in self._types)
        else:
            return self.unqualified_type.get_display_name(False)

    _types: List[UnqualifiedCSharpType]

    def __init__(self, types: List[UnqualifiedCSharpType]):
        self._types = list(types)

    def __len__(self):
        return len(self._types)

    def __getitem__(self, item):
        return self._types[item]

    @property
    def unqualified_type(self) -> UnqualifiedCSharpType:
        return self[-1]

    @classmethod
    def concatenate(cls, first: Optional[CSharpType], second: Optional[CSharpType]) -> CSharpType:
        if first is None:
            return second

        if second is None:
            return first

        types = []
        if isinstance(first, QualifiedCSharpType):
            for t in first._types:
                types.append(t)
        else:
            types.append(first)
        if isinstance(second, QualifiedCSharpType):
            for t in second._types:
                types.append(t)
        else:
            types.append(second)
        qtype = QualifiedCSharpType(types)
        return qtype

    @classmethod
    def strip(cls, current: Optional[CSharpType], to_remove: Optional[CSharpType]) -> Optional[CSharpType]:
        if to_remove is None:
            return current

        if current is None:
            raise SyntaxError(f"Cannot remove scope {to_remove} from None")

        current_types = []
        to_remove_types = []

        if isinstance(current, QualifiedCSharpType):
            for type in current._types:
                current_types.append(type)
        else:
            current_types.append(current)

        if isinstance(to_remove, QualifiedCSharpType):
            for type in to_remove._types:
                to_remove_types.append(type)
        else:
            to_remove_types.append(to_remove)

        if len(to_remove_types) > len(current_types):
            raise SyntaxError(f"Cannot remove scope {to_remove} from {current}")

        for i in range(1, 1 + len(to_remove_types)):
            a = current_types[-i]
            b = to_remove_types[-i]
            if str(a.get_display_name(True)) != str(b.get_display_name(True)):
                raise SyntaxError(f"Cannot remove scope {to_remove} from {current}")

        if len(to_remove_types) == len(current_types):
            return None

        if len(current_types) - len(to_remove_types) == 1:
            return current_types[0]

        return QualifiedCSharpType(current_types[0: len(current_types) - len(to_remove_types)])


class CSharpParameter:
    """
    A C# parameter, which consists of a type and a name
    """

    _type: CSharpType
    _name: str

    @property
    def type(self) -> CSharpType:
        """
        :return: The type of this parameter
        """
        return self._type

    @property
    def name(self) -> str:
        """
        :return: The name of this parameter
        """
        return self._name

    def __init__(self, name: str, type: CSharpType):
        self._type = type
        self._name = name

    def __str__(self):
        return str(self.type) + ' ' + self.name
