import sys

import antlr4
from antlr4.error.ErrorListener import ErrorListener

from sphinxcs.antlr.CSharpLexer import CSharpLexer, CommonTokenStream
from sphinxcs.antlr.CSharpParser import CSharpParser
from sphinxcs.roles.crossreference import parseCrossReference, CrossReference
from sphinxcs.utils.signatures import ClassSignature, InterfaceSignature, StructSignature, EventSignature, \
    FieldSignature, ConstructorSignature, MethodSignature, EnumMemberSignature, DelegateSignature, NamespaceSignature, \
    EnumSignature, IndexerSignature, PropertySignature, ConversionSignature, OperatorSignature, Union
from sphinxcs.utils.types import CSharpGenericTypeParameter


class CSharpErrorListener(ErrorListener):
    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        raise SyntaxError(msg)


def _parse(method, signature: str):
    input = antlr4.InputStream(signature)
    lexer = CSharpLexer(input, sys.stdout)  # call your lexer
    stream = CommonTokenStream(lexer)
    parser = CSharpParser(stream, sys.stdout)
    parser.addErrorListener(CSharpErrorListener())
    try:
        tree = method(parser)
    except SyntaxError as e:
        print(f"Failed to parse signature: {signature} : with : {method}")
        raise SyntaxError(f"Failed to parse signature: {signature} : with : {method}")
    return tree


def parse_xref_target(target: str) -> Union[CrossReference, CSharpGenericTypeParameter]:
    """
    Parse an xref target
    """
    context = _parse(CSharpParser.object_cross_reference, target.lstrip('~'))
    if context.object_reference():
        return parseCrossReference(context.object_reference())
    return CSharpGenericTypeParameter(context.type_param_reference().unqualified_type_name().getText())


def parse_class_signature(signature: str) -> ClassSignature:
    """
    Parse a C# class signature
    """
    return ClassSignature(_parse(CSharpParser.class_signature, signature))


def parse_interface_signature(signature: str) -> InterfaceSignature:
    """
    Parse a C# interface signature
    """
    return InterfaceSignature(_parse(CSharpParser.interface_signature, signature))


def parse_struct_signature(signature: str) -> StructSignature:
    """
    Parse a C# struct signature
    """
    return StructSignature(_parse(CSharpParser.struct_signature, signature))


def parse_event_signature(signature: str) -> EventSignature:
    """
    Parse a C# event signature
    """
    return EventSignature(_parse(CSharpParser.event_signature, signature))


def parse_field_signature(signature: str) -> FieldSignature:
    """
    Parse a C# field signature
    """
    return FieldSignature(_parse(CSharpParser.field_signature, signature))


def parse_constructor_signature(signature: str) -> ConstructorSignature:
    """
    Parse a C# constructor signature
    """
    return ConstructorSignature(_parse(CSharpParser.constructor_signature, signature))


def parse_method_signature(signature: str) -> MethodSignature:
    """
    Parse a C# method signature
    """
    return MethodSignature(_parse(CSharpParser.method_signature, signature))


def parse_enummember_signature(signature: str) -> EnumMemberSignature:
    """
    Parse a C# enum member signature
    """
    return EnumMemberSignature(_parse(CSharpParser.enummember_signature, signature))


def parse_delegate_signature(signature: str) -> DelegateSignature:
    """
    Parse a C# delegate signature
    """
    return DelegateSignature(_parse(CSharpParser.delegate_signature, signature))


def parse_namespace_signature(signature: str) -> NamespaceSignature:
    """
    Parse a C# namespace signature
    """
    return NamespaceSignature(_parse(CSharpParser.namespace_signature, signature))


def parse_enum_signature(signature: str) -> EnumSignature:
    """
    Parse a C# enum signature
    """
    return EnumSignature(_parse(CSharpParser.enum_signature, signature))


def parse_property_signature(signature: str) -> PropertySignature:
    """
    Parse a C# property signature
    """
    return PropertySignature(_parse(CSharpParser.property_signature, signature))


def parse_indexer_signature(signature: str) -> IndexerSignature:
    """
    Parse a C# indexer signature
    """
    return IndexerSignature(_parse(CSharpParser.indexer_signature, signature))


def parse_conversion_signature(signature: str) -> ConversionSignature:
    """
    Parse a C# conversion signature
    """
    return ConversionSignature(_parse(CSharpParser.conversion_signature, signature))


def parse_operator_signature(signature: str) -> OperatorSignature:
    """
    Parse a C# operator signature
    """
    return OperatorSignature(_parse(CSharpParser.operator_signature, signature))
