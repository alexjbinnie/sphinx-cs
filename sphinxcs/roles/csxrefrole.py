from typing import Tuple

from docutils.nodes import Element
from sphinx.environment import BuildEnvironment
from sphinx.roles import XRefRole

from sphinxcs.roles.crossreference import CrossReference
from sphinxcs.utils.signature_parser import parse_xref_target


class CSXRefRole(XRefRole):

    def process_link(self, env: BuildEnvironment, refnode: Element,
                     has_explicit_title: bool, title: str, target: str) -> Tuple[str, str]:
        title, target = super().process_link(env, refnode, has_explicit_title, title, target)

        cross_reference = parse_xref_target(target)

        if not isinstance(cross_reference, CrossReference):
            return title, target

        # If there is a ~ prefix, strip out unwanted information from the title
        if not has_explicit_title:
            if title[0:1] == '~':
                title = cross_reference.unqualified.get_display_name(False)
            else:
                title = cross_reference.qualified.get_display_name(True)
        return title, target
