from typing import List, Optional

from sphinxcs.antlr.CSharpParser import CSharpParser
from sphinxcs.utils.signatures import Signature, IndexerSignature
from sphinxcs.utils.types import CSharpType, CSharpParameter, UnqualifiedCSharpType, QualifiedCSharpType, \
    CSharpGenericTypeParameter


class CrossReference:
    """
    Represents a reference to a specific type
    """

    _type: QualifiedCSharpType
    _parameters: List[CSharpParameter]

    def __init__(self, type: QualifiedCSharpType, parameters: Optional[List[CSharpParameter]] = None):
        self._type = type
        self._parameters = parameters
        if not self._parameters:
            self._parameters = []

    @property
    def unqualified(self) -> UnqualifiedCSharpType:
        return self._type.unqualified_type

    @property
    def qualified(self) -> QualifiedCSharpType:
        return self._type

    def matches(self, signature: Signature):
        # For now, indexer signature does not match, as it's type is the same as its owner
        if isinstance(signature, IndexerSignature):
            return False

        sig_type = signature.type_name
        if isinstance(sig_type, UnqualifiedCSharpType):
            sig_type = [sig_type]
        elif not isinstance(sig_type, QualifiedCSharpType):
            raise ValueError(f"Signature {signature.unique_name} must have a type_name which is qualified")
        if len(sig_type) < len(self._type):
            return False
        for i in range(0, len(self._type)):
            a = sig_type[-(1 + i)]
            b = self._type[-(1 + i)]
            if not isinstance(a, UnqualifiedCSharpType):
                raise ValueError(f"Signature {signature.unique_name} must have a type_name which is qualified")
            if not isinstance(b, UnqualifiedCSharpType):
                raise ValueError(
                    f"Reference {self._type.get_display_name(True)} must have a type_name which is qualified")
            if not UnqualifiedCSharpType.are_equal_ignoring_generics(a, b):
                return False
        return True

    def get_display_name(self, qualified: bool) -> str:
        return self._type.get_display_name(qualified)

    def get_linkable_name(self, generic_seperator='`'):
        return self._type.get_linkable_name(generic_seperator)


def _parse_generic_type_count(context: CSharpParser.Generic_type_countContext) -> int:
    return int(context.INTEGER_VALUE().getText())


def _parse_object_reference_generic_list(context: CSharpParser.Object_reference_generic_listContext):
    types = []
    for type in context.object_reference_generic():
        types.append(_parse_object_reference_generic(type))
    return types


def _parse_object_reference_generic(context: CSharpParser.Object_reference_genericContext) -> Optional[CSharpType]:
    if context.object_reference():
        return _parse_object_reference(context.object_reference())
    if context.type_param_reference():
        return CSharpGenericTypeParameter(context.type_param_reference().unqualified_type_name().getText())
    return None


def _parse_object_reference_qualifier(context: CSharpParser.Object_reference_qualifierContext) -> List[
    UnqualifiedCSharpType]:
    types = []
    for t in context.unqualified_object_reference():
        types.append(_parse_unqualified_object_reference(t))
    return types


def _parse_unqualified_object_reference(
        context: CSharpParser.Unqualified_object_referenceContext) -> UnqualifiedCSharpType:
    name = context.unqualified_type_name().getText()
    if context.generic_type_count():
        return UnqualifiedCSharpType(name, _parse_generic_type_count(context.generic_type_count()))
    if context.object_reference_generic_list():
        params = _parse_object_reference_generic_list(context.object_reference_generic_list())
        if None in params:
            return UnqualifiedCSharpType(name, len(params))
        return UnqualifiedCSharpType(name, params)
    return UnqualifiedCSharpType(name, [])


def _parse_object_reference(context: CSharpParser.Object_referenceContext) -> QualifiedCSharpType:
    unqualified = _parse_unqualified_object_reference(context.unqualified_object_reference())
    qualified = _parse_object_reference_qualifier(context.object_reference_qualifier())
    if len(qualified) == 0:
        return QualifiedCSharpType([unqualified])
    else:
        return QualifiedCSharpType(qualified + [unqualified])


def parseCrossReference(context: CSharpParser.Object_referenceContext) -> CrossReference:
    return CrossReference(_parse_object_reference(context))
