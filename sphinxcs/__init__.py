from sphinx.application import Sphinx

from sphinxcs.csharpdomain import CSharpDomain

from sphinxcs.autodoc.autodoc_docfx import autodoc_docfx

def setup(app: Sphinx):
    """
    Setup function for the C# Sphinx extension.
    :return: JSON describing the extension
    """

    print("Registering C# Plugin for Sphinx...")

    app.add_domain(CSharpDomain)

    return {'version': '0.1'}  # identifies the version of our extension
