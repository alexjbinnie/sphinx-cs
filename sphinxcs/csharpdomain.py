import re
from typing import List, Dict, Tuple, Iterable, Optional

from docutils import nodes
from docutils.nodes import Element
from sphinx.addnodes import pending_xref
from sphinx.builders import Builder
from sphinx.domains import Domain, ObjType
from sphinx.environment import BuildEnvironment, logger
from sphinx.util.nodes import make_refnode

from sphinxcs.directive.setscope import SetScope
from sphinxcs.directive.signature.csclass import CSClass
from sphinxcs.directive.signature.csconstructor import CSConstructor
from sphinxcs.directive.signature.csconversion import CSConversion
from sphinxcs.directive.signature.csdelegate import CSDelegate
from sphinxcs.directive.signature.csenum import CSEnum
from sphinxcs.directive.signature.csenummember import CSEnumMember
from sphinxcs.directive.signature.csevent import CSEvent
from sphinxcs.directive.signature.csfield import CSField
from sphinxcs.directive.signature.csindexer import CSIndexer
from sphinxcs.directive.signature.csinterface import CSInterface
from sphinxcs.directive.signature.csmethod import CSMethod
from sphinxcs.directive.signature.csnamespace import CSNamespace
from sphinxcs.directive.signature.csoperator import CSOperator
from sphinxcs.directive.signature.csproperty import CSProperty
from sphinxcs.directive.signature.csstruct import CSStruct
from sphinxcs.roles.crossreference import CrossReference
from sphinxcs.roles.csxrefrole import CSXRefRole
from sphinxcs.utils.signature_parser import parse_xref_target
from sphinxcs.utils.signatures import Signature


class ObjectEntry:
    _docname: str

    def __init__(self, docname: str, signature: Signature):
        self._signature = signature
        self._docname = docname

    @property
    def unqualified_name(self):
        return self._signature.type_name.get_display_name(False)

    @property
    def qualified_name(self):
        return self._signature.type_name.get_display_name(True)

    @property
    def docname(self):
        return self._docname

    @property
    def objtype(self):
        return self._signature.objtype

    @property
    def target_id(self):
        return self._signature.unique_name


class CSharpDomain(Domain):
    """
    Sphinx domain representing C# code. Directives are found under the cs: domain.
    """

    name = 'cs'

    label = 'C#'

    directives = {
        'class': CSClass,
        'field': CSField,
        'property': CSProperty,
        'namespace': CSNamespace,
        'method': CSMethod,
        'constructor': CSConstructor,
        'interface': CSInterface,
        'struct': CSStruct,
        'event': CSEvent,
        'indexer': CSIndexer,
        'enum': CSEnum,
        'enummember': CSEnumMember,
        'delegate': CSDelegate,
        'operator': CSOperator,
        'conversion': CSConversion,

        'setscope': SetScope
    }

    object_types = {
        'class': ObjType('class', 'class', 'type'),
        'field': ObjType('field', 'field', 'member'),
        'property': ObjType('property', 'prop', 'member'),
        'method': ObjType('method', 'meth', 'member')
    }

    roles = {
        'class': CSXRefRole(),
        'field': CSXRefRole(),
        'property': CSXRefRole(),
        'any': CSXRefRole()
    }

    initial_data = {
        'objects': {},  # identifier -> docname, objtype
    }  # type: Dict[str, Dict[str, Tuple[Any]]]

    cs_classes = []

    @property
    def objects(self) -> Dict[str, ObjectEntry]:
        return self.data.setdefault('objects', {})

    def resolve_any_xref(self, env: BuildEnvironment, fromdocname: str, builder: Builder, target: str,
                         node: pending_xref, contnode: Element) -> List[Tuple[str, Element]]:
        xref = self.resolve_xref(env, fromdocname, builder, "any", target, node, contnode)
        if not xref:
            return []
        return [('cs:any', xref)]

    def resolve_xref(self, env: BuildEnvironment, fromdocname: str, builder: Builder,
                     typ: str, target: str, node: pending_xref, contnode: Element
                     ) -> Optional[Element]:

        cross_reference = parse_xref_target(target)
        if not isinstance(cross_reference, CrossReference):
            return None

        target = target.lstrip('~')

        matches = [entry
                   for entry
                   in self.objects.values() if
                   cross_reference.matches(entry._signature) and (entry.objtype == typ or typ == 'any')]

        if len(matches) > 1:
            logger.warning(f"More than one XRef reference for: {target}.")
            for match in matches:
                logger.warning(match._signature.type_name.get_display_name(True))

        if len(matches) > 0:
            entry = matches[0]

            return make_refnode(builder, fromdocname, entry.docname, entry.target_id,
                                contnode, entry.unqualified_name)
        else:
            if target.startswith("System."):
                target = cross_reference.get_linkable_name(
                    generic_seperator='-')  # switch from ` to - for generic arguments
                node = nodes.reference('', '', internal=False)
                node['refuri'] = f'https://docs.microsoft.com/en-us/dotnet/api/{target.lower()}'
                node.append(contnode)
                return node
            if target.startswith("Grpc.Core."):
                target = cross_reference.get_linkable_name('-')  # switch from ` to - for generic arguments
                node = nodes.reference('', '', internal=False)
                node['refuri'] = f'https://grpc.github.io/grpc/csharp/api/{target.lower()}.html'
                node.append(contnode)
                return node
            if target.startswith("Google.Protobuf."):
                def camel_case_split(str):
                    return re.findall(r'[A-Z](?:[a-z]+|[A-Z]*(?=[A-Z]|$))', str)

                target = '/'.join(
                    ['-'.join(camel_case_split(part.get_linkable_name())) for part in cross_reference.qualified])
                node = nodes.reference('', '', internal=False)
                node[
                    'refuri'] = f'https://developers.google.com/protocol-buffers/docs/reference/csharp/class/{target.lower()}.html'
                node.append(contnode)
                return node
            if target.startswith("UnityEngine."):
                node = nodes.reference('', '', internal=False)
                node['refuri'] = f"https://docs.unity3d.com/ScriptReference/{target.split('.')[-1]}.html"
                node.append(contnode)
                return node
            if target.startswith("Valve."):
                node = nodes.reference('', '', internal=False)
                node['refuri'] = f"https://valvesoftware.github.io/steamvr_unity_plugin/api/{target}.html"
                node.append(contnode)
                return node

            logger.warning(f"Unresolved XRef reference: {target}.")

            return None

        pass

    def merge_domaindata(self, docnames: List[str], otherdata: Dict) -> None:
        raise NotImplementedError()

    def note_object(self, signature: Signature):
        if signature.unique_name in self.objects.keys():
            other = self.objects[signature.unique_name]
            logger.warning(f'duplicate object description of {signature.unique_name}, '
                           f'other instance in {other.docname}, use :noindex: for one of them')
        self.objects[signature.unique_name] = ObjectEntry(self.env.docname, signature)

    @staticmethod
    def _as_obj_description(entry: ObjectEntry) -> Tuple[str, str, str, str, str, int]:
        """
        Convert an object identifier with its metadata to the Tuple expected for get_objects
        """
        return entry.qualified_name, entry.unqualified_name, entry.objtype, entry.docname, \
               entry._signature.unique_name, 1

    def get_objects(self) -> Iterable[Tuple[str, str, str, str, str, int]]:
        for entry in self.objects.values():
            yield self._as_obj_description(entry)
