# Generated from C:/Users/Alex-Binnie/Projects/Python/sphinxcs/sphinxcs/antlr\CSharp.g4 by ANTLR 4.8
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .CSharpParser import CSharpParser
else:
    from CSharpParser import CSharpParser

# This class defines a complete generic visitor for a parse tree produced by CSharpParser.

class CSharpVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by CSharpParser#class_signature.
    def visitClass_signature(self, ctx:CSharpParser.Class_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#enum_signature.
    def visitEnum_signature(self, ctx:CSharpParser.Enum_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#struct_signature.
    def visitStruct_signature(self, ctx:CSharpParser.Struct_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#interface_signature.
    def visitInterface_signature(self, ctx:CSharpParser.Interface_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#namespace_signature.
    def visitNamespace_signature(self, ctx:CSharpParser.Namespace_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#event_signature.
    def visitEvent_signature(self, ctx:CSharpParser.Event_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#property_signature.
    def visitProperty_signature(self, ctx:CSharpParser.Property_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#field_signature.
    def visitField_signature(self, ctx:CSharpParser.Field_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#enummember_signature.
    def visitEnummember_signature(self, ctx:CSharpParser.Enummember_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#method_signature.
    def visitMethod_signature(self, ctx:CSharpParser.Method_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#constructor_signature.
    def visitConstructor_signature(self, ctx:CSharpParser.Constructor_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#delegate_signature.
    def visitDelegate_signature(self, ctx:CSharpParser.Delegate_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#indexer_signature.
    def visitIndexer_signature(self, ctx:CSharpParser.Indexer_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#conversion_signature.
    def visitConversion_signature(self, ctx:CSharpParser.Conversion_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#operator_signature.
    def visitOperator_signature(self, ctx:CSharpParser.Operator_signatureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#object_cross_reference.
    def visitObject_cross_reference(self, ctx:CSharpParser.Object_cross_referenceContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#object_reference.
    def visitObject_reference(self, ctx:CSharpParser.Object_referenceContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#object_reference_arguments.
    def visitObject_reference_arguments(self, ctx:CSharpParser.Object_reference_argumentsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#object_reference_argument_list.
    def visitObject_reference_argument_list(self, ctx:CSharpParser.Object_reference_argument_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#object_reference_argument.
    def visitObject_reference_argument(self, ctx:CSharpParser.Object_reference_argumentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#object_reference_mutator.
    def visitObject_reference_mutator(self, ctx:CSharpParser.Object_reference_mutatorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#object_reference_qualifier.
    def visitObject_reference_qualifier(self, ctx:CSharpParser.Object_reference_qualifierContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#unqualified_object_reference.
    def visitUnqualified_object_reference(self, ctx:CSharpParser.Unqualified_object_referenceContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#generic_type_count.
    def visitGeneric_type_count(self, ctx:CSharpParser.Generic_type_countContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#object_reference_generic_list.
    def visitObject_reference_generic_list(self, ctx:CSharpParser.Object_reference_generic_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#object_reference_generic.
    def visitObject_reference_generic(self, ctx:CSharpParser.Object_reference_genericContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_param_reference.
    def visitType_param_reference(self, ctx:CSharpParser.Type_param_referenceContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_parameter_constraints_clauses.
    def visitType_parameter_constraints_clauses(self, ctx:CSharpParser.Type_parameter_constraints_clausesContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_parameter_constraints_clause.
    def visitType_parameter_constraints_clause(self, ctx:CSharpParser.Type_parameter_constraints_clauseContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_parameter_constraints.
    def visitType_parameter_constraints(self, ctx:CSharpParser.Type_parameter_constraintsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_parameter_constraint.
    def visitType_parameter_constraint(self, ctx:CSharpParser.Type_parameter_constraintContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#attributes.
    def visitAttributes(self, ctx:CSharpParser.AttributesContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#attribute.
    def visitAttribute(self, ctx:CSharpParser.AttributeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#argument_list.
    def visitArgument_list(self, ctx:CSharpParser.Argument_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#argument.
    def visitArgument(self, ctx:CSharpParser.ArgumentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#parameter_list.
    def visitParameter_list(self, ctx:CSharpParser.Parameter_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#parameter.
    def visitParameter(self, ctx:CSharpParser.ParameterContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#literal.
    def visitLiteral(self, ctx:CSharpParser.LiteralContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#accessor_declarations.
    def visitAccessor_declarations(self, ctx:CSharpParser.Accessor_declarationsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#parameter_modifiers.
    def visitParameter_modifiers(self, ctx:CSharpParser.Parameter_modifiersContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#class_base.
    def visitClass_base(self, ctx:CSharpParser.Class_baseContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#operator_name.
    def visitOperator_name(self, ctx:CSharpParser.Operator_nameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_.
    def visitType_(self, ctx:CSharpParser.Type_Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_array_decorator.
    def visitType_array_decorator(self, ctx:CSharpParser.Type_array_decoratorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_nullable_decorator.
    def visitType_nullable_decorator(self, ctx:CSharpParser.Type_nullable_decoratorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_tuple.
    def visitType_tuple(self, ctx:CSharpParser.Type_tupleContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_name.
    def visitType_name(self, ctx:CSharpParser.Type_nameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#unqualified_type.
    def visitUnqualified_type(self, ctx:CSharpParser.Unqualified_typeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#unqualified_type_name.
    def visitUnqualified_type_name(self, ctx:CSharpParser.Unqualified_type_nameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#parameter_name.
    def visitParameter_name(self, ctx:CSharpParser.Parameter_nameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_parameter_list.
    def visitType_parameter_list(self, ctx:CSharpParser.Type_parameter_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_parameter.
    def visitType_parameter(self, ctx:CSharpParser.Type_parameterContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_parameter_modifiers.
    def visitType_parameter_modifiers(self, ctx:CSharpParser.Type_parameter_modifiersContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#type_parameter_modifier.
    def visitType_parameter_modifier(self, ctx:CSharpParser.Type_parameter_modifierContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#integral_value.
    def visitIntegral_value(self, ctx:CSharpParser.Integral_valueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#string_literal.
    def visitString_literal(self, ctx:CSharpParser.String_literalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#enum_literal.
    def visitEnum_literal(self, ctx:CSharpParser.Enum_literalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#numeric_literal.
    def visitNumeric_literal(self, ctx:CSharpParser.Numeric_literalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#bool_literal.
    def visitBool_literal(self, ctx:CSharpParser.Bool_literalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#modifiers.
    def visitModifiers(self, ctx:CSharpParser.ModifiersContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CSharpParser#modifier.
    def visitModifier(self, ctx:CSharpParser.ModifierContext):
        return self.visitChildren(ctx)



del CSharpParser