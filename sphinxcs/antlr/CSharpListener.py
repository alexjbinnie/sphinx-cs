# Generated from C:/Users/Alex-Binnie/Projects/Python/sphinxcs/sphinxcs/antlr\CSharp.g4 by ANTLR 4.8
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .CSharpParser import CSharpParser
else:
    from CSharpParser import CSharpParser

# This class defines a complete listener for a parse tree produced by CSharpParser.
class CSharpListener(ParseTreeListener):

    # Enter a parse tree produced by CSharpParser#class_signature.
    def enterClass_signature(self, ctx:CSharpParser.Class_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#class_signature.
    def exitClass_signature(self, ctx:CSharpParser.Class_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#enum_signature.
    def enterEnum_signature(self, ctx:CSharpParser.Enum_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#enum_signature.
    def exitEnum_signature(self, ctx:CSharpParser.Enum_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#struct_signature.
    def enterStruct_signature(self, ctx:CSharpParser.Struct_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#struct_signature.
    def exitStruct_signature(self, ctx:CSharpParser.Struct_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#interface_signature.
    def enterInterface_signature(self, ctx:CSharpParser.Interface_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#interface_signature.
    def exitInterface_signature(self, ctx:CSharpParser.Interface_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#namespace_signature.
    def enterNamespace_signature(self, ctx:CSharpParser.Namespace_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#namespace_signature.
    def exitNamespace_signature(self, ctx:CSharpParser.Namespace_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#event_signature.
    def enterEvent_signature(self, ctx:CSharpParser.Event_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#event_signature.
    def exitEvent_signature(self, ctx:CSharpParser.Event_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#property_signature.
    def enterProperty_signature(self, ctx:CSharpParser.Property_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#property_signature.
    def exitProperty_signature(self, ctx:CSharpParser.Property_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#field_signature.
    def enterField_signature(self, ctx:CSharpParser.Field_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#field_signature.
    def exitField_signature(self, ctx:CSharpParser.Field_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#enummember_signature.
    def enterEnummember_signature(self, ctx:CSharpParser.Enummember_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#enummember_signature.
    def exitEnummember_signature(self, ctx:CSharpParser.Enummember_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#method_signature.
    def enterMethod_signature(self, ctx:CSharpParser.Method_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#method_signature.
    def exitMethod_signature(self, ctx:CSharpParser.Method_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#constructor_signature.
    def enterConstructor_signature(self, ctx:CSharpParser.Constructor_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#constructor_signature.
    def exitConstructor_signature(self, ctx:CSharpParser.Constructor_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#delegate_signature.
    def enterDelegate_signature(self, ctx:CSharpParser.Delegate_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#delegate_signature.
    def exitDelegate_signature(self, ctx:CSharpParser.Delegate_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#indexer_signature.
    def enterIndexer_signature(self, ctx:CSharpParser.Indexer_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#indexer_signature.
    def exitIndexer_signature(self, ctx:CSharpParser.Indexer_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#conversion_signature.
    def enterConversion_signature(self, ctx:CSharpParser.Conversion_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#conversion_signature.
    def exitConversion_signature(self, ctx:CSharpParser.Conversion_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#operator_signature.
    def enterOperator_signature(self, ctx:CSharpParser.Operator_signatureContext):
        pass

    # Exit a parse tree produced by CSharpParser#operator_signature.
    def exitOperator_signature(self, ctx:CSharpParser.Operator_signatureContext):
        pass


    # Enter a parse tree produced by CSharpParser#object_cross_reference.
    def enterObject_cross_reference(self, ctx:CSharpParser.Object_cross_referenceContext):
        pass

    # Exit a parse tree produced by CSharpParser#object_cross_reference.
    def exitObject_cross_reference(self, ctx:CSharpParser.Object_cross_referenceContext):
        pass


    # Enter a parse tree produced by CSharpParser#object_reference.
    def enterObject_reference(self, ctx:CSharpParser.Object_referenceContext):
        pass

    # Exit a parse tree produced by CSharpParser#object_reference.
    def exitObject_reference(self, ctx:CSharpParser.Object_referenceContext):
        pass


    # Enter a parse tree produced by CSharpParser#object_reference_arguments.
    def enterObject_reference_arguments(self, ctx:CSharpParser.Object_reference_argumentsContext):
        pass

    # Exit a parse tree produced by CSharpParser#object_reference_arguments.
    def exitObject_reference_arguments(self, ctx:CSharpParser.Object_reference_argumentsContext):
        pass


    # Enter a parse tree produced by CSharpParser#object_reference_argument_list.
    def enterObject_reference_argument_list(self, ctx:CSharpParser.Object_reference_argument_listContext):
        pass

    # Exit a parse tree produced by CSharpParser#object_reference_argument_list.
    def exitObject_reference_argument_list(self, ctx:CSharpParser.Object_reference_argument_listContext):
        pass


    # Enter a parse tree produced by CSharpParser#object_reference_argument.
    def enterObject_reference_argument(self, ctx:CSharpParser.Object_reference_argumentContext):
        pass

    # Exit a parse tree produced by CSharpParser#object_reference_argument.
    def exitObject_reference_argument(self, ctx:CSharpParser.Object_reference_argumentContext):
        pass


    # Enter a parse tree produced by CSharpParser#object_reference_mutator.
    def enterObject_reference_mutator(self, ctx:CSharpParser.Object_reference_mutatorContext):
        pass

    # Exit a parse tree produced by CSharpParser#object_reference_mutator.
    def exitObject_reference_mutator(self, ctx:CSharpParser.Object_reference_mutatorContext):
        pass


    # Enter a parse tree produced by CSharpParser#object_reference_qualifier.
    def enterObject_reference_qualifier(self, ctx:CSharpParser.Object_reference_qualifierContext):
        pass

    # Exit a parse tree produced by CSharpParser#object_reference_qualifier.
    def exitObject_reference_qualifier(self, ctx:CSharpParser.Object_reference_qualifierContext):
        pass


    # Enter a parse tree produced by CSharpParser#unqualified_object_reference.
    def enterUnqualified_object_reference(self, ctx:CSharpParser.Unqualified_object_referenceContext):
        pass

    # Exit a parse tree produced by CSharpParser#unqualified_object_reference.
    def exitUnqualified_object_reference(self, ctx:CSharpParser.Unqualified_object_referenceContext):
        pass


    # Enter a parse tree produced by CSharpParser#generic_type_count.
    def enterGeneric_type_count(self, ctx:CSharpParser.Generic_type_countContext):
        pass

    # Exit a parse tree produced by CSharpParser#generic_type_count.
    def exitGeneric_type_count(self, ctx:CSharpParser.Generic_type_countContext):
        pass


    # Enter a parse tree produced by CSharpParser#object_reference_generic_list.
    def enterObject_reference_generic_list(self, ctx:CSharpParser.Object_reference_generic_listContext):
        pass

    # Exit a parse tree produced by CSharpParser#object_reference_generic_list.
    def exitObject_reference_generic_list(self, ctx:CSharpParser.Object_reference_generic_listContext):
        pass


    # Enter a parse tree produced by CSharpParser#object_reference_generic.
    def enterObject_reference_generic(self, ctx:CSharpParser.Object_reference_genericContext):
        pass

    # Exit a parse tree produced by CSharpParser#object_reference_generic.
    def exitObject_reference_generic(self, ctx:CSharpParser.Object_reference_genericContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_param_reference.
    def enterType_param_reference(self, ctx:CSharpParser.Type_param_referenceContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_param_reference.
    def exitType_param_reference(self, ctx:CSharpParser.Type_param_referenceContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_parameter_constraints_clauses.
    def enterType_parameter_constraints_clauses(self, ctx:CSharpParser.Type_parameter_constraints_clausesContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_parameter_constraints_clauses.
    def exitType_parameter_constraints_clauses(self, ctx:CSharpParser.Type_parameter_constraints_clausesContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_parameter_constraints_clause.
    def enterType_parameter_constraints_clause(self, ctx:CSharpParser.Type_parameter_constraints_clauseContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_parameter_constraints_clause.
    def exitType_parameter_constraints_clause(self, ctx:CSharpParser.Type_parameter_constraints_clauseContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_parameter_constraints.
    def enterType_parameter_constraints(self, ctx:CSharpParser.Type_parameter_constraintsContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_parameter_constraints.
    def exitType_parameter_constraints(self, ctx:CSharpParser.Type_parameter_constraintsContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_parameter_constraint.
    def enterType_parameter_constraint(self, ctx:CSharpParser.Type_parameter_constraintContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_parameter_constraint.
    def exitType_parameter_constraint(self, ctx:CSharpParser.Type_parameter_constraintContext):
        pass


    # Enter a parse tree produced by CSharpParser#attributes.
    def enterAttributes(self, ctx:CSharpParser.AttributesContext):
        pass

    # Exit a parse tree produced by CSharpParser#attributes.
    def exitAttributes(self, ctx:CSharpParser.AttributesContext):
        pass


    # Enter a parse tree produced by CSharpParser#attribute.
    def enterAttribute(self, ctx:CSharpParser.AttributeContext):
        pass

    # Exit a parse tree produced by CSharpParser#attribute.
    def exitAttribute(self, ctx:CSharpParser.AttributeContext):
        pass


    # Enter a parse tree produced by CSharpParser#argument_list.
    def enterArgument_list(self, ctx:CSharpParser.Argument_listContext):
        pass

    # Exit a parse tree produced by CSharpParser#argument_list.
    def exitArgument_list(self, ctx:CSharpParser.Argument_listContext):
        pass


    # Enter a parse tree produced by CSharpParser#argument.
    def enterArgument(self, ctx:CSharpParser.ArgumentContext):
        pass

    # Exit a parse tree produced by CSharpParser#argument.
    def exitArgument(self, ctx:CSharpParser.ArgumentContext):
        pass


    # Enter a parse tree produced by CSharpParser#parameter_list.
    def enterParameter_list(self, ctx:CSharpParser.Parameter_listContext):
        pass

    # Exit a parse tree produced by CSharpParser#parameter_list.
    def exitParameter_list(self, ctx:CSharpParser.Parameter_listContext):
        pass


    # Enter a parse tree produced by CSharpParser#parameter.
    def enterParameter(self, ctx:CSharpParser.ParameterContext):
        pass

    # Exit a parse tree produced by CSharpParser#parameter.
    def exitParameter(self, ctx:CSharpParser.ParameterContext):
        pass


    # Enter a parse tree produced by CSharpParser#literal.
    def enterLiteral(self, ctx:CSharpParser.LiteralContext):
        pass

    # Exit a parse tree produced by CSharpParser#literal.
    def exitLiteral(self, ctx:CSharpParser.LiteralContext):
        pass


    # Enter a parse tree produced by CSharpParser#accessor_declarations.
    def enterAccessor_declarations(self, ctx:CSharpParser.Accessor_declarationsContext):
        pass

    # Exit a parse tree produced by CSharpParser#accessor_declarations.
    def exitAccessor_declarations(self, ctx:CSharpParser.Accessor_declarationsContext):
        pass


    # Enter a parse tree produced by CSharpParser#parameter_modifiers.
    def enterParameter_modifiers(self, ctx:CSharpParser.Parameter_modifiersContext):
        pass

    # Exit a parse tree produced by CSharpParser#parameter_modifiers.
    def exitParameter_modifiers(self, ctx:CSharpParser.Parameter_modifiersContext):
        pass


    # Enter a parse tree produced by CSharpParser#class_base.
    def enterClass_base(self, ctx:CSharpParser.Class_baseContext):
        pass

    # Exit a parse tree produced by CSharpParser#class_base.
    def exitClass_base(self, ctx:CSharpParser.Class_baseContext):
        pass


    # Enter a parse tree produced by CSharpParser#operator_name.
    def enterOperator_name(self, ctx:CSharpParser.Operator_nameContext):
        pass

    # Exit a parse tree produced by CSharpParser#operator_name.
    def exitOperator_name(self, ctx:CSharpParser.Operator_nameContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_.
    def enterType_(self, ctx:CSharpParser.Type_Context):
        pass

    # Exit a parse tree produced by CSharpParser#type_.
    def exitType_(self, ctx:CSharpParser.Type_Context):
        pass


    # Enter a parse tree produced by CSharpParser#type_array_decorator.
    def enterType_array_decorator(self, ctx:CSharpParser.Type_array_decoratorContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_array_decorator.
    def exitType_array_decorator(self, ctx:CSharpParser.Type_array_decoratorContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_nullable_decorator.
    def enterType_nullable_decorator(self, ctx:CSharpParser.Type_nullable_decoratorContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_nullable_decorator.
    def exitType_nullable_decorator(self, ctx:CSharpParser.Type_nullable_decoratorContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_tuple.
    def enterType_tuple(self, ctx:CSharpParser.Type_tupleContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_tuple.
    def exitType_tuple(self, ctx:CSharpParser.Type_tupleContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_name.
    def enterType_name(self, ctx:CSharpParser.Type_nameContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_name.
    def exitType_name(self, ctx:CSharpParser.Type_nameContext):
        pass


    # Enter a parse tree produced by CSharpParser#unqualified_type.
    def enterUnqualified_type(self, ctx:CSharpParser.Unqualified_typeContext):
        pass

    # Exit a parse tree produced by CSharpParser#unqualified_type.
    def exitUnqualified_type(self, ctx:CSharpParser.Unqualified_typeContext):
        pass


    # Enter a parse tree produced by CSharpParser#unqualified_type_name.
    def enterUnqualified_type_name(self, ctx:CSharpParser.Unqualified_type_nameContext):
        pass

    # Exit a parse tree produced by CSharpParser#unqualified_type_name.
    def exitUnqualified_type_name(self, ctx:CSharpParser.Unqualified_type_nameContext):
        pass


    # Enter a parse tree produced by CSharpParser#parameter_name.
    def enterParameter_name(self, ctx:CSharpParser.Parameter_nameContext):
        pass

    # Exit a parse tree produced by CSharpParser#parameter_name.
    def exitParameter_name(self, ctx:CSharpParser.Parameter_nameContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_parameter_list.
    def enterType_parameter_list(self, ctx:CSharpParser.Type_parameter_listContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_parameter_list.
    def exitType_parameter_list(self, ctx:CSharpParser.Type_parameter_listContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_parameter.
    def enterType_parameter(self, ctx:CSharpParser.Type_parameterContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_parameter.
    def exitType_parameter(self, ctx:CSharpParser.Type_parameterContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_parameter_modifiers.
    def enterType_parameter_modifiers(self, ctx:CSharpParser.Type_parameter_modifiersContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_parameter_modifiers.
    def exitType_parameter_modifiers(self, ctx:CSharpParser.Type_parameter_modifiersContext):
        pass


    # Enter a parse tree produced by CSharpParser#type_parameter_modifier.
    def enterType_parameter_modifier(self, ctx:CSharpParser.Type_parameter_modifierContext):
        pass

    # Exit a parse tree produced by CSharpParser#type_parameter_modifier.
    def exitType_parameter_modifier(self, ctx:CSharpParser.Type_parameter_modifierContext):
        pass


    # Enter a parse tree produced by CSharpParser#integral_value.
    def enterIntegral_value(self, ctx:CSharpParser.Integral_valueContext):
        pass

    # Exit a parse tree produced by CSharpParser#integral_value.
    def exitIntegral_value(self, ctx:CSharpParser.Integral_valueContext):
        pass


    # Enter a parse tree produced by CSharpParser#string_literal.
    def enterString_literal(self, ctx:CSharpParser.String_literalContext):
        pass

    # Exit a parse tree produced by CSharpParser#string_literal.
    def exitString_literal(self, ctx:CSharpParser.String_literalContext):
        pass


    # Enter a parse tree produced by CSharpParser#enum_literal.
    def enterEnum_literal(self, ctx:CSharpParser.Enum_literalContext):
        pass

    # Exit a parse tree produced by CSharpParser#enum_literal.
    def exitEnum_literal(self, ctx:CSharpParser.Enum_literalContext):
        pass


    # Enter a parse tree produced by CSharpParser#numeric_literal.
    def enterNumeric_literal(self, ctx:CSharpParser.Numeric_literalContext):
        pass

    # Exit a parse tree produced by CSharpParser#numeric_literal.
    def exitNumeric_literal(self, ctx:CSharpParser.Numeric_literalContext):
        pass


    # Enter a parse tree produced by CSharpParser#bool_literal.
    def enterBool_literal(self, ctx:CSharpParser.Bool_literalContext):
        pass

    # Exit a parse tree produced by CSharpParser#bool_literal.
    def exitBool_literal(self, ctx:CSharpParser.Bool_literalContext):
        pass


    # Enter a parse tree produced by CSharpParser#modifiers.
    def enterModifiers(self, ctx:CSharpParser.ModifiersContext):
        pass

    # Exit a parse tree produced by CSharpParser#modifiers.
    def exitModifiers(self, ctx:CSharpParser.ModifiersContext):
        pass


    # Enter a parse tree produced by CSharpParser#modifier.
    def enterModifier(self, ctx:CSharpParser.ModifierContext):
        pass

    # Exit a parse tree produced by CSharpParser#modifier.
    def exitModifier(self, ctx:CSharpParser.ModifierContext):
        pass



del CSharpParser