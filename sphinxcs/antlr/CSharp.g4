grammar CSharp;

class_signature
    : attributes? modifiers? CLASS? type_name class_base? type_parameter_constraints_clauses? ';'? EOF
	;

enum_signature
    : attributes? modifiers? ENUM? type_name ';'? EOF
	;

struct_signature
    : attributes? modifiers? STRUCT? type_name class_base? type_parameter_constraints_clauses? ';'? EOF
	;

interface_signature
    : attributes? modifiers? INTERFACE? type_name class_base? type_parameter_constraints_clauses? ';'? EOF
	;

namespace_signature
    : attributes? NAMESPACE? type_name ';'? EOF
	;

event_signature
    : attributes? modifiers? EVENT? type_ type_name ';'? EOF
	;

property_signature
	: attributes? modifiers? type_ type_name ('{' accessor_declarations '}' ) ';'? EOF
	;

field_signature
	: attributes? modifiers? type_ type_name ('=' literal)? ';'? EOF
	;

enummember_signature
	: attributes? type_name ('=' literal)? ';'? EOF
	;


method_signature // lamdas from C# 6
	: attributes? modifiers? type_ type_name '(' parameter_list? ')' type_parameter_constraints_clauses? ';'? EOF
	;

constructor_signature // lamdas from C# 6
	: attributes? modifiers? type_name '(' parameter_list? ')' ';'? EOF
	;

delegate_signature
    : attributes? modifiers? DELEGATE? type_ type_name '(' parameter_list? ')' type_parameter_constraints_clauses? ';'? EOF
    ;

indexer_signature
    : attributes? modifiers? type_ THIS '[' parameter ']' '{' accessor_declarations '}' ';'? EOF
    ;

conversion_signature
    : attributes? modifiers? OPERATOR? type_ '(' parameter ')' ';'? EOF
    ;

operator_signature
    : attributes? modifiers? type_ OPERATOR? operator_name '(' parameter_list? ')' ';'? EOF
    ;



object_cross_reference
    : (object_reference | type_param_reference object_reference_mutator? ) EOF
    ;

// Represents a full object reference, such as ABC.DEF, ABC.XYZ`1 or ABC.DEF{XYZ}, that can appear in a cross reference.
object_reference
    : object_reference_qualifier (unqualified_object_reference) (object_reference_mutator? | object_reference_arguments?)
    ;

object_reference_arguments
    : '(' object_reference_argument_list ')'
    ;

object_reference_argument_list
    : object_reference_argument? (',' object_reference_argument)*
    ;

object_reference_argument
    : (object_reference | generic_type_count | type_param_reference) '@'?
    ;

object_reference_mutator
    : '?' | '[]'
    ;

// One or more prefixes that form the qualified name
object_reference_qualifier
    : ( unqualified_object_reference '.')*
    ;

// Represents a part of a type reference. Can be a type identifier, type name with generic argument count
// or a type name with specified arguments
unqualified_object_reference
    : unqualified_type_name ( generic_type_count | '{' object_reference_generic_list'}' )?
    ;

generic_type_count
    : '`' '`'? INTEGER_VALUE
    ;

object_reference_generic_list
    : object_reference_generic ( ',' object_reference_generic)*
    ;

object_reference_generic
    : (object_reference | type_param_reference | generic_type_count) object_reference_mutator?
    ;

// Represents a reference to a generic type, such as TValue
type_param_reference
    : '{' unqualified_type_name '}'
    ;





type_parameter_constraints_clauses
	: type_parameter_constraints_clause+
	;

type_parameter_constraints_clause
	: WHERE type_name ':' type_parameter_constraints
	;

type_parameter_constraints
	: type_parameter_constraint (',' type_parameter_constraint)*
	;

type_parameter_constraint
    : 'new()' | CLASS | STRUCT | type_
    ;




attributes
    : attribute*
    ;

attribute
    : '[' type_name ( '(' argument_list? ')' )? ']'
    ;

// A list of arguments to a function. Occurs as arguments to an attribute
argument_list
    : argument (',' argument)*
    ;

argument
    : (parameter_name '=')? literal
    ;

parameter_list
    : parameter (',' parameter)*
    ;

parameter
    : attributes? parameter_modifiers type_ parameter_name ('=' literal)?
    ;

literal
    : 'null' | numeric_literal | bool_literal | string_literal | enum_literal | DEFAULT ('(' type_ ')')? | 'typeof(' type_ ')'
    ;

accessor_declarations
	: (attributes? modifiers? GET ';')? (attributes? modifiers? SET ';')?
	;




parameter_modifiers
    : (PARAMS | THIS | OUT | REF)*
    ;

class_base
	: ':' type_ (','  type_)*
	;



operator_name
    : '+' | '-' | '!' | '++' | '--' | '*' | '/' | '&' | '|' | '^' | '<' '<' | '>' '>' | '==' | '!=' | '<' | '>' | '<=' | '>=' | TRUE | FALSE
    ;


/// ANY type, with qualifiers
type_
    : (type_name | type_tuple) type_array_decorator? type_nullable_decorator?
    ;

type_array_decorator
    : '[]'
    ;

type_nullable_decorator
    : '?'
    ;

type_tuple
    : '(' type_ parameter_name? (','  type_ parameter_name?)* ')'
    ;

/// A valid type name with possible scopes, such as ABC.DEF<T>.GHI<G>
type_name
    : (unqualified_type '.')* unqualified_type
    ;

/// A valid type name with possible generic type arguments
unqualified_type
    : unqualified_type_name type_parameter_list?
    ;

/// A valid type name, such as IInterface or CClass
unqualified_type_name
    : IDENTIFIER
    ;

/// A valid name of a parameter
parameter_name
    : IDENTIFIER | SET | GET
    ;

/// A set of generic type arguments
type_parameter_list
	: '<' type_parameter (',' type_parameter)* '>'
	;

type_parameter
    : attributes? type_parameter_modifiers? type_
    ;

/// Possible modifiers for type argument
type_parameter_modifiers
    : type_parameter_modifier+
    ;

type_parameter_modifier
	: IN | OUT
	;





integral_value
    : INTEGER_VALUE
    ;

string_literal
    : STRING_LITERAL
    ;

enum_literal
    : type_name
    ;

numeric_literal
    : INTEGER_VALUE | DOUBLE_VALUE | FLOAT_VALUE
    ;

bool_literal
    : TRUE | FALSE
    ;

/// A set of one or more keyword modifiers
modifiers
	: modifier+
	;

/// A single keyword that can modifier a method, class or other signature
modifier
	: NEW
	| PUBLIC
	| PROTECTED
	| INTERNAL
	| PRIVATE
	| READONLY
	| VOLATILE
	| VIRTUAL
	| SEALED
	| OVERRIDE
	| ABSTRACT
	| STATIC
	| UNSAFE
	| EXTERN
	| PARTIAL
	| ASYNC
	| PARAMS
	| THIS
	| CONST
	| IMPLICIT
	| EXPLICIT
	;

WHITESPACES:   (' ')+            -> channel(HIDDEN);

CLASS: 'class';
DELEGATE: 'delegate';
OPERATOR: 'operator';
ENUM: 'enum';
IN: 'in';
OUT: 'out';
STRUCT: 'struct';
NAMESPACE: 'namespace';
INTERFACE: 'interface';
EVENT: 'event';
NEW: 'new';
PUBLIC: 'public';
PROTECTED: 'protected';
INTERNAL: 'internal';
PRIVATE: 'private';
READONLY: 'readonly';
VOLATILE: 'volatile';
VIRTUAL: 'virtual';
SEALED: 'sealed';
OVERRIDE: 'override';
ABSTRACT: 'abstract';
STATIC: 'static';
UNSAFE: 'unsafe';
EXTERN: 'extern';
PARTIAL: 'partial';
ASYNC: 'async';
PARAMS: 'params';
THIS: 'this';
REF: 'ref';
WHERE: 'where';
DEFAULT: 'default';
CONST: 'const';
IMPLICIT: 'implicit';
EXPLICIT: 'explicit';
SET: 'set';
GET: 'get';
TRUE: 'true';
FALSE: 'false';

STRING_LITERAL
  : UNTERMINATED_STRING_LITERAL '"'
  ;

UNTERMINATED_STRING_LITERAL
  : '"' (~["\\\r\n] | '\\' (. | EOF))*
  ;

INTEGER_VALUE: ('-' | '+')? [0-9]+;
DOUBLE_VALUE: ('-' | '+')? [0-9]+ (. [0-9]+)? [dD]?;
FLOAT_VALUE: ('-' | '+')? [0-9]+ (. [0-9]+)? [fF]?;

IDENTIFIER:          '@'? [a-zA-Z0-9_]+;