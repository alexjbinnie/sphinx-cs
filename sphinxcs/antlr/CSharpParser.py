# Generated from C:/Users/Alex-Binnie/Projects/Python/sphinxcs/sphinxcs/antlr\CSharp.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3Q")
        buf.write("\u02d6\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t")
        buf.write("&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.\t.\4")
        buf.write("/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t\64")
        buf.write("\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t")
        buf.write(";\4<\t<\4=\t=\4>\t>\3\2\5\2~\n\2\3\2\5\2\u0081\n\2\3\2")
        buf.write("\5\2\u0084\n\2\3\2\3\2\5\2\u0088\n\2\3\2\5\2\u008b\n\2")
        buf.write("\3\2\5\2\u008e\n\2\3\2\3\2\3\3\5\3\u0093\n\3\3\3\5\3\u0096")
        buf.write("\n\3\3\3\5\3\u0099\n\3\3\3\3\3\5\3\u009d\n\3\3\3\3\3\3")
        buf.write("\4\5\4\u00a2\n\4\3\4\5\4\u00a5\n\4\3\4\5\4\u00a8\n\4\3")
        buf.write("\4\3\4\5\4\u00ac\n\4\3\4\5\4\u00af\n\4\3\4\5\4\u00b2\n")
        buf.write("\4\3\4\3\4\3\5\5\5\u00b7\n\5\3\5\5\5\u00ba\n\5\3\5\5\5")
        buf.write("\u00bd\n\5\3\5\3\5\5\5\u00c1\n\5\3\5\5\5\u00c4\n\5\3\5")
        buf.write("\5\5\u00c7\n\5\3\5\3\5\3\6\5\6\u00cc\n\6\3\6\5\6\u00cf")
        buf.write("\n\6\3\6\3\6\5\6\u00d3\n\6\3\6\3\6\3\7\5\7\u00d8\n\7\3")
        buf.write("\7\5\7\u00db\n\7\3\7\5\7\u00de\n\7\3\7\3\7\3\7\5\7\u00e3")
        buf.write("\n\7\3\7\3\7\3\b\5\b\u00e8\n\b\3\b\5\b\u00eb\n\b\3\b\3")
        buf.write("\b\3\b\3\b\3\b\3\b\3\b\5\b\u00f4\n\b\3\b\3\b\3\t\5\t\u00f9")
        buf.write("\n\t\3\t\5\t\u00fc\n\t\3\t\3\t\3\t\3\t\5\t\u0102\n\t\3")
        buf.write("\t\5\t\u0105\n\t\3\t\3\t\3\n\5\n\u010a\n\n\3\n\3\n\3\n")
        buf.write("\5\n\u010f\n\n\3\n\5\n\u0112\n\n\3\n\3\n\3\13\5\13\u0117")
        buf.write("\n\13\3\13\5\13\u011a\n\13\3\13\3\13\3\13\3\13\5\13\u0120")
        buf.write("\n\13\3\13\3\13\5\13\u0124\n\13\3\13\5\13\u0127\n\13\3")
        buf.write("\13\3\13\3\f\5\f\u012c\n\f\3\f\5\f\u012f\n\f\3\f\3\f\3")
        buf.write("\f\5\f\u0134\n\f\3\f\3\f\5\f\u0138\n\f\3\f\3\f\3\r\5\r")
        buf.write("\u013d\n\r\3\r\5\r\u0140\n\r\3\r\5\r\u0143\n\r\3\r\3\r")
        buf.write("\3\r\3\r\5\r\u0149\n\r\3\r\3\r\5\r\u014d\n\r\3\r\5\r\u0150")
        buf.write("\n\r\3\r\3\r\3\16\5\16\u0155\n\16\3\16\5\16\u0158\n\16")
        buf.write("\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u0163")
        buf.write("\n\16\3\16\3\16\3\17\5\17\u0168\n\17\3\17\5\17\u016b\n")
        buf.write("\17\3\17\5\17\u016e\n\17\3\17\3\17\3\17\3\17\3\17\5\17")
        buf.write("\u0175\n\17\3\17\3\17\3\20\5\20\u017a\n\20\3\20\5\20\u017d")
        buf.write("\n\20\3\20\3\20\5\20\u0181\n\20\3\20\3\20\3\20\5\20\u0186")
        buf.write("\n\20\3\20\3\20\5\20\u018a\n\20\3\20\3\20\3\21\3\21\3")
        buf.write("\21\5\21\u0191\n\21\5\21\u0193\n\21\3\21\3\21\3\22\3\22")
        buf.write("\3\22\5\22\u019a\n\22\3\22\5\22\u019d\n\22\5\22\u019f")
        buf.write("\n\22\3\23\3\23\3\23\3\23\3\24\5\24\u01a6\n\24\3\24\3")
        buf.write("\24\7\24\u01aa\n\24\f\24\16\24\u01ad\13\24\3\25\3\25\3")
        buf.write("\25\5\25\u01b2\n\25\3\25\5\25\u01b5\n\25\3\26\3\26\3\27")
        buf.write("\3\27\3\27\7\27\u01bc\n\27\f\27\16\27\u01bf\13\27\3\30")
        buf.write("\3\30\3\30\3\30\3\30\3\30\5\30\u01c7\n\30\3\31\3\31\5")
        buf.write("\31\u01cb\n\31\3\31\3\31\3\32\3\32\3\32\7\32\u01d2\n\32")
        buf.write("\f\32\16\32\u01d5\13\32\3\33\3\33\3\33\5\33\u01da\n\33")
        buf.write("\3\33\5\33\u01dd\n\33\3\34\3\34\3\34\3\34\3\35\6\35\u01e4")
        buf.write("\n\35\r\35\16\35\u01e5\3\36\3\36\3\36\3\36\3\36\3\37\3")
        buf.write("\37\3\37\7\37\u01f0\n\37\f\37\16\37\u01f3\13\37\3 \3 ")
        buf.write("\3 \3 \5 \u01f9\n \3!\7!\u01fc\n!\f!\16!\u01ff\13!\3\"")
        buf.write("\3\"\3\"\3\"\5\"\u0205\n\"\3\"\5\"\u0208\n\"\3\"\3\"\3")
        buf.write("#\3#\3#\7#\u020f\n#\f#\16#\u0212\13#\3$\3$\3$\5$\u0217")
        buf.write("\n$\3$\3$\3%\3%\3%\7%\u021e\n%\f%\16%\u0221\13%\3&\5&")
        buf.write("\u0224\n&\3&\3&\3&\3&\3&\5&\u022b\n&\3\'\3\'\3\'\3\'\3")
        buf.write("\'\3\'\3\'\3\'\3\'\3\'\5\'\u0237\n\'\3\'\3\'\3\'\3\'\5")
        buf.write("\'\u023d\n\'\3(\5(\u0240\n(\3(\5(\u0243\n(\3(\3(\5(\u0247")
        buf.write("\n(\3(\5(\u024a\n(\3(\5(\u024d\n(\3(\3(\5(\u0251\n(\3")
        buf.write(")\7)\u0254\n)\f)\16)\u0257\13)\3*\3*\3*\3*\7*\u025d\n")
        buf.write("*\f*\16*\u0260\13*\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3")
        buf.write("+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\5+\u0278\n+\3,\3,\5,\u027c")
        buf.write("\n,\3,\5,\u027f\n,\3,\5,\u0282\n,\3-\3-\3.\3.\3/\3/\3")
        buf.write("/\5/\u028b\n/\3/\3/\3/\5/\u0290\n/\7/\u0292\n/\f/\16/")
        buf.write("\u0295\13/\3/\3/\3\60\3\60\3\60\7\60\u029c\n\60\f\60\16")
        buf.write("\60\u029f\13\60\3\60\3\60\3\61\3\61\5\61\u02a5\n\61\3")
        buf.write("\62\3\62\3\63\3\63\3\64\3\64\3\64\3\64\7\64\u02af\n\64")
        buf.write("\f\64\16\64\u02b2\13\64\3\64\3\64\3\65\5\65\u02b7\n\65")
        buf.write("\3\65\5\65\u02ba\n\65\3\65\3\65\3\66\6\66\u02bf\n\66\r")
        buf.write("\66\16\66\u02c0\3\67\3\67\38\38\39\39\3:\3:\3;\3;\3<\3")
        buf.write("<\3=\6=\u02d0\n=\r=\16=\u02d1\3>\3>\3>\2\2?\2\4\6\b\n")
        buf.write("\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<")
        buf.write(">@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz\2\t\3\2\r\16\4\2++@B")
        buf.write("\4\2HIQQ\3\2*+\3\2NP\3\2JK\4\2\60AEG\2\u0328\2}\3\2\2")
        buf.write("\2\4\u0092\3\2\2\2\6\u00a1\3\2\2\2\b\u00b6\3\2\2\2\n\u00cb")
        buf.write("\3\2\2\2\f\u00d7\3\2\2\2\16\u00e7\3\2\2\2\20\u00f8\3\2")
        buf.write("\2\2\22\u0109\3\2\2\2\24\u0116\3\2\2\2\26\u012b\3\2\2")
        buf.write("\2\30\u013c\3\2\2\2\32\u0154\3\2\2\2\34\u0167\3\2\2\2")
        buf.write("\36\u0179\3\2\2\2 \u0192\3\2\2\2\"\u0196\3\2\2\2$\u01a0")
        buf.write("\3\2\2\2&\u01a5\3\2\2\2(\u01b1\3\2\2\2*\u01b6\3\2\2\2")
        buf.write(",\u01bd\3\2\2\2.\u01c0\3\2\2\2\60\u01c8\3\2\2\2\62\u01ce")
        buf.write("\3\2\2\2\64\u01d9\3\2\2\2\66\u01de\3\2\2\28\u01e3\3\2")
        buf.write("\2\2:\u01e7\3\2\2\2<\u01ec\3\2\2\2>\u01f8\3\2\2\2@\u01fd")
        buf.write("\3\2\2\2B\u0200\3\2\2\2D\u020b\3\2\2\2F\u0216\3\2\2\2")
        buf.write("H\u021a\3\2\2\2J\u0223\3\2\2\2L\u023c\3\2\2\2N\u0246\3")
        buf.write("\2\2\2P\u0255\3\2\2\2R\u0258\3\2\2\2T\u0277\3\2\2\2V\u027b")
        buf.write("\3\2\2\2X\u0283\3\2\2\2Z\u0285\3\2\2\2\\\u0287\3\2\2\2")
        buf.write("^\u029d\3\2\2\2`\u02a2\3\2\2\2b\u02a6\3\2\2\2d\u02a8\3")
        buf.write("\2\2\2f\u02aa\3\2\2\2h\u02b6\3\2\2\2j\u02be\3\2\2\2l\u02c2")
        buf.write("\3\2\2\2n\u02c4\3\2\2\2p\u02c6\3\2\2\2r\u02c8\3\2\2\2")
        buf.write("t\u02ca\3\2\2\2v\u02cc\3\2\2\2x\u02cf\3\2\2\2z\u02d3\3")
        buf.write("\2\2\2|~\5@!\2}|\3\2\2\2}~\3\2\2\2~\u0080\3\2\2\2\177")
        buf.write("\u0081\5x=\2\u0080\177\3\2\2\2\u0080\u0081\3\2\2\2\u0081")
        buf.write("\u0083\3\2\2\2\u0082\u0084\7&\2\2\u0083\u0082\3\2\2\2")
        buf.write("\u0083\u0084\3\2\2\2\u0084\u0085\3\2\2\2\u0085\u0087\5")
        buf.write("^\60\2\u0086\u0088\5R*\2\u0087\u0086\3\2\2\2\u0087\u0088")
        buf.write("\3\2\2\2\u0088\u008a\3\2\2\2\u0089\u008b\58\35\2\u008a")
        buf.write("\u0089\3\2\2\2\u008a\u008b\3\2\2\2\u008b\u008d\3\2\2\2")
        buf.write("\u008c\u008e\7\3\2\2\u008d\u008c\3\2\2\2\u008d\u008e\3")
        buf.write("\2\2\2\u008e\u008f\3\2\2\2\u008f\u0090\7\2\2\3\u0090\3")
        buf.write("\3\2\2\2\u0091\u0093\5@!\2\u0092\u0091\3\2\2\2\u0092\u0093")
        buf.write("\3\2\2\2\u0093\u0095\3\2\2\2\u0094\u0096\5x=\2\u0095\u0094")
        buf.write("\3\2\2\2\u0095\u0096\3\2\2\2\u0096\u0098\3\2\2\2\u0097")
        buf.write("\u0099\7)\2\2\u0098\u0097\3\2\2\2\u0098\u0099\3\2\2\2")
        buf.write("\u0099\u009a\3\2\2\2\u009a\u009c\5^\60\2\u009b\u009d\7")
        buf.write("\3\2\2\u009c\u009b\3\2\2\2\u009c\u009d\3\2\2\2\u009d\u009e")
        buf.write("\3\2\2\2\u009e\u009f\7\2\2\3\u009f\5\3\2\2\2\u00a0\u00a2")
        buf.write("\5@!\2\u00a1\u00a0\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a4")
        buf.write("\3\2\2\2\u00a3\u00a5\5x=\2\u00a4\u00a3\3\2\2\2\u00a4\u00a5")
        buf.write("\3\2\2\2\u00a5\u00a7\3\2\2\2\u00a6\u00a8\7,\2\2\u00a7")
        buf.write("\u00a6\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00a9\3\2\2\2")
        buf.write("\u00a9\u00ab\5^\60\2\u00aa\u00ac\5R*\2\u00ab\u00aa\3\2")
        buf.write("\2\2\u00ab\u00ac\3\2\2\2\u00ac\u00ae\3\2\2\2\u00ad\u00af")
        buf.write("\58\35\2\u00ae\u00ad\3\2\2\2\u00ae\u00af\3\2\2\2\u00af")
        buf.write("\u00b1\3\2\2\2\u00b0\u00b2\7\3\2\2\u00b1\u00b0\3\2\2\2")
        buf.write("\u00b1\u00b2\3\2\2\2\u00b2\u00b3\3\2\2\2\u00b3\u00b4\7")
        buf.write("\2\2\3\u00b4\7\3\2\2\2\u00b5\u00b7\5@!\2\u00b6\u00b5\3")
        buf.write("\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b9\3\2\2\2\u00b8\u00ba")
        buf.write("\5x=\2\u00b9\u00b8\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba\u00bc")
        buf.write("\3\2\2\2\u00bb\u00bd\7.\2\2\u00bc\u00bb\3\2\2\2\u00bc")
        buf.write("\u00bd\3\2\2\2\u00bd\u00be\3\2\2\2\u00be\u00c0\5^\60\2")
        buf.write("\u00bf\u00c1\5R*\2\u00c0\u00bf\3\2\2\2\u00c0\u00c1\3\2")
        buf.write("\2\2\u00c1\u00c3\3\2\2\2\u00c2\u00c4\58\35\2\u00c3\u00c2")
        buf.write("\3\2\2\2\u00c3\u00c4\3\2\2\2\u00c4\u00c6\3\2\2\2\u00c5")
        buf.write("\u00c7\7\3\2\2\u00c6\u00c5\3\2\2\2\u00c6\u00c7\3\2\2\2")
        buf.write("\u00c7\u00c8\3\2\2\2\u00c8\u00c9\7\2\2\3\u00c9\t\3\2\2")
        buf.write("\2\u00ca\u00cc\5@!\2\u00cb\u00ca\3\2\2\2\u00cb\u00cc\3")
        buf.write("\2\2\2\u00cc\u00ce\3\2\2\2\u00cd\u00cf\7-\2\2\u00ce\u00cd")
        buf.write("\3\2\2\2\u00ce\u00cf\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0")
        buf.write("\u00d2\5^\60\2\u00d1\u00d3\7\3\2\2\u00d2\u00d1\3\2\2\2")
        buf.write("\u00d2\u00d3\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\u00d5\7")
        buf.write("\2\2\3\u00d5\13\3\2\2\2\u00d6\u00d8\5@!\2\u00d7\u00d6")
        buf.write("\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8\u00da\3\2\2\2\u00d9")
        buf.write("\u00db\5x=\2\u00da\u00d9\3\2\2\2\u00da\u00db\3\2\2\2\u00db")
        buf.write("\u00dd\3\2\2\2\u00dc\u00de\7/\2\2\u00dd\u00dc\3\2\2\2")
        buf.write("\u00dd\u00de\3\2\2\2\u00de\u00df\3\2\2\2\u00df\u00e0\5")
        buf.write("V,\2\u00e0\u00e2\5^\60\2\u00e1\u00e3\7\3\2\2\u00e2\u00e1")
        buf.write("\3\2\2\2\u00e2\u00e3\3\2\2\2\u00e3\u00e4\3\2\2\2\u00e4")
        buf.write("\u00e5\7\2\2\3\u00e5\r\3\2\2\2\u00e6\u00e8\5@!\2\u00e7")
        buf.write("\u00e6\3\2\2\2\u00e7\u00e8\3\2\2\2\u00e8\u00ea\3\2\2\2")
        buf.write("\u00e9\u00eb\5x=\2\u00ea\u00e9\3\2\2\2\u00ea\u00eb\3\2")
        buf.write("\2\2\u00eb\u00ec\3\2\2\2\u00ec\u00ed\5V,\2\u00ed\u00ee")
        buf.write("\5^\60\2\u00ee\u00ef\7\4\2\2\u00ef\u00f0\5N(\2\u00f0\u00f1")
        buf.write("\7\5\2\2\u00f1\u00f3\3\2\2\2\u00f2\u00f4\7\3\2\2\u00f3")
        buf.write("\u00f2\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00f5\3\2\2\2")
        buf.write("\u00f5\u00f6\7\2\2\3\u00f6\17\3\2\2\2\u00f7\u00f9\5@!")
        buf.write("\2\u00f8\u00f7\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9\u00fb")
        buf.write("\3\2\2\2\u00fa\u00fc\5x=\2\u00fb\u00fa\3\2\2\2\u00fb\u00fc")
        buf.write("\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd\u00fe\5V,\2\u00fe\u0101")
        buf.write("\5^\60\2\u00ff\u0100\7\6\2\2\u0100\u0102\5L\'\2\u0101")
        buf.write("\u00ff\3\2\2\2\u0101\u0102\3\2\2\2\u0102\u0104\3\2\2\2")
        buf.write("\u0103\u0105\7\3\2\2\u0104\u0103\3\2\2\2\u0104\u0105\3")
        buf.write("\2\2\2\u0105\u0106\3\2\2\2\u0106\u0107\7\2\2\3\u0107\21")
        buf.write("\3\2\2\2\u0108\u010a\5@!\2\u0109\u0108\3\2\2\2\u0109\u010a")
        buf.write("\3\2\2\2\u010a\u010b\3\2\2\2\u010b\u010e\5^\60\2\u010c")
        buf.write("\u010d\7\6\2\2\u010d\u010f\5L\'\2\u010e\u010c\3\2\2\2")
        buf.write("\u010e\u010f\3\2\2\2\u010f\u0111\3\2\2\2\u0110\u0112\7")
        buf.write("\3\2\2\u0111\u0110\3\2\2\2\u0111\u0112\3\2\2\2\u0112\u0113")
        buf.write("\3\2\2\2\u0113\u0114\7\2\2\3\u0114\23\3\2\2\2\u0115\u0117")
        buf.write("\5@!\2\u0116\u0115\3\2\2\2\u0116\u0117\3\2\2\2\u0117\u0119")
        buf.write("\3\2\2\2\u0118\u011a\5x=\2\u0119\u0118\3\2\2\2\u0119\u011a")
        buf.write("\3\2\2\2\u011a\u011b\3\2\2\2\u011b\u011c\5V,\2\u011c\u011d")
        buf.write("\5^\60\2\u011d\u011f\7\7\2\2\u011e\u0120\5H%\2\u011f\u011e")
        buf.write("\3\2\2\2\u011f\u0120\3\2\2\2\u0120\u0121\3\2\2\2\u0121")
        buf.write("\u0123\7\b\2\2\u0122\u0124\58\35\2\u0123\u0122\3\2\2\2")
        buf.write("\u0123\u0124\3\2\2\2\u0124\u0126\3\2\2\2\u0125\u0127\7")
        buf.write("\3\2\2\u0126\u0125\3\2\2\2\u0126\u0127\3\2\2\2\u0127\u0128")
        buf.write("\3\2\2\2\u0128\u0129\7\2\2\3\u0129\25\3\2\2\2\u012a\u012c")
        buf.write("\5@!\2\u012b\u012a\3\2\2\2\u012b\u012c\3\2\2\2\u012c\u012e")
        buf.write("\3\2\2\2\u012d\u012f\5x=\2\u012e\u012d\3\2\2\2\u012e\u012f")
        buf.write("\3\2\2\2\u012f\u0130\3\2\2\2\u0130\u0131\5^\60\2\u0131")
        buf.write("\u0133\7\7\2\2\u0132\u0134\5H%\2\u0133\u0132\3\2\2\2\u0133")
        buf.write("\u0134\3\2\2\2\u0134\u0135\3\2\2\2\u0135\u0137\7\b\2\2")
        buf.write("\u0136\u0138\7\3\2\2\u0137\u0136\3\2\2\2\u0137\u0138\3")
        buf.write("\2\2\2\u0138\u0139\3\2\2\2\u0139\u013a\7\2\2\3\u013a\27")
        buf.write("\3\2\2\2\u013b\u013d\5@!\2\u013c\u013b\3\2\2\2\u013c\u013d")
        buf.write("\3\2\2\2\u013d\u013f\3\2\2\2\u013e\u0140\5x=\2\u013f\u013e")
        buf.write("\3\2\2\2\u013f\u0140\3\2\2\2\u0140\u0142\3\2\2\2\u0141")
        buf.write("\u0143\7\'\2\2\u0142\u0141\3\2\2\2\u0142\u0143\3\2\2\2")
        buf.write("\u0143\u0144\3\2\2\2\u0144\u0145\5V,\2\u0145\u0146\5^")
        buf.write("\60\2\u0146\u0148\7\7\2\2\u0147\u0149\5H%\2\u0148\u0147")
        buf.write("\3\2\2\2\u0148\u0149\3\2\2\2\u0149\u014a\3\2\2\2\u014a")
        buf.write("\u014c\7\b\2\2\u014b\u014d\58\35\2\u014c\u014b\3\2\2\2")
        buf.write("\u014c\u014d\3\2\2\2\u014d\u014f\3\2\2\2\u014e\u0150\7")
        buf.write("\3\2\2\u014f\u014e\3\2\2\2\u014f\u0150\3\2\2\2\u0150\u0151")
        buf.write("\3\2\2\2\u0151\u0152\7\2\2\3\u0152\31\3\2\2\2\u0153\u0155")
        buf.write("\5@!\2\u0154\u0153\3\2\2\2\u0154\u0155\3\2\2\2\u0155\u0157")
        buf.write("\3\2\2\2\u0156\u0158\5x=\2\u0157\u0156\3\2\2\2\u0157\u0158")
        buf.write("\3\2\2\2\u0158\u0159\3\2\2\2\u0159\u015a\5V,\2\u015a\u015b")
        buf.write("\7A\2\2\u015b\u015c\7\t\2\2\u015c\u015d\5J&\2\u015d\u015e")
        buf.write("\7\n\2\2\u015e\u015f\7\4\2\2\u015f\u0160\5N(\2\u0160\u0162")
        buf.write("\7\5\2\2\u0161\u0163\7\3\2\2\u0162\u0161\3\2\2\2\u0162")
        buf.write("\u0163\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0165\7\2\2\3")
        buf.write("\u0165\33\3\2\2\2\u0166\u0168\5@!\2\u0167\u0166\3\2\2")
        buf.write("\2\u0167\u0168\3\2\2\2\u0168\u016a\3\2\2\2\u0169\u016b")
        buf.write("\5x=\2\u016a\u0169\3\2\2\2\u016a\u016b\3\2\2\2\u016b\u016d")
        buf.write("\3\2\2\2\u016c\u016e\7(\2\2\u016d\u016c\3\2\2\2\u016d")
        buf.write("\u016e\3\2\2\2\u016e\u016f\3\2\2\2\u016f\u0170\5V,\2\u0170")
        buf.write("\u0171\7\7\2\2\u0171\u0172\5J&\2\u0172\u0174\7\b\2\2\u0173")
        buf.write("\u0175\7\3\2\2\u0174\u0173\3\2\2\2\u0174\u0175\3\2\2\2")
        buf.write("\u0175\u0176\3\2\2\2\u0176\u0177\7\2\2\3\u0177\35\3\2")
        buf.write("\2\2\u0178\u017a\5@!\2\u0179\u0178\3\2\2\2\u0179\u017a")
        buf.write("\3\2\2\2\u017a\u017c\3\2\2\2\u017b\u017d\5x=\2\u017c\u017b")
        buf.write("\3\2\2\2\u017c\u017d\3\2\2\2\u017d\u017e\3\2\2\2\u017e")
        buf.write("\u0180\5V,\2\u017f\u0181\7(\2\2\u0180\u017f\3\2\2\2\u0180")
        buf.write("\u0181\3\2\2\2\u0181\u0182\3\2\2\2\u0182\u0183\5T+\2\u0183")
        buf.write("\u0185\7\7\2\2\u0184\u0186\5H%\2\u0185\u0184\3\2\2\2\u0185")
        buf.write("\u0186\3\2\2\2\u0186\u0187\3\2\2\2\u0187\u0189\7\b\2\2")
        buf.write("\u0188\u018a\7\3\2\2\u0189\u0188\3\2\2\2\u0189\u018a\3")
        buf.write("\2\2\2\u018a\u018b\3\2\2\2\u018b\u018c\7\2\2\3\u018c\37")
        buf.write("\3\2\2\2\u018d\u0193\5\"\22\2\u018e\u0190\5\66\34\2\u018f")
        buf.write("\u0191\5*\26\2\u0190\u018f\3\2\2\2\u0190\u0191\3\2\2\2")
        buf.write("\u0191\u0193\3\2\2\2\u0192\u018d\3\2\2\2\u0192\u018e\3")
        buf.write("\2\2\2\u0193\u0194\3\2\2\2\u0194\u0195\7\2\2\3\u0195!")
        buf.write("\3\2\2\2\u0196\u0197\5,\27\2\u0197\u019e\5.\30\2\u0198")
        buf.write("\u019a\5*\26\2\u0199\u0198\3\2\2\2\u0199\u019a\3\2\2\2")
        buf.write("\u019a\u019f\3\2\2\2\u019b\u019d\5$\23\2\u019c\u019b\3")
        buf.write("\2\2\2\u019c\u019d\3\2\2\2\u019d\u019f\3\2\2\2\u019e\u0199")
        buf.write("\3\2\2\2\u019e\u019c\3\2\2\2\u019f#\3\2\2\2\u01a0\u01a1")
        buf.write("\7\7\2\2\u01a1\u01a2\5&\24\2\u01a2\u01a3\7\b\2\2\u01a3")
        buf.write("%\3\2\2\2\u01a4\u01a6\5(\25\2\u01a5\u01a4\3\2\2\2\u01a5")
        buf.write("\u01a6\3\2\2\2\u01a6\u01ab\3\2\2\2\u01a7\u01a8\7\13\2")
        buf.write("\2\u01a8\u01aa\5(\25\2\u01a9\u01a7\3\2\2\2\u01aa\u01ad")
        buf.write("\3\2\2\2\u01ab\u01a9\3\2\2\2\u01ab\u01ac\3\2\2\2\u01ac")
        buf.write("\'\3\2\2\2\u01ad\u01ab\3\2\2\2\u01ae\u01b2\5\"\22\2\u01af")
        buf.write("\u01b2\5\60\31\2\u01b0\u01b2\5\66\34\2\u01b1\u01ae\3\2")
        buf.write("\2\2\u01b1\u01af\3\2\2\2\u01b1\u01b0\3\2\2\2\u01b2\u01b4")
        buf.write("\3\2\2\2\u01b3\u01b5\7\f\2\2\u01b4\u01b3\3\2\2\2\u01b4")
        buf.write("\u01b5\3\2\2\2\u01b5)\3\2\2\2\u01b6\u01b7\t\2\2\2\u01b7")
        buf.write("+\3\2\2\2\u01b8\u01b9\5.\30\2\u01b9\u01ba\7\17\2\2\u01ba")
        buf.write("\u01bc\3\2\2\2\u01bb\u01b8\3\2\2\2\u01bc\u01bf\3\2\2\2")
        buf.write("\u01bd\u01bb\3\2\2\2\u01bd\u01be\3\2\2\2\u01be-\3\2\2")
        buf.write("\2\u01bf\u01bd\3\2\2\2\u01c0\u01c6\5b\62\2\u01c1\u01c7")
        buf.write("\5\60\31\2\u01c2\u01c3\7\4\2\2\u01c3\u01c4\5\62\32\2\u01c4")
        buf.write("\u01c5\7\5\2\2\u01c5\u01c7\3\2\2\2\u01c6\u01c1\3\2\2\2")
        buf.write("\u01c6\u01c2\3\2\2\2\u01c6\u01c7\3\2\2\2\u01c7/\3\2\2")
        buf.write("\2\u01c8\u01ca\7\20\2\2\u01c9\u01cb\7\20\2\2\u01ca\u01c9")
        buf.write("\3\2\2\2\u01ca\u01cb\3\2\2\2\u01cb\u01cc\3\2\2\2\u01cc")
        buf.write("\u01cd\7N\2\2\u01cd\61\3\2\2\2\u01ce\u01d3\5\64\33\2\u01cf")
        buf.write("\u01d0\7\13\2\2\u01d0\u01d2\5\64\33\2\u01d1\u01cf\3\2")
        buf.write("\2\2\u01d2\u01d5\3\2\2\2\u01d3\u01d1\3\2\2\2\u01d3\u01d4")
        buf.write("\3\2\2\2\u01d4\63\3\2\2\2\u01d5\u01d3\3\2\2\2\u01d6\u01da")
        buf.write("\5\"\22\2\u01d7\u01da\5\66\34\2\u01d8\u01da\5\60\31\2")
        buf.write("\u01d9\u01d6\3\2\2\2\u01d9\u01d7\3\2\2\2\u01d9\u01d8\3")
        buf.write("\2\2\2\u01da\u01dc\3\2\2\2\u01db\u01dd\5*\26\2\u01dc\u01db")
        buf.write("\3\2\2\2\u01dc\u01dd\3\2\2\2\u01dd\65\3\2\2\2\u01de\u01df")
        buf.write("\7\4\2\2\u01df\u01e0\5b\62\2\u01e0\u01e1\7\5\2\2\u01e1")
        buf.write("\67\3\2\2\2\u01e2\u01e4\5:\36\2\u01e3\u01e2\3\2\2\2\u01e4")
        buf.write("\u01e5\3\2\2\2\u01e5\u01e3\3\2\2\2\u01e5\u01e6\3\2\2\2")
        buf.write("\u01e69\3\2\2\2\u01e7\u01e8\7C\2\2\u01e8\u01e9\5^\60\2")
        buf.write("\u01e9\u01ea\7\21\2\2\u01ea\u01eb\5<\37\2\u01eb;\3\2\2")
        buf.write("\2\u01ec\u01f1\5> \2\u01ed\u01ee\7\13\2\2\u01ee\u01f0")
        buf.write("\5> \2\u01ef\u01ed\3\2\2\2\u01f0\u01f3\3\2\2\2\u01f1\u01ef")
        buf.write("\3\2\2\2\u01f1\u01f2\3\2\2\2\u01f2=\3\2\2\2\u01f3\u01f1")
        buf.write("\3\2\2\2\u01f4\u01f9\7\22\2\2\u01f5\u01f9\7&\2\2\u01f6")
        buf.write("\u01f9\7,\2\2\u01f7\u01f9\5V,\2\u01f8\u01f4\3\2\2\2\u01f8")
        buf.write("\u01f5\3\2\2\2\u01f8\u01f6\3\2\2\2\u01f8\u01f7\3\2\2\2")
        buf.write("\u01f9?\3\2\2\2\u01fa\u01fc\5B\"\2\u01fb\u01fa\3\2\2\2")
        buf.write("\u01fc\u01ff\3\2\2\2\u01fd\u01fb\3\2\2\2\u01fd\u01fe\3")
        buf.write("\2\2\2\u01feA\3\2\2\2\u01ff\u01fd\3\2\2\2\u0200\u0201")
        buf.write("\7\t\2\2\u0201\u0207\5^\60\2\u0202\u0204\7\7\2\2\u0203")
        buf.write("\u0205\5D#\2\u0204\u0203\3\2\2\2\u0204\u0205\3\2\2\2\u0205")
        buf.write("\u0206\3\2\2\2\u0206\u0208\7\b\2\2\u0207\u0202\3\2\2\2")
        buf.write("\u0207\u0208\3\2\2\2\u0208\u0209\3\2\2\2\u0209\u020a\7")
        buf.write("\n\2\2\u020aC\3\2\2\2\u020b\u0210\5F$\2\u020c\u020d\7")
        buf.write("\13\2\2\u020d\u020f\5F$\2\u020e\u020c\3\2\2\2\u020f\u0212")
        buf.write("\3\2\2\2\u0210\u020e\3\2\2\2\u0210\u0211\3\2\2\2\u0211")
        buf.write("E\3\2\2\2\u0212\u0210\3\2\2\2\u0213\u0214\5d\63\2\u0214")
        buf.write("\u0215\7\6\2\2\u0215\u0217\3\2\2\2\u0216\u0213\3\2\2\2")
        buf.write("\u0216\u0217\3\2\2\2\u0217\u0218\3\2\2\2\u0218\u0219\5")
        buf.write("L\'\2\u0219G\3\2\2\2\u021a\u021f\5J&\2\u021b\u021c\7\13")
        buf.write("\2\2\u021c\u021e\5J&\2\u021d\u021b\3\2\2\2\u021e\u0221")
        buf.write("\3\2\2\2\u021f\u021d\3\2\2\2\u021f\u0220\3\2\2\2\u0220")
        buf.write("I\3\2\2\2\u0221\u021f\3\2\2\2\u0222\u0224\5@!\2\u0223")
        buf.write("\u0222\3\2\2\2\u0223\u0224\3\2\2\2\u0224\u0225\3\2\2\2")
        buf.write("\u0225\u0226\5P)\2\u0226\u0227\5V,\2\u0227\u022a\5d\63")
        buf.write("\2\u0228\u0229\7\6\2\2\u0229\u022b\5L\'\2\u022a\u0228")
        buf.write("\3\2\2\2\u022a\u022b\3\2\2\2\u022bK\3\2\2\2\u022c\u023d")
        buf.write("\7\23\2\2\u022d\u023d\5t;\2\u022e\u023d\5v<\2\u022f\u023d")
        buf.write("\5p9\2\u0230\u023d\5r:\2\u0231\u0236\7D\2\2\u0232\u0233")
        buf.write("\7\7\2\2\u0233\u0234\5V,\2\u0234\u0235\7\b\2\2\u0235\u0237")
        buf.write("\3\2\2\2\u0236\u0232\3\2\2\2\u0236\u0237\3\2\2\2\u0237")
        buf.write("\u023d\3\2\2\2\u0238\u0239\7\24\2\2\u0239\u023a\5V,\2")
        buf.write("\u023a\u023b\7\b\2\2\u023b\u023d\3\2\2\2\u023c\u022c\3")
        buf.write("\2\2\2\u023c\u022d\3\2\2\2\u023c\u022e\3\2\2\2\u023c\u022f")
        buf.write("\3\2\2\2\u023c\u0230\3\2\2\2\u023c\u0231\3\2\2\2\u023c")
        buf.write("\u0238\3\2\2\2\u023dM\3\2\2\2\u023e\u0240\5@!\2\u023f")
        buf.write("\u023e\3\2\2\2\u023f\u0240\3\2\2\2\u0240\u0242\3\2\2\2")
        buf.write("\u0241\u0243\5x=\2\u0242\u0241\3\2\2\2\u0242\u0243\3\2")
        buf.write("\2\2\u0243\u0244\3\2\2\2\u0244\u0245\7I\2\2\u0245\u0247")
        buf.write("\7\3\2\2\u0246\u023f\3\2\2\2\u0246\u0247\3\2\2\2\u0247")
        buf.write("\u0250\3\2\2\2\u0248\u024a\5@!\2\u0249\u0248\3\2\2\2\u0249")
        buf.write("\u024a\3\2\2\2\u024a\u024c\3\2\2\2\u024b\u024d\5x=\2\u024c")
        buf.write("\u024b\3\2\2\2\u024c\u024d\3\2\2\2\u024d\u024e\3\2\2\2")
        buf.write("\u024e\u024f\7H\2\2\u024f\u0251\7\3\2\2\u0250\u0249\3")
        buf.write("\2\2\2\u0250\u0251\3\2\2\2\u0251O\3\2\2\2\u0252\u0254")
        buf.write("\t\3\2\2\u0253\u0252\3\2\2\2\u0254\u0257\3\2\2\2\u0255")
        buf.write("\u0253\3\2\2\2\u0255\u0256\3\2\2\2\u0256Q\3\2\2\2\u0257")
        buf.write("\u0255\3\2\2\2\u0258\u0259\7\21\2\2\u0259\u025e\5V,\2")
        buf.write("\u025a\u025b\7\13\2\2\u025b\u025d\5V,\2\u025c\u025a\3")
        buf.write("\2\2\2\u025d\u0260\3\2\2\2\u025e\u025c\3\2\2\2\u025e\u025f")
        buf.write("\3\2\2\2\u025fS\3\2\2\2\u0260\u025e\3\2\2\2\u0261\u0278")
        buf.write("\7\25\2\2\u0262\u0278\7\26\2\2\u0263\u0278\7\27\2\2\u0264")
        buf.write("\u0278\7\30\2\2\u0265\u0278\7\31\2\2\u0266\u0278\7\32")
        buf.write("\2\2\u0267\u0278\7\33\2\2\u0268\u0278\7\34\2\2\u0269\u0278")
        buf.write("\7\35\2\2\u026a\u0278\7\36\2\2\u026b\u026c\7\37\2\2\u026c")
        buf.write("\u0278\7\37\2\2\u026d\u026e\7 \2\2\u026e\u0278\7 \2\2")
        buf.write("\u026f\u0278\7!\2\2\u0270\u0278\7\"\2\2\u0271\u0278\7")
        buf.write("\37\2\2\u0272\u0278\7 \2\2\u0273\u0278\7#\2\2\u0274\u0278")
        buf.write("\7$\2\2\u0275\u0278\7J\2\2\u0276\u0278\7K\2\2\u0277\u0261")
        buf.write("\3\2\2\2\u0277\u0262\3\2\2\2\u0277\u0263\3\2\2\2\u0277")
        buf.write("\u0264\3\2\2\2\u0277\u0265\3\2\2\2\u0277\u0266\3\2\2\2")
        buf.write("\u0277\u0267\3\2\2\2\u0277\u0268\3\2\2\2\u0277\u0269\3")
        buf.write("\2\2\2\u0277\u026a\3\2\2\2\u0277\u026b\3\2\2\2\u0277\u026d")
        buf.write("\3\2\2\2\u0277\u026f\3\2\2\2\u0277\u0270\3\2\2\2\u0277")
        buf.write("\u0271\3\2\2\2\u0277\u0272\3\2\2\2\u0277\u0273\3\2\2\2")
        buf.write("\u0277\u0274\3\2\2\2\u0277\u0275\3\2\2\2\u0277\u0276\3")
        buf.write("\2\2\2\u0278U\3\2\2\2\u0279\u027c\5^\60\2\u027a\u027c")
        buf.write("\5\\/\2\u027b\u0279\3\2\2\2\u027b\u027a\3\2\2\2\u027c")
        buf.write("\u027e\3\2\2\2\u027d\u027f\5X-\2\u027e\u027d\3\2\2\2\u027e")
        buf.write("\u027f\3\2\2\2\u027f\u0281\3\2\2\2\u0280\u0282\5Z.\2\u0281")
        buf.write("\u0280\3\2\2\2\u0281\u0282\3\2\2\2\u0282W\3\2\2\2\u0283")
        buf.write("\u0284\7\16\2\2\u0284Y\3\2\2\2\u0285\u0286\7\r\2\2\u0286")
        buf.write("[\3\2\2\2\u0287\u0288\7\7\2\2\u0288\u028a\5V,\2\u0289")
        buf.write("\u028b\5d\63\2\u028a\u0289\3\2\2\2\u028a\u028b\3\2\2\2")
        buf.write("\u028b\u0293\3\2\2\2\u028c\u028d\7\13\2\2\u028d\u028f")
        buf.write("\5V,\2\u028e\u0290\5d\63\2\u028f\u028e\3\2\2\2\u028f\u0290")
        buf.write("\3\2\2\2\u0290\u0292\3\2\2\2\u0291\u028c\3\2\2\2\u0292")
        buf.write("\u0295\3\2\2\2\u0293\u0291\3\2\2\2\u0293\u0294\3\2\2\2")
        buf.write("\u0294\u0296\3\2\2\2\u0295\u0293\3\2\2\2\u0296\u0297\7")
        buf.write("\b\2\2\u0297]\3\2\2\2\u0298\u0299\5`\61\2\u0299\u029a")
        buf.write("\7\17\2\2\u029a\u029c\3\2\2\2\u029b\u0298\3\2\2\2\u029c")
        buf.write("\u029f\3\2\2\2\u029d\u029b\3\2\2\2\u029d\u029e\3\2\2\2")
        buf.write("\u029e\u02a0\3\2\2\2\u029f\u029d\3\2\2\2\u02a0\u02a1\5")
        buf.write("`\61\2\u02a1_\3\2\2\2\u02a2\u02a4\5b\62\2\u02a3\u02a5")
        buf.write("\5f\64\2\u02a4\u02a3\3\2\2\2\u02a4\u02a5\3\2\2\2\u02a5")
        buf.write("a\3\2\2\2\u02a6\u02a7\7Q\2\2\u02a7c\3\2\2\2\u02a8\u02a9")
        buf.write("\t\4\2\2\u02a9e\3\2\2\2\u02aa\u02ab\7\37\2\2\u02ab\u02b0")
        buf.write("\5h\65\2\u02ac\u02ad\7\13\2\2\u02ad\u02af\5h\65\2\u02ae")
        buf.write("\u02ac\3\2\2\2\u02af\u02b2\3\2\2\2\u02b0\u02ae\3\2\2\2")
        buf.write("\u02b0\u02b1\3\2\2\2\u02b1\u02b3\3\2\2\2\u02b2\u02b0\3")
        buf.write("\2\2\2\u02b3\u02b4\7 \2\2\u02b4g\3\2\2\2\u02b5\u02b7\5")
        buf.write("@!\2\u02b6\u02b5\3\2\2\2\u02b6\u02b7\3\2\2\2\u02b7\u02b9")
        buf.write("\3\2\2\2\u02b8\u02ba\5j\66\2\u02b9\u02b8\3\2\2\2\u02b9")
        buf.write("\u02ba\3\2\2\2\u02ba\u02bb\3\2\2\2\u02bb\u02bc\5V,\2\u02bc")
        buf.write("i\3\2\2\2\u02bd\u02bf\5l\67\2\u02be\u02bd\3\2\2\2\u02bf")
        buf.write("\u02c0\3\2\2\2\u02c0\u02be\3\2\2\2\u02c0\u02c1\3\2\2\2")
        buf.write("\u02c1k\3\2\2\2\u02c2\u02c3\t\5\2\2\u02c3m\3\2\2\2\u02c4")
        buf.write("\u02c5\7N\2\2\u02c5o\3\2\2\2\u02c6\u02c7\7L\2\2\u02c7")
        buf.write("q\3\2\2\2\u02c8\u02c9\5^\60\2\u02c9s\3\2\2\2\u02ca\u02cb")
        buf.write("\t\6\2\2\u02cbu\3\2\2\2\u02cc\u02cd\t\7\2\2\u02cdw\3\2")
        buf.write("\2\2\u02ce\u02d0\5z>\2\u02cf\u02ce\3\2\2\2\u02d0\u02d1")
        buf.write("\3\2\2\2\u02d1\u02cf\3\2\2\2\u02d1\u02d2\3\2\2\2\u02d2")
        buf.write("y\3\2\2\2\u02d3\u02d4\t\b\2\2\u02d4{\3\2\2\2v}\u0080\u0083")
        buf.write("\u0087\u008a\u008d\u0092\u0095\u0098\u009c\u00a1\u00a4")
        buf.write("\u00a7\u00ab\u00ae\u00b1\u00b6\u00b9\u00bc\u00c0\u00c3")
        buf.write("\u00c6\u00cb\u00ce\u00d2\u00d7\u00da\u00dd\u00e2\u00e7")
        buf.write("\u00ea\u00f3\u00f8\u00fb\u0101\u0104\u0109\u010e\u0111")
        buf.write("\u0116\u0119\u011f\u0123\u0126\u012b\u012e\u0133\u0137")
        buf.write("\u013c\u013f\u0142\u0148\u014c\u014f\u0154\u0157\u0162")
        buf.write("\u0167\u016a\u016d\u0174\u0179\u017c\u0180\u0185\u0189")
        buf.write("\u0190\u0192\u0199\u019c\u019e\u01a5\u01ab\u01b1\u01b4")
        buf.write("\u01bd\u01c6\u01ca\u01d3\u01d9\u01dc\u01e5\u01f1\u01f8")
        buf.write("\u01fd\u0204\u0207\u0210\u0216\u021f\u0223\u022a\u0236")
        buf.write("\u023c\u023f\u0242\u0246\u0249\u024c\u0250\u0255\u025e")
        buf.write("\u0277\u027b\u027e\u0281\u028a\u028f\u0293\u029d\u02a4")
        buf.write("\u02b0\u02b6\u02b9\u02c0\u02d1")
        return buf.getvalue()


class CSharpParser ( Parser ):

    grammarFileName = "CSharp.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "';'", "'{'", "'}'", "'='", "'('", "')'", 
                     "'['", "']'", "','", "'@'", "'?'", "'[]'", "'.'", "'`'", 
                     "':'", "'new()'", "'null'", "'typeof('", "'+'", "'-'", 
                     "'!'", "'++'", "'--'", "'*'", "'/'", "'&'", "'|'", 
                     "'^'", "'<'", "'>'", "'=='", "'!='", "'<='", "'>='", 
                     "<INVALID>", "'class'", "'delegate'", "'operator'", 
                     "'enum'", "'in'", "'out'", "'struct'", "'namespace'", 
                     "'interface'", "'event'", "'new'", "'public'", "'protected'", 
                     "'internal'", "'private'", "'readonly'", "'volatile'", 
                     "'virtual'", "'sealed'", "'override'", "'abstract'", 
                     "'static'", "'unsafe'", "'extern'", "'partial'", "'async'", 
                     "'params'", "'this'", "'ref'", "'where'", "'default'", 
                     "'const'", "'implicit'", "'explicit'", "'set'", "'get'", 
                     "'true'", "'false'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "WHITESPACES", 
                      "CLASS", "DELEGATE", "OPERATOR", "ENUM", "IN", "OUT", 
                      "STRUCT", "NAMESPACE", "INTERFACE", "EVENT", "NEW", 
                      "PUBLIC", "PROTECTED", "INTERNAL", "PRIVATE", "READONLY", 
                      "VOLATILE", "VIRTUAL", "SEALED", "OVERRIDE", "ABSTRACT", 
                      "STATIC", "UNSAFE", "EXTERN", "PARTIAL", "ASYNC", 
                      "PARAMS", "THIS", "REF", "WHERE", "DEFAULT", "CONST", 
                      "IMPLICIT", "EXPLICIT", "SET", "GET", "TRUE", "FALSE", 
                      "STRING_LITERAL", "UNTERMINATED_STRING_LITERAL", "INTEGER_VALUE", 
                      "DOUBLE_VALUE", "FLOAT_VALUE", "IDENTIFIER" ]

    RULE_class_signature = 0
    RULE_enum_signature = 1
    RULE_struct_signature = 2
    RULE_interface_signature = 3
    RULE_namespace_signature = 4
    RULE_event_signature = 5
    RULE_property_signature = 6
    RULE_field_signature = 7
    RULE_enummember_signature = 8
    RULE_method_signature = 9
    RULE_constructor_signature = 10
    RULE_delegate_signature = 11
    RULE_indexer_signature = 12
    RULE_conversion_signature = 13
    RULE_operator_signature = 14
    RULE_object_cross_reference = 15
    RULE_object_reference = 16
    RULE_object_reference_arguments = 17
    RULE_object_reference_argument_list = 18
    RULE_object_reference_argument = 19
    RULE_object_reference_mutator = 20
    RULE_object_reference_qualifier = 21
    RULE_unqualified_object_reference = 22
    RULE_generic_type_count = 23
    RULE_object_reference_generic_list = 24
    RULE_object_reference_generic = 25
    RULE_type_param_reference = 26
    RULE_type_parameter_constraints_clauses = 27
    RULE_type_parameter_constraints_clause = 28
    RULE_type_parameter_constraints = 29
    RULE_type_parameter_constraint = 30
    RULE_attributes = 31
    RULE_attribute = 32
    RULE_argument_list = 33
    RULE_argument = 34
    RULE_parameter_list = 35
    RULE_parameter = 36
    RULE_literal = 37
    RULE_accessor_declarations = 38
    RULE_parameter_modifiers = 39
    RULE_class_base = 40
    RULE_operator_name = 41
    RULE_type_ = 42
    RULE_type_array_decorator = 43
    RULE_type_nullable_decorator = 44
    RULE_type_tuple = 45
    RULE_type_name = 46
    RULE_unqualified_type = 47
    RULE_unqualified_type_name = 48
    RULE_parameter_name = 49
    RULE_type_parameter_list = 50
    RULE_type_parameter = 51
    RULE_type_parameter_modifiers = 52
    RULE_type_parameter_modifier = 53
    RULE_integral_value = 54
    RULE_string_literal = 55
    RULE_enum_literal = 56
    RULE_numeric_literal = 57
    RULE_bool_literal = 58
    RULE_modifiers = 59
    RULE_modifier = 60

    ruleNames =  [ "class_signature", "enum_signature", "struct_signature", 
                   "interface_signature", "namespace_signature", "event_signature", 
                   "property_signature", "field_signature", "enummember_signature", 
                   "method_signature", "constructor_signature", "delegate_signature", 
                   "indexer_signature", "conversion_signature", "operator_signature", 
                   "object_cross_reference", "object_reference", "object_reference_arguments", 
                   "object_reference_argument_list", "object_reference_argument", 
                   "object_reference_mutator", "object_reference_qualifier", 
                   "unqualified_object_reference", "generic_type_count", 
                   "object_reference_generic_list", "object_reference_generic", 
                   "type_param_reference", "type_parameter_constraints_clauses", 
                   "type_parameter_constraints_clause", "type_parameter_constraints", 
                   "type_parameter_constraint", "attributes", "attribute", 
                   "argument_list", "argument", "parameter_list", "parameter", 
                   "literal", "accessor_declarations", "parameter_modifiers", 
                   "class_base", "operator_name", "type_", "type_array_decorator", 
                   "type_nullable_decorator", "type_tuple", "type_name", 
                   "unqualified_type", "unqualified_type_name", "parameter_name", 
                   "type_parameter_list", "type_parameter", "type_parameter_modifiers", 
                   "type_parameter_modifier", "integral_value", "string_literal", 
                   "enum_literal", "numeric_literal", "bool_literal", "modifiers", 
                   "modifier" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    T__24=25
    T__25=26
    T__26=27
    T__27=28
    T__28=29
    T__29=30
    T__30=31
    T__31=32
    T__32=33
    T__33=34
    WHITESPACES=35
    CLASS=36
    DELEGATE=37
    OPERATOR=38
    ENUM=39
    IN=40
    OUT=41
    STRUCT=42
    NAMESPACE=43
    INTERFACE=44
    EVENT=45
    NEW=46
    PUBLIC=47
    PROTECTED=48
    INTERNAL=49
    PRIVATE=50
    READONLY=51
    VOLATILE=52
    VIRTUAL=53
    SEALED=54
    OVERRIDE=55
    ABSTRACT=56
    STATIC=57
    UNSAFE=58
    EXTERN=59
    PARTIAL=60
    ASYNC=61
    PARAMS=62
    THIS=63
    REF=64
    WHERE=65
    DEFAULT=66
    CONST=67
    IMPLICIT=68
    EXPLICIT=69
    SET=70
    GET=71
    TRUE=72
    FALSE=73
    STRING_LITERAL=74
    UNTERMINATED_STRING_LITERAL=75
    INTEGER_VALUE=76
    DOUBLE_VALUE=77
    FLOAT_VALUE=78
    IDENTIFIER=79

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class Class_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def CLASS(self):
            return self.getToken(CSharpParser.CLASS, 0)

        def class_base(self):
            return self.getTypedRuleContext(CSharpParser.Class_baseContext,0)


        def type_parameter_constraints_clauses(self):
            return self.getTypedRuleContext(CSharpParser.Type_parameter_constraints_clausesContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_class_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClass_signature" ):
                listener.enterClass_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClass_signature" ):
                listener.exitClass_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitClass_signature" ):
                return visitor.visitClass_signature(self)
            else:
                return visitor.visitChildren(self)




    def class_signature(self):

        localctx = CSharpParser.Class_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_class_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 123
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
            if la_ == 1:
                self.state = 122
                self.attributes()


            self.state = 126
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 125
                self.modifiers()


            self.state = 129
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.CLASS:
                self.state = 128
                self.match(CSharpParser.CLASS)


            self.state = 131
            self.type_name()
            self.state = 133
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__14:
                self.state = 132
                self.class_base()


            self.state = 136
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.WHERE:
                self.state = 135
                self.type_parameter_constraints_clauses()


            self.state = 139
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 138
                self.match(CSharpParser.T__0)


            self.state = 141
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Enum_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def ENUM(self):
            return self.getToken(CSharpParser.ENUM, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_enum_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEnum_signature" ):
                listener.enterEnum_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEnum_signature" ):
                listener.exitEnum_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEnum_signature" ):
                return visitor.visitEnum_signature(self)
            else:
                return visitor.visitChildren(self)




    def enum_signature(self):

        localctx = CSharpParser.Enum_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_enum_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 144
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                self.state = 143
                self.attributes()


            self.state = 147
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 146
                self.modifiers()


            self.state = 150
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.ENUM:
                self.state = 149
                self.match(CSharpParser.ENUM)


            self.state = 152
            self.type_name()
            self.state = 154
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 153
                self.match(CSharpParser.T__0)


            self.state = 156
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Struct_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def STRUCT(self):
            return self.getToken(CSharpParser.STRUCT, 0)

        def class_base(self):
            return self.getTypedRuleContext(CSharpParser.Class_baseContext,0)


        def type_parameter_constraints_clauses(self):
            return self.getTypedRuleContext(CSharpParser.Type_parameter_constraints_clausesContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_struct_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStruct_signature" ):
                listener.enterStruct_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStruct_signature" ):
                listener.exitStruct_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStruct_signature" ):
                return visitor.visitStruct_signature(self)
            else:
                return visitor.visitChildren(self)




    def struct_signature(self):

        localctx = CSharpParser.Struct_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_struct_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 159
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,10,self._ctx)
            if la_ == 1:
                self.state = 158
                self.attributes()


            self.state = 162
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 161
                self.modifiers()


            self.state = 165
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.STRUCT:
                self.state = 164
                self.match(CSharpParser.STRUCT)


            self.state = 167
            self.type_name()
            self.state = 169
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__14:
                self.state = 168
                self.class_base()


            self.state = 172
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.WHERE:
                self.state = 171
                self.type_parameter_constraints_clauses()


            self.state = 175
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 174
                self.match(CSharpParser.T__0)


            self.state = 177
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Interface_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def INTERFACE(self):
            return self.getToken(CSharpParser.INTERFACE, 0)

        def class_base(self):
            return self.getTypedRuleContext(CSharpParser.Class_baseContext,0)


        def type_parameter_constraints_clauses(self):
            return self.getTypedRuleContext(CSharpParser.Type_parameter_constraints_clausesContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_interface_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInterface_signature" ):
                listener.enterInterface_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInterface_signature" ):
                listener.exitInterface_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInterface_signature" ):
                return visitor.visitInterface_signature(self)
            else:
                return visitor.visitChildren(self)




    def interface_signature(self):

        localctx = CSharpParser.Interface_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_interface_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 180
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,16,self._ctx)
            if la_ == 1:
                self.state = 179
                self.attributes()


            self.state = 183
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 182
                self.modifiers()


            self.state = 186
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.INTERFACE:
                self.state = 185
                self.match(CSharpParser.INTERFACE)


            self.state = 188
            self.type_name()
            self.state = 190
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__14:
                self.state = 189
                self.class_base()


            self.state = 193
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.WHERE:
                self.state = 192
                self.type_parameter_constraints_clauses()


            self.state = 196
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 195
                self.match(CSharpParser.T__0)


            self.state = 198
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Namespace_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def NAMESPACE(self):
            return self.getToken(CSharpParser.NAMESPACE, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_namespace_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNamespace_signature" ):
                listener.enterNamespace_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNamespace_signature" ):
                listener.exitNamespace_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNamespace_signature" ):
                return visitor.visitNamespace_signature(self)
            else:
                return visitor.visitChildren(self)




    def namespace_signature(self):

        localctx = CSharpParser.Namespace_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_namespace_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 201
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,22,self._ctx)
            if la_ == 1:
                self.state = 200
                self.attributes()


            self.state = 204
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.NAMESPACE:
                self.state = 203
                self.match(CSharpParser.NAMESPACE)


            self.state = 206
            self.type_name()
            self.state = 208
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 207
                self.match(CSharpParser.T__0)


            self.state = 210
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Event_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def EVENT(self):
            return self.getToken(CSharpParser.EVENT, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_event_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEvent_signature" ):
                listener.enterEvent_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEvent_signature" ):
                listener.exitEvent_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEvent_signature" ):
                return visitor.visitEvent_signature(self)
            else:
                return visitor.visitChildren(self)




    def event_signature(self):

        localctx = CSharpParser.Event_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_event_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 213
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,25,self._ctx)
            if la_ == 1:
                self.state = 212
                self.attributes()


            self.state = 216
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 215
                self.modifiers()


            self.state = 219
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.EVENT:
                self.state = 218
                self.match(CSharpParser.EVENT)


            self.state = 221
            self.type_()
            self.state = 222
            self.type_name()
            self.state = 224
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 223
                self.match(CSharpParser.T__0)


            self.state = 226
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Property_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def accessor_declarations(self):
            return self.getTypedRuleContext(CSharpParser.Accessor_declarationsContext,0)


        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_property_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProperty_signature" ):
                listener.enterProperty_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProperty_signature" ):
                listener.exitProperty_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProperty_signature" ):
                return visitor.visitProperty_signature(self)
            else:
                return visitor.visitChildren(self)




    def property_signature(self):

        localctx = CSharpParser.Property_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_property_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 229
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,29,self._ctx)
            if la_ == 1:
                self.state = 228
                self.attributes()


            self.state = 232
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 231
                self.modifiers()


            self.state = 234
            self.type_()
            self.state = 235
            self.type_name()

            self.state = 236
            self.match(CSharpParser.T__1)
            self.state = 237
            self.accessor_declarations()
            self.state = 238
            self.match(CSharpParser.T__2)
            self.state = 241
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 240
                self.match(CSharpParser.T__0)


            self.state = 243
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Field_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def literal(self):
            return self.getTypedRuleContext(CSharpParser.LiteralContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_field_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterField_signature" ):
                listener.enterField_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitField_signature" ):
                listener.exitField_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitField_signature" ):
                return visitor.visitField_signature(self)
            else:
                return visitor.visitChildren(self)




    def field_signature(self):

        localctx = CSharpParser.Field_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_field_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 246
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,32,self._ctx)
            if la_ == 1:
                self.state = 245
                self.attributes()


            self.state = 249
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 248
                self.modifiers()


            self.state = 251
            self.type_()
            self.state = 252
            self.type_name()
            self.state = 255
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__3:
                self.state = 253
                self.match(CSharpParser.T__3)
                self.state = 254
                self.literal()


            self.state = 258
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 257
                self.match(CSharpParser.T__0)


            self.state = 260
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Enummember_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def literal(self):
            return self.getTypedRuleContext(CSharpParser.LiteralContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_enummember_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEnummember_signature" ):
                listener.enterEnummember_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEnummember_signature" ):
                listener.exitEnummember_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEnummember_signature" ):
                return visitor.visitEnummember_signature(self)
            else:
                return visitor.visitChildren(self)




    def enummember_signature(self):

        localctx = CSharpParser.Enummember_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_enummember_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 263
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,36,self._ctx)
            if la_ == 1:
                self.state = 262
                self.attributes()


            self.state = 265
            self.type_name()
            self.state = 268
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__3:
                self.state = 266
                self.match(CSharpParser.T__3)
                self.state = 267
                self.literal()


            self.state = 271
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 270
                self.match(CSharpParser.T__0)


            self.state = 273
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Method_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def parameter_list(self):
            return self.getTypedRuleContext(CSharpParser.Parameter_listContext,0)


        def type_parameter_constraints_clauses(self):
            return self.getTypedRuleContext(CSharpParser.Type_parameter_constraints_clausesContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_method_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMethod_signature" ):
                listener.enterMethod_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMethod_signature" ):
                listener.exitMethod_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMethod_signature" ):
                return visitor.visitMethod_signature(self)
            else:
                return visitor.visitChildren(self)




    def method_signature(self):

        localctx = CSharpParser.Method_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_method_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 276
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,39,self._ctx)
            if la_ == 1:
                self.state = 275
                self.attributes()


            self.state = 279
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 278
                self.modifiers()


            self.state = 281
            self.type_()
            self.state = 282
            self.type_name()
            self.state = 283
            self.match(CSharpParser.T__4)
            self.state = 285
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << CSharpParser.T__4) | (1 << CSharpParser.T__6) | (1 << CSharpParser.OUT) | (1 << CSharpParser.PARAMS) | (1 << CSharpParser.THIS))) != 0) or _la==CSharpParser.REF or _la==CSharpParser.IDENTIFIER:
                self.state = 284
                self.parameter_list()


            self.state = 287
            self.match(CSharpParser.T__5)
            self.state = 289
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.WHERE:
                self.state = 288
                self.type_parameter_constraints_clauses()


            self.state = 292
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 291
                self.match(CSharpParser.T__0)


            self.state = 294
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Constructor_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def parameter_list(self):
            return self.getTypedRuleContext(CSharpParser.Parameter_listContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_constructor_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConstructor_signature" ):
                listener.enterConstructor_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConstructor_signature" ):
                listener.exitConstructor_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConstructor_signature" ):
                return visitor.visitConstructor_signature(self)
            else:
                return visitor.visitChildren(self)




    def constructor_signature(self):

        localctx = CSharpParser.Constructor_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_constructor_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 297
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,44,self._ctx)
            if la_ == 1:
                self.state = 296
                self.attributes()


            self.state = 300
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 299
                self.modifiers()


            self.state = 302
            self.type_name()
            self.state = 303
            self.match(CSharpParser.T__4)
            self.state = 305
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << CSharpParser.T__4) | (1 << CSharpParser.T__6) | (1 << CSharpParser.OUT) | (1 << CSharpParser.PARAMS) | (1 << CSharpParser.THIS))) != 0) or _la==CSharpParser.REF or _la==CSharpParser.IDENTIFIER:
                self.state = 304
                self.parameter_list()


            self.state = 307
            self.match(CSharpParser.T__5)
            self.state = 309
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 308
                self.match(CSharpParser.T__0)


            self.state = 311
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Delegate_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def DELEGATE(self):
            return self.getToken(CSharpParser.DELEGATE, 0)

        def parameter_list(self):
            return self.getTypedRuleContext(CSharpParser.Parameter_listContext,0)


        def type_parameter_constraints_clauses(self):
            return self.getTypedRuleContext(CSharpParser.Type_parameter_constraints_clausesContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_delegate_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDelegate_signature" ):
                listener.enterDelegate_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDelegate_signature" ):
                listener.exitDelegate_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDelegate_signature" ):
                return visitor.visitDelegate_signature(self)
            else:
                return visitor.visitChildren(self)




    def delegate_signature(self):

        localctx = CSharpParser.Delegate_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_delegate_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 314
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,48,self._ctx)
            if la_ == 1:
                self.state = 313
                self.attributes()


            self.state = 317
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 316
                self.modifiers()


            self.state = 320
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.DELEGATE:
                self.state = 319
                self.match(CSharpParser.DELEGATE)


            self.state = 322
            self.type_()
            self.state = 323
            self.type_name()
            self.state = 324
            self.match(CSharpParser.T__4)
            self.state = 326
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << CSharpParser.T__4) | (1 << CSharpParser.T__6) | (1 << CSharpParser.OUT) | (1 << CSharpParser.PARAMS) | (1 << CSharpParser.THIS))) != 0) or _la==CSharpParser.REF or _la==CSharpParser.IDENTIFIER:
                self.state = 325
                self.parameter_list()


            self.state = 328
            self.match(CSharpParser.T__5)
            self.state = 330
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.WHERE:
                self.state = 329
                self.type_parameter_constraints_clauses()


            self.state = 333
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 332
                self.match(CSharpParser.T__0)


            self.state = 335
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Indexer_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def THIS(self):
            return self.getToken(CSharpParser.THIS, 0)

        def parameter(self):
            return self.getTypedRuleContext(CSharpParser.ParameterContext,0)


        def accessor_declarations(self):
            return self.getTypedRuleContext(CSharpParser.Accessor_declarationsContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_indexer_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIndexer_signature" ):
                listener.enterIndexer_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIndexer_signature" ):
                listener.exitIndexer_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIndexer_signature" ):
                return visitor.visitIndexer_signature(self)
            else:
                return visitor.visitChildren(self)




    def indexer_signature(self):

        localctx = CSharpParser.Indexer_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_indexer_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 338
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,54,self._ctx)
            if la_ == 1:
                self.state = 337
                self.attributes()


            self.state = 341
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 340
                self.modifiers()


            self.state = 343
            self.type_()
            self.state = 344
            self.match(CSharpParser.THIS)
            self.state = 345
            self.match(CSharpParser.T__6)
            self.state = 346
            self.parameter()
            self.state = 347
            self.match(CSharpParser.T__7)
            self.state = 348
            self.match(CSharpParser.T__1)
            self.state = 349
            self.accessor_declarations()
            self.state = 350
            self.match(CSharpParser.T__2)
            self.state = 352
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 351
                self.match(CSharpParser.T__0)


            self.state = 354
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Conversion_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def parameter(self):
            return self.getTypedRuleContext(CSharpParser.ParameterContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def OPERATOR(self):
            return self.getToken(CSharpParser.OPERATOR, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_conversion_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConversion_signature" ):
                listener.enterConversion_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConversion_signature" ):
                listener.exitConversion_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConversion_signature" ):
                return visitor.visitConversion_signature(self)
            else:
                return visitor.visitChildren(self)




    def conversion_signature(self):

        localctx = CSharpParser.Conversion_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_conversion_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 357
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,57,self._ctx)
            if la_ == 1:
                self.state = 356
                self.attributes()


            self.state = 360
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 359
                self.modifiers()


            self.state = 363
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.OPERATOR:
                self.state = 362
                self.match(CSharpParser.OPERATOR)


            self.state = 365
            self.type_()
            self.state = 366
            self.match(CSharpParser.T__4)
            self.state = 367
            self.parameter()
            self.state = 368
            self.match(CSharpParser.T__5)
            self.state = 370
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 369
                self.match(CSharpParser.T__0)


            self.state = 372
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Operator_signatureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def operator_name(self):
            return self.getTypedRuleContext(CSharpParser.Operator_nameContext,0)


        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(CSharpParser.ModifiersContext,0)


        def OPERATOR(self):
            return self.getToken(CSharpParser.OPERATOR, 0)

        def parameter_list(self):
            return self.getTypedRuleContext(CSharpParser.Parameter_listContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_operator_signature

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOperator_signature" ):
                listener.enterOperator_signature(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOperator_signature" ):
                listener.exitOperator_signature(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOperator_signature" ):
                return visitor.visitOperator_signature(self)
            else:
                return visitor.visitChildren(self)




    def operator_signature(self):

        localctx = CSharpParser.Operator_signatureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_operator_signature)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 375
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,61,self._ctx)
            if la_ == 1:
                self.state = 374
                self.attributes()


            self.state = 378
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                self.state = 377
                self.modifiers()


            self.state = 380
            self.type_()
            self.state = 382
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.OPERATOR:
                self.state = 381
                self.match(CSharpParser.OPERATOR)


            self.state = 384
            self.operator_name()
            self.state = 385
            self.match(CSharpParser.T__4)
            self.state = 387
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << CSharpParser.T__4) | (1 << CSharpParser.T__6) | (1 << CSharpParser.OUT) | (1 << CSharpParser.PARAMS) | (1 << CSharpParser.THIS))) != 0) or _la==CSharpParser.REF or _la==CSharpParser.IDENTIFIER:
                self.state = 386
                self.parameter_list()


            self.state = 389
            self.match(CSharpParser.T__5)
            self.state = 391
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__0:
                self.state = 390
                self.match(CSharpParser.T__0)


            self.state = 393
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Object_cross_referenceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(CSharpParser.EOF, 0)

        def object_reference(self):
            return self.getTypedRuleContext(CSharpParser.Object_referenceContext,0)


        def type_param_reference(self):
            return self.getTypedRuleContext(CSharpParser.Type_param_referenceContext,0)


        def object_reference_mutator(self):
            return self.getTypedRuleContext(CSharpParser.Object_reference_mutatorContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_object_cross_reference

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObject_cross_reference" ):
                listener.enterObject_cross_reference(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObject_cross_reference" ):
                listener.exitObject_cross_reference(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitObject_cross_reference" ):
                return visitor.visitObject_cross_reference(self)
            else:
                return visitor.visitChildren(self)




    def object_cross_reference(self):

        localctx = CSharpParser.Object_cross_referenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_object_cross_reference)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 400
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CSharpParser.IDENTIFIER]:
                self.state = 395
                self.object_reference()
                pass
            elif token in [CSharpParser.T__1]:
                self.state = 396
                self.type_param_reference()
                self.state = 398
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==CSharpParser.T__10 or _la==CSharpParser.T__11:
                    self.state = 397
                    self.object_reference_mutator()


                pass
            else:
                raise NoViableAltException(self)

            self.state = 402
            self.match(CSharpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Object_referenceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def object_reference_qualifier(self):
            return self.getTypedRuleContext(CSharpParser.Object_reference_qualifierContext,0)


        def unqualified_object_reference(self):
            return self.getTypedRuleContext(CSharpParser.Unqualified_object_referenceContext,0)


        def object_reference_mutator(self):
            return self.getTypedRuleContext(CSharpParser.Object_reference_mutatorContext,0)


        def object_reference_arguments(self):
            return self.getTypedRuleContext(CSharpParser.Object_reference_argumentsContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_object_reference

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObject_reference" ):
                listener.enterObject_reference(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObject_reference" ):
                listener.exitObject_reference(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitObject_reference" ):
                return visitor.visitObject_reference(self)
            else:
                return visitor.visitChildren(self)




    def object_reference(self):

        localctx = CSharpParser.Object_referenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_object_reference)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 404
            self.object_reference_qualifier()

            self.state = 405
            self.unqualified_object_reference()
            self.state = 412
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,70,self._ctx)
            if la_ == 1:
                self.state = 407
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,68,self._ctx)
                if la_ == 1:
                    self.state = 406
                    self.object_reference_mutator()


                pass

            elif la_ == 2:
                self.state = 410
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==CSharpParser.T__4:
                    self.state = 409
                    self.object_reference_arguments()


                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Object_reference_argumentsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def object_reference_argument_list(self):
            return self.getTypedRuleContext(CSharpParser.Object_reference_argument_listContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_object_reference_arguments

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObject_reference_arguments" ):
                listener.enterObject_reference_arguments(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObject_reference_arguments" ):
                listener.exitObject_reference_arguments(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitObject_reference_arguments" ):
                return visitor.visitObject_reference_arguments(self)
            else:
                return visitor.visitChildren(self)




    def object_reference_arguments(self):

        localctx = CSharpParser.Object_reference_argumentsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_object_reference_arguments)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 414
            self.match(CSharpParser.T__4)
            self.state = 415
            self.object_reference_argument_list()
            self.state = 416
            self.match(CSharpParser.T__5)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Object_reference_argument_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def object_reference_argument(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.Object_reference_argumentContext)
            else:
                return self.getTypedRuleContext(CSharpParser.Object_reference_argumentContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_object_reference_argument_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObject_reference_argument_list" ):
                listener.enterObject_reference_argument_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObject_reference_argument_list" ):
                listener.exitObject_reference_argument_list(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitObject_reference_argument_list" ):
                return visitor.visitObject_reference_argument_list(self)
            else:
                return visitor.visitChildren(self)




    def object_reference_argument_list(self):

        localctx = CSharpParser.Object_reference_argument_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_object_reference_argument_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 419
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__1 or _la==CSharpParser.T__13 or _la==CSharpParser.IDENTIFIER:
                self.state = 418
                self.object_reference_argument()


            self.state = 425
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CSharpParser.T__8:
                self.state = 421
                self.match(CSharpParser.T__8)
                self.state = 422
                self.object_reference_argument()
                self.state = 427
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Object_reference_argumentContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def object_reference(self):
            return self.getTypedRuleContext(CSharpParser.Object_referenceContext,0)


        def generic_type_count(self):
            return self.getTypedRuleContext(CSharpParser.Generic_type_countContext,0)


        def type_param_reference(self):
            return self.getTypedRuleContext(CSharpParser.Type_param_referenceContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_object_reference_argument

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObject_reference_argument" ):
                listener.enterObject_reference_argument(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObject_reference_argument" ):
                listener.exitObject_reference_argument(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitObject_reference_argument" ):
                return visitor.visitObject_reference_argument(self)
            else:
                return visitor.visitChildren(self)




    def object_reference_argument(self):

        localctx = CSharpParser.Object_reference_argumentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_object_reference_argument)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 431
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CSharpParser.IDENTIFIER]:
                self.state = 428
                self.object_reference()
                pass
            elif token in [CSharpParser.T__13]:
                self.state = 429
                self.generic_type_count()
                pass
            elif token in [CSharpParser.T__1]:
                self.state = 430
                self.type_param_reference()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 434
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__9:
                self.state = 433
                self.match(CSharpParser.T__9)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Object_reference_mutatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return CSharpParser.RULE_object_reference_mutator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObject_reference_mutator" ):
                listener.enterObject_reference_mutator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObject_reference_mutator" ):
                listener.exitObject_reference_mutator(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitObject_reference_mutator" ):
                return visitor.visitObject_reference_mutator(self)
            else:
                return visitor.visitChildren(self)




    def object_reference_mutator(self):

        localctx = CSharpParser.Object_reference_mutatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_object_reference_mutator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 436
            _la = self._input.LA(1)
            if not(_la==CSharpParser.T__10 or _la==CSharpParser.T__11):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Object_reference_qualifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def unqualified_object_reference(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.Unqualified_object_referenceContext)
            else:
                return self.getTypedRuleContext(CSharpParser.Unqualified_object_referenceContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_object_reference_qualifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObject_reference_qualifier" ):
                listener.enterObject_reference_qualifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObject_reference_qualifier" ):
                listener.exitObject_reference_qualifier(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitObject_reference_qualifier" ):
                return visitor.visitObject_reference_qualifier(self)
            else:
                return visitor.visitChildren(self)




    def object_reference_qualifier(self):

        localctx = CSharpParser.Object_reference_qualifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_object_reference_qualifier)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 443
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,75,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 438
                    self.unqualified_object_reference()
                    self.state = 439
                    self.match(CSharpParser.T__12) 
                self.state = 445
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,75,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Unqualified_object_referenceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def unqualified_type_name(self):
            return self.getTypedRuleContext(CSharpParser.Unqualified_type_nameContext,0)


        def generic_type_count(self):
            return self.getTypedRuleContext(CSharpParser.Generic_type_countContext,0)


        def object_reference_generic_list(self):
            return self.getTypedRuleContext(CSharpParser.Object_reference_generic_listContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_unqualified_object_reference

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnqualified_object_reference" ):
                listener.enterUnqualified_object_reference(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnqualified_object_reference" ):
                listener.exitUnqualified_object_reference(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnqualified_object_reference" ):
                return visitor.visitUnqualified_object_reference(self)
            else:
                return visitor.visitChildren(self)




    def unqualified_object_reference(self):

        localctx = CSharpParser.Unqualified_object_referenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_unqualified_object_reference)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 446
            self.unqualified_type_name()
            self.state = 452
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CSharpParser.T__13]:
                self.state = 447
                self.generic_type_count()
                pass
            elif token in [CSharpParser.T__1]:
                self.state = 448
                self.match(CSharpParser.T__1)
                self.state = 449
                self.object_reference_generic_list()
                self.state = 450
                self.match(CSharpParser.T__2)
                pass
            elif token in [CSharpParser.EOF, CSharpParser.T__2, CSharpParser.T__4, CSharpParser.T__5, CSharpParser.T__8, CSharpParser.T__9, CSharpParser.T__10, CSharpParser.T__11, CSharpParser.T__12]:
                pass
            else:
                pass
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Generic_type_countContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTEGER_VALUE(self):
            return self.getToken(CSharpParser.INTEGER_VALUE, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_generic_type_count

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGeneric_type_count" ):
                listener.enterGeneric_type_count(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGeneric_type_count" ):
                listener.exitGeneric_type_count(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGeneric_type_count" ):
                return visitor.visitGeneric_type_count(self)
            else:
                return visitor.visitChildren(self)




    def generic_type_count(self):

        localctx = CSharpParser.Generic_type_countContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_generic_type_count)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 454
            self.match(CSharpParser.T__13)
            self.state = 456
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__13:
                self.state = 455
                self.match(CSharpParser.T__13)


            self.state = 458
            self.match(CSharpParser.INTEGER_VALUE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Object_reference_generic_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def object_reference_generic(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.Object_reference_genericContext)
            else:
                return self.getTypedRuleContext(CSharpParser.Object_reference_genericContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_object_reference_generic_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObject_reference_generic_list" ):
                listener.enterObject_reference_generic_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObject_reference_generic_list" ):
                listener.exitObject_reference_generic_list(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitObject_reference_generic_list" ):
                return visitor.visitObject_reference_generic_list(self)
            else:
                return visitor.visitChildren(self)




    def object_reference_generic_list(self):

        localctx = CSharpParser.Object_reference_generic_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_object_reference_generic_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 460
            self.object_reference_generic()
            self.state = 465
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CSharpParser.T__8:
                self.state = 461
                self.match(CSharpParser.T__8)
                self.state = 462
                self.object_reference_generic()
                self.state = 467
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Object_reference_genericContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def object_reference(self):
            return self.getTypedRuleContext(CSharpParser.Object_referenceContext,0)


        def type_param_reference(self):
            return self.getTypedRuleContext(CSharpParser.Type_param_referenceContext,0)


        def generic_type_count(self):
            return self.getTypedRuleContext(CSharpParser.Generic_type_countContext,0)


        def object_reference_mutator(self):
            return self.getTypedRuleContext(CSharpParser.Object_reference_mutatorContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_object_reference_generic

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObject_reference_generic" ):
                listener.enterObject_reference_generic(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObject_reference_generic" ):
                listener.exitObject_reference_generic(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitObject_reference_generic" ):
                return visitor.visitObject_reference_generic(self)
            else:
                return visitor.visitChildren(self)




    def object_reference_generic(self):

        localctx = CSharpParser.Object_reference_genericContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_object_reference_generic)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 471
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CSharpParser.IDENTIFIER]:
                self.state = 468
                self.object_reference()
                pass
            elif token in [CSharpParser.T__1]:
                self.state = 469
                self.type_param_reference()
                pass
            elif token in [CSharpParser.T__13]:
                self.state = 470
                self.generic_type_count()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 474
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__10 or _la==CSharpParser.T__11:
                self.state = 473
                self.object_reference_mutator()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_param_referenceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def unqualified_type_name(self):
            return self.getTypedRuleContext(CSharpParser.Unqualified_type_nameContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_type_param_reference

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_param_reference" ):
                listener.enterType_param_reference(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_param_reference" ):
                listener.exitType_param_reference(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_param_reference" ):
                return visitor.visitType_param_reference(self)
            else:
                return visitor.visitChildren(self)




    def type_param_reference(self):

        localctx = CSharpParser.Type_param_referenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_type_param_reference)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 476
            self.match(CSharpParser.T__1)
            self.state = 477
            self.unqualified_type_name()
            self.state = 478
            self.match(CSharpParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_parameter_constraints_clausesContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_parameter_constraints_clause(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.Type_parameter_constraints_clauseContext)
            else:
                return self.getTypedRuleContext(CSharpParser.Type_parameter_constraints_clauseContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_type_parameter_constraints_clauses

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_parameter_constraints_clauses" ):
                listener.enterType_parameter_constraints_clauses(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_parameter_constraints_clauses" ):
                listener.exitType_parameter_constraints_clauses(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_parameter_constraints_clauses" ):
                return visitor.visitType_parameter_constraints_clauses(self)
            else:
                return visitor.visitChildren(self)




    def type_parameter_constraints_clauses(self):

        localctx = CSharpParser.Type_parameter_constraints_clausesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_type_parameter_constraints_clauses)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 481 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 480
                self.type_parameter_constraints_clause()
                self.state = 483 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==CSharpParser.WHERE):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_parameter_constraints_clauseContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WHERE(self):
            return self.getToken(CSharpParser.WHERE, 0)

        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def type_parameter_constraints(self):
            return self.getTypedRuleContext(CSharpParser.Type_parameter_constraintsContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_type_parameter_constraints_clause

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_parameter_constraints_clause" ):
                listener.enterType_parameter_constraints_clause(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_parameter_constraints_clause" ):
                listener.exitType_parameter_constraints_clause(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_parameter_constraints_clause" ):
                return visitor.visitType_parameter_constraints_clause(self)
            else:
                return visitor.visitChildren(self)




    def type_parameter_constraints_clause(self):

        localctx = CSharpParser.Type_parameter_constraints_clauseContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_type_parameter_constraints_clause)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 485
            self.match(CSharpParser.WHERE)
            self.state = 486
            self.type_name()
            self.state = 487
            self.match(CSharpParser.T__14)
            self.state = 488
            self.type_parameter_constraints()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_parameter_constraintsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_parameter_constraint(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.Type_parameter_constraintContext)
            else:
                return self.getTypedRuleContext(CSharpParser.Type_parameter_constraintContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_type_parameter_constraints

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_parameter_constraints" ):
                listener.enterType_parameter_constraints(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_parameter_constraints" ):
                listener.exitType_parameter_constraints(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_parameter_constraints" ):
                return visitor.visitType_parameter_constraints(self)
            else:
                return visitor.visitChildren(self)




    def type_parameter_constraints(self):

        localctx = CSharpParser.Type_parameter_constraintsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_type_parameter_constraints)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 490
            self.type_parameter_constraint()
            self.state = 495
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CSharpParser.T__8:
                self.state = 491
                self.match(CSharpParser.T__8)
                self.state = 492
                self.type_parameter_constraint()
                self.state = 497
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_parameter_constraintContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CLASS(self):
            return self.getToken(CSharpParser.CLASS, 0)

        def STRUCT(self):
            return self.getToken(CSharpParser.STRUCT, 0)

        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_type_parameter_constraint

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_parameter_constraint" ):
                listener.enterType_parameter_constraint(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_parameter_constraint" ):
                listener.exitType_parameter_constraint(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_parameter_constraint" ):
                return visitor.visitType_parameter_constraint(self)
            else:
                return visitor.visitChildren(self)




    def type_parameter_constraint(self):

        localctx = CSharpParser.Type_parameter_constraintContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_type_parameter_constraint)
        try:
            self.state = 502
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CSharpParser.T__15]:
                self.enterOuterAlt(localctx, 1)
                self.state = 498
                self.match(CSharpParser.T__15)
                pass
            elif token in [CSharpParser.CLASS]:
                self.enterOuterAlt(localctx, 2)
                self.state = 499
                self.match(CSharpParser.CLASS)
                pass
            elif token in [CSharpParser.STRUCT]:
                self.enterOuterAlt(localctx, 3)
                self.state = 500
                self.match(CSharpParser.STRUCT)
                pass
            elif token in [CSharpParser.T__4, CSharpParser.IDENTIFIER]:
                self.enterOuterAlt(localctx, 4)
                self.state = 501
                self.type_()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AttributesContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def attribute(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.AttributeContext)
            else:
                return self.getTypedRuleContext(CSharpParser.AttributeContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_attributes

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAttributes" ):
                listener.enterAttributes(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAttributes" ):
                listener.exitAttributes(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAttributes" ):
                return visitor.visitAttributes(self)
            else:
                return visitor.visitChildren(self)




    def attributes(self):

        localctx = CSharpParser.AttributesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_attributes)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 507
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CSharpParser.T__6:
                self.state = 504
                self.attribute()
                self.state = 509
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AttributeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def argument_list(self):
            return self.getTypedRuleContext(CSharpParser.Argument_listContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_attribute

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAttribute" ):
                listener.enterAttribute(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAttribute" ):
                listener.exitAttribute(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAttribute" ):
                return visitor.visitAttribute(self)
            else:
                return visitor.visitChildren(self)




    def attribute(self):

        localctx = CSharpParser.AttributeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_attribute)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 510
            self.match(CSharpParser.T__6)
            self.state = 511
            self.type_name()
            self.state = 517
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__4:
                self.state = 512
                self.match(CSharpParser.T__4)
                self.state = 514
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if ((((_la - 17)) & ~0x3f) == 0 and ((1 << (_la - 17)) & ((1 << (CSharpParser.T__16 - 17)) | (1 << (CSharpParser.T__17 - 17)) | (1 << (CSharpParser.DEFAULT - 17)) | (1 << (CSharpParser.SET - 17)) | (1 << (CSharpParser.GET - 17)) | (1 << (CSharpParser.TRUE - 17)) | (1 << (CSharpParser.FALSE - 17)) | (1 << (CSharpParser.STRING_LITERAL - 17)) | (1 << (CSharpParser.INTEGER_VALUE - 17)) | (1 << (CSharpParser.DOUBLE_VALUE - 17)) | (1 << (CSharpParser.FLOAT_VALUE - 17)) | (1 << (CSharpParser.IDENTIFIER - 17)))) != 0):
                    self.state = 513
                    self.argument_list()


                self.state = 516
                self.match(CSharpParser.T__5)


            self.state = 519
            self.match(CSharpParser.T__7)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Argument_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def argument(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.ArgumentContext)
            else:
                return self.getTypedRuleContext(CSharpParser.ArgumentContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_argument_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgument_list" ):
                listener.enterArgument_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgument_list" ):
                listener.exitArgument_list(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArgument_list" ):
                return visitor.visitArgument_list(self)
            else:
                return visitor.visitChildren(self)




    def argument_list(self):

        localctx = CSharpParser.Argument_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_argument_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 521
            self.argument()
            self.state = 526
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CSharpParser.T__8:
                self.state = 522
                self.match(CSharpParser.T__8)
                self.state = 523
                self.argument()
                self.state = 528
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArgumentContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def literal(self):
            return self.getTypedRuleContext(CSharpParser.LiteralContext,0)


        def parameter_name(self):
            return self.getTypedRuleContext(CSharpParser.Parameter_nameContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_argument

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgument" ):
                listener.enterArgument(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgument" ):
                listener.exitArgument(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArgument" ):
                return visitor.visitArgument(self)
            else:
                return visitor.visitChildren(self)




    def argument(self):

        localctx = CSharpParser.ArgumentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_argument)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 532
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,88,self._ctx)
            if la_ == 1:
                self.state = 529
                self.parameter_name()
                self.state = 530
                self.match(CSharpParser.T__3)


            self.state = 534
            self.literal()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Parameter_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def parameter(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.ParameterContext)
            else:
                return self.getTypedRuleContext(CSharpParser.ParameterContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_parameter_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameter_list" ):
                listener.enterParameter_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameter_list" ):
                listener.exitParameter_list(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParameter_list" ):
                return visitor.visitParameter_list(self)
            else:
                return visitor.visitChildren(self)




    def parameter_list(self):

        localctx = CSharpParser.Parameter_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 70, self.RULE_parameter_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 536
            self.parameter()
            self.state = 541
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CSharpParser.T__8:
                self.state = 537
                self.match(CSharpParser.T__8)
                self.state = 538
                self.parameter()
                self.state = 543
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParameterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def parameter_modifiers(self):
            return self.getTypedRuleContext(CSharpParser.Parameter_modifiersContext,0)


        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def parameter_name(self):
            return self.getTypedRuleContext(CSharpParser.Parameter_nameContext,0)


        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def literal(self):
            return self.getTypedRuleContext(CSharpParser.LiteralContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_parameter

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameter" ):
                listener.enterParameter(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameter" ):
                listener.exitParameter(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParameter" ):
                return visitor.visitParameter(self)
            else:
                return visitor.visitChildren(self)




    def parameter(self):

        localctx = CSharpParser.ParameterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 72, self.RULE_parameter)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 545
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,90,self._ctx)
            if la_ == 1:
                self.state = 544
                self.attributes()


            self.state = 547
            self.parameter_modifiers()
            self.state = 548
            self.type_()
            self.state = 549
            self.parameter_name()
            self.state = 552
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__3:
                self.state = 550
                self.match(CSharpParser.T__3)
                self.state = 551
                self.literal()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LiteralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def numeric_literal(self):
            return self.getTypedRuleContext(CSharpParser.Numeric_literalContext,0)


        def bool_literal(self):
            return self.getTypedRuleContext(CSharpParser.Bool_literalContext,0)


        def string_literal(self):
            return self.getTypedRuleContext(CSharpParser.String_literalContext,0)


        def enum_literal(self):
            return self.getTypedRuleContext(CSharpParser.Enum_literalContext,0)


        def DEFAULT(self):
            return self.getToken(CSharpParser.DEFAULT, 0)

        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_literal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLiteral" ):
                listener.enterLiteral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLiteral" ):
                listener.exitLiteral(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLiteral" ):
                return visitor.visitLiteral(self)
            else:
                return visitor.visitChildren(self)




    def literal(self):

        localctx = CSharpParser.LiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 74, self.RULE_literal)
        self._la = 0 # Token type
        try:
            self.state = 570
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CSharpParser.T__16]:
                self.enterOuterAlt(localctx, 1)
                self.state = 554
                self.match(CSharpParser.T__16)
                pass
            elif token in [CSharpParser.INTEGER_VALUE, CSharpParser.DOUBLE_VALUE, CSharpParser.FLOAT_VALUE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 555
                self.numeric_literal()
                pass
            elif token in [CSharpParser.TRUE, CSharpParser.FALSE]:
                self.enterOuterAlt(localctx, 3)
                self.state = 556
                self.bool_literal()
                pass
            elif token in [CSharpParser.STRING_LITERAL]:
                self.enterOuterAlt(localctx, 4)
                self.state = 557
                self.string_literal()
                pass
            elif token in [CSharpParser.IDENTIFIER]:
                self.enterOuterAlt(localctx, 5)
                self.state = 558
                self.enum_literal()
                pass
            elif token in [CSharpParser.DEFAULT]:
                self.enterOuterAlt(localctx, 6)
                self.state = 559
                self.match(CSharpParser.DEFAULT)
                self.state = 564
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==CSharpParser.T__4:
                    self.state = 560
                    self.match(CSharpParser.T__4)
                    self.state = 561
                    self.type_()
                    self.state = 562
                    self.match(CSharpParser.T__5)


                pass
            elif token in [CSharpParser.T__17]:
                self.enterOuterAlt(localctx, 7)
                self.state = 566
                self.match(CSharpParser.T__17)
                self.state = 567
                self.type_()
                self.state = 568
                self.match(CSharpParser.T__5)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Accessor_declarationsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def GET(self):
            return self.getToken(CSharpParser.GET, 0)

        def SET(self):
            return self.getToken(CSharpParser.SET, 0)

        def attributes(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.AttributesContext)
            else:
                return self.getTypedRuleContext(CSharpParser.AttributesContext,i)


        def modifiers(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.ModifiersContext)
            else:
                return self.getTypedRuleContext(CSharpParser.ModifiersContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_accessor_declarations

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAccessor_declarations" ):
                listener.enterAccessor_declarations(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAccessor_declarations" ):
                listener.exitAccessor_declarations(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAccessor_declarations" ):
                return visitor.visitAccessor_declarations(self)
            else:
                return visitor.visitChildren(self)




    def accessor_declarations(self):

        localctx = CSharpParser.Accessor_declarationsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 76, self.RULE_accessor_declarations)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 580
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,96,self._ctx)
            if la_ == 1:
                self.state = 573
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,94,self._ctx)
                if la_ == 1:
                    self.state = 572
                    self.attributes()


                self.state = 576
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                    self.state = 575
                    self.modifiers()


                self.state = 578
                self.match(CSharpParser.GET)
                self.state = 579
                self.match(CSharpParser.T__0)


            self.state = 590
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 7)) & ~0x3f) == 0 and ((1 << (_la - 7)) & ((1 << (CSharpParser.T__6 - 7)) | (1 << (CSharpParser.NEW - 7)) | (1 << (CSharpParser.PUBLIC - 7)) | (1 << (CSharpParser.PROTECTED - 7)) | (1 << (CSharpParser.INTERNAL - 7)) | (1 << (CSharpParser.PRIVATE - 7)) | (1 << (CSharpParser.READONLY - 7)) | (1 << (CSharpParser.VOLATILE - 7)) | (1 << (CSharpParser.VIRTUAL - 7)) | (1 << (CSharpParser.SEALED - 7)) | (1 << (CSharpParser.OVERRIDE - 7)) | (1 << (CSharpParser.ABSTRACT - 7)) | (1 << (CSharpParser.STATIC - 7)) | (1 << (CSharpParser.UNSAFE - 7)) | (1 << (CSharpParser.EXTERN - 7)) | (1 << (CSharpParser.PARTIAL - 7)) | (1 << (CSharpParser.ASYNC - 7)) | (1 << (CSharpParser.PARAMS - 7)) | (1 << (CSharpParser.THIS - 7)) | (1 << (CSharpParser.CONST - 7)) | (1 << (CSharpParser.IMPLICIT - 7)) | (1 << (CSharpParser.EXPLICIT - 7)) | (1 << (CSharpParser.SET - 7)))) != 0):
                self.state = 583
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,97,self._ctx)
                if la_ == 1:
                    self.state = 582
                    self.attributes()


                self.state = 586
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if ((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0):
                    self.state = 585
                    self.modifiers()


                self.state = 588
                self.match(CSharpParser.SET)
                self.state = 589
                self.match(CSharpParser.T__0)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Parameter_modifiersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def PARAMS(self, i:int=None):
            if i is None:
                return self.getTokens(CSharpParser.PARAMS)
            else:
                return self.getToken(CSharpParser.PARAMS, i)

        def THIS(self, i:int=None):
            if i is None:
                return self.getTokens(CSharpParser.THIS)
            else:
                return self.getToken(CSharpParser.THIS, i)

        def OUT(self, i:int=None):
            if i is None:
                return self.getTokens(CSharpParser.OUT)
            else:
                return self.getToken(CSharpParser.OUT, i)

        def REF(self, i:int=None):
            if i is None:
                return self.getTokens(CSharpParser.REF)
            else:
                return self.getToken(CSharpParser.REF, i)

        def getRuleIndex(self):
            return CSharpParser.RULE_parameter_modifiers

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameter_modifiers" ):
                listener.enterParameter_modifiers(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameter_modifiers" ):
                listener.exitParameter_modifiers(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParameter_modifiers" ):
                return visitor.visitParameter_modifiers(self)
            else:
                return visitor.visitChildren(self)




    def parameter_modifiers(self):

        localctx = CSharpParser.Parameter_modifiersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 78, self.RULE_parameter_modifiers)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 595
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 41)) & ~0x3f) == 0 and ((1 << (_la - 41)) & ((1 << (CSharpParser.OUT - 41)) | (1 << (CSharpParser.PARAMS - 41)) | (1 << (CSharpParser.THIS - 41)) | (1 << (CSharpParser.REF - 41)))) != 0):
                self.state = 592
                _la = self._input.LA(1)
                if not(((((_la - 41)) & ~0x3f) == 0 and ((1 << (_la - 41)) & ((1 << (CSharpParser.OUT - 41)) | (1 << (CSharpParser.PARAMS - 41)) | (1 << (CSharpParser.THIS - 41)) | (1 << (CSharpParser.REF - 41)))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 597
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Class_baseContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.Type_Context)
            else:
                return self.getTypedRuleContext(CSharpParser.Type_Context,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_class_base

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClass_base" ):
                listener.enterClass_base(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClass_base" ):
                listener.exitClass_base(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitClass_base" ):
                return visitor.visitClass_base(self)
            else:
                return visitor.visitChildren(self)




    def class_base(self):

        localctx = CSharpParser.Class_baseContext(self, self._ctx, self.state)
        self.enterRule(localctx, 80, self.RULE_class_base)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 598
            self.match(CSharpParser.T__14)
            self.state = 599
            self.type_()
            self.state = 604
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CSharpParser.T__8:
                self.state = 600
                self.match(CSharpParser.T__8)
                self.state = 601
                self.type_()
                self.state = 606
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Operator_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TRUE(self):
            return self.getToken(CSharpParser.TRUE, 0)

        def FALSE(self):
            return self.getToken(CSharpParser.FALSE, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_operator_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOperator_name" ):
                listener.enterOperator_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOperator_name" ):
                listener.exitOperator_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOperator_name" ):
                return visitor.visitOperator_name(self)
            else:
                return visitor.visitChildren(self)




    def operator_name(self):

        localctx = CSharpParser.Operator_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 82, self.RULE_operator_name)
        try:
            self.state = 629
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,102,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 607
                self.match(CSharpParser.T__18)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 608
                self.match(CSharpParser.T__19)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 609
                self.match(CSharpParser.T__20)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 610
                self.match(CSharpParser.T__21)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 611
                self.match(CSharpParser.T__22)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 612
                self.match(CSharpParser.T__23)
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 613
                self.match(CSharpParser.T__24)
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 614
                self.match(CSharpParser.T__25)
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 615
                self.match(CSharpParser.T__26)
                pass

            elif la_ == 10:
                self.enterOuterAlt(localctx, 10)
                self.state = 616
                self.match(CSharpParser.T__27)
                pass

            elif la_ == 11:
                self.enterOuterAlt(localctx, 11)
                self.state = 617
                self.match(CSharpParser.T__28)
                self.state = 618
                self.match(CSharpParser.T__28)
                pass

            elif la_ == 12:
                self.enterOuterAlt(localctx, 12)
                self.state = 619
                self.match(CSharpParser.T__29)
                self.state = 620
                self.match(CSharpParser.T__29)
                pass

            elif la_ == 13:
                self.enterOuterAlt(localctx, 13)
                self.state = 621
                self.match(CSharpParser.T__30)
                pass

            elif la_ == 14:
                self.enterOuterAlt(localctx, 14)
                self.state = 622
                self.match(CSharpParser.T__31)
                pass

            elif la_ == 15:
                self.enterOuterAlt(localctx, 15)
                self.state = 623
                self.match(CSharpParser.T__28)
                pass

            elif la_ == 16:
                self.enterOuterAlt(localctx, 16)
                self.state = 624
                self.match(CSharpParser.T__29)
                pass

            elif la_ == 17:
                self.enterOuterAlt(localctx, 17)
                self.state = 625
                self.match(CSharpParser.T__32)
                pass

            elif la_ == 18:
                self.enterOuterAlt(localctx, 18)
                self.state = 626
                self.match(CSharpParser.T__33)
                pass

            elif la_ == 19:
                self.enterOuterAlt(localctx, 19)
                self.state = 627
                self.match(CSharpParser.TRUE)
                pass

            elif la_ == 20:
                self.enterOuterAlt(localctx, 20)
                self.state = 628
                self.match(CSharpParser.FALSE)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def type_tuple(self):
            return self.getTypedRuleContext(CSharpParser.Type_tupleContext,0)


        def type_array_decorator(self):
            return self.getTypedRuleContext(CSharpParser.Type_array_decoratorContext,0)


        def type_nullable_decorator(self):
            return self.getTypedRuleContext(CSharpParser.Type_nullable_decoratorContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_type_

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_" ):
                listener.enterType_(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_" ):
                listener.exitType_(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_" ):
                return visitor.visitType_(self)
            else:
                return visitor.visitChildren(self)




    def type_(self):

        localctx = CSharpParser.Type_Context(self, self._ctx, self.state)
        self.enterRule(localctx, 84, self.RULE_type_)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 633
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CSharpParser.IDENTIFIER]:
                self.state = 631
                self.type_name()
                pass
            elif token in [CSharpParser.T__4]:
                self.state = 632
                self.type_tuple()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 636
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__11:
                self.state = 635
                self.type_array_decorator()


            self.state = 639
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.T__10:
                self.state = 638
                self.type_nullable_decorator()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_array_decoratorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return CSharpParser.RULE_type_array_decorator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_array_decorator" ):
                listener.enterType_array_decorator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_array_decorator" ):
                listener.exitType_array_decorator(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_array_decorator" ):
                return visitor.visitType_array_decorator(self)
            else:
                return visitor.visitChildren(self)




    def type_array_decorator(self):

        localctx = CSharpParser.Type_array_decoratorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 86, self.RULE_type_array_decorator)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 641
            self.match(CSharpParser.T__11)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_nullable_decoratorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return CSharpParser.RULE_type_nullable_decorator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_nullable_decorator" ):
                listener.enterType_nullable_decorator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_nullable_decorator" ):
                listener.exitType_nullable_decorator(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_nullable_decorator" ):
                return visitor.visitType_nullable_decorator(self)
            else:
                return visitor.visitChildren(self)




    def type_nullable_decorator(self):

        localctx = CSharpParser.Type_nullable_decoratorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 88, self.RULE_type_nullable_decorator)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 643
            self.match(CSharpParser.T__10)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_tupleContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.Type_Context)
            else:
                return self.getTypedRuleContext(CSharpParser.Type_Context,i)


        def parameter_name(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.Parameter_nameContext)
            else:
                return self.getTypedRuleContext(CSharpParser.Parameter_nameContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_type_tuple

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_tuple" ):
                listener.enterType_tuple(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_tuple" ):
                listener.exitType_tuple(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_tuple" ):
                return visitor.visitType_tuple(self)
            else:
                return visitor.visitChildren(self)




    def type_tuple(self):

        localctx = CSharpParser.Type_tupleContext(self, self._ctx, self.state)
        self.enterRule(localctx, 90, self.RULE_type_tuple)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 645
            self.match(CSharpParser.T__4)
            self.state = 646
            self.type_()
            self.state = 648
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 70)) & ~0x3f) == 0 and ((1 << (_la - 70)) & ((1 << (CSharpParser.SET - 70)) | (1 << (CSharpParser.GET - 70)) | (1 << (CSharpParser.IDENTIFIER - 70)))) != 0):
                self.state = 647
                self.parameter_name()


            self.state = 657
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CSharpParser.T__8:
                self.state = 650
                self.match(CSharpParser.T__8)
                self.state = 651
                self.type_()
                self.state = 653
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if ((((_la - 70)) & ~0x3f) == 0 and ((1 << (_la - 70)) & ((1 << (CSharpParser.SET - 70)) | (1 << (CSharpParser.GET - 70)) | (1 << (CSharpParser.IDENTIFIER - 70)))) != 0):
                    self.state = 652
                    self.parameter_name()


                self.state = 659
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 660
            self.match(CSharpParser.T__5)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def unqualified_type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.Unqualified_typeContext)
            else:
                return self.getTypedRuleContext(CSharpParser.Unqualified_typeContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_type_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_name" ):
                listener.enterType_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_name" ):
                listener.exitType_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_name" ):
                return visitor.visitType_name(self)
            else:
                return visitor.visitChildren(self)




    def type_name(self):

        localctx = CSharpParser.Type_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 92, self.RULE_type_name)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 667
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,109,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 662
                    self.unqualified_type()
                    self.state = 663
                    self.match(CSharpParser.T__12) 
                self.state = 669
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,109,self._ctx)

            self.state = 670
            self.unqualified_type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Unqualified_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def unqualified_type_name(self):
            return self.getTypedRuleContext(CSharpParser.Unqualified_type_nameContext,0)


        def type_parameter_list(self):
            return self.getTypedRuleContext(CSharpParser.Type_parameter_listContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_unqualified_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnqualified_type" ):
                listener.enterUnqualified_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnqualified_type" ):
                listener.exitUnqualified_type(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnqualified_type" ):
                return visitor.visitUnqualified_type(self)
            else:
                return visitor.visitChildren(self)




    def unqualified_type(self):

        localctx = CSharpParser.Unqualified_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 94, self.RULE_unqualified_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 672
            self.unqualified_type_name()
            self.state = 674
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,110,self._ctx)
            if la_ == 1:
                self.state = 673
                self.type_parameter_list()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Unqualified_type_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IDENTIFIER(self):
            return self.getToken(CSharpParser.IDENTIFIER, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_unqualified_type_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnqualified_type_name" ):
                listener.enterUnqualified_type_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnqualified_type_name" ):
                listener.exitUnqualified_type_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnqualified_type_name" ):
                return visitor.visitUnqualified_type_name(self)
            else:
                return visitor.visitChildren(self)




    def unqualified_type_name(self):

        localctx = CSharpParser.Unqualified_type_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 96, self.RULE_unqualified_type_name)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 676
            self.match(CSharpParser.IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Parameter_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IDENTIFIER(self):
            return self.getToken(CSharpParser.IDENTIFIER, 0)

        def SET(self):
            return self.getToken(CSharpParser.SET, 0)

        def GET(self):
            return self.getToken(CSharpParser.GET, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_parameter_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameter_name" ):
                listener.enterParameter_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameter_name" ):
                listener.exitParameter_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParameter_name" ):
                return visitor.visitParameter_name(self)
            else:
                return visitor.visitChildren(self)




    def parameter_name(self):

        localctx = CSharpParser.Parameter_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 98, self.RULE_parameter_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 678
            _la = self._input.LA(1)
            if not(((((_la - 70)) & ~0x3f) == 0 and ((1 << (_la - 70)) & ((1 << (CSharpParser.SET - 70)) | (1 << (CSharpParser.GET - 70)) | (1 << (CSharpParser.IDENTIFIER - 70)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_parameter_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_parameter(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.Type_parameterContext)
            else:
                return self.getTypedRuleContext(CSharpParser.Type_parameterContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_type_parameter_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_parameter_list" ):
                listener.enterType_parameter_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_parameter_list" ):
                listener.exitType_parameter_list(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_parameter_list" ):
                return visitor.visitType_parameter_list(self)
            else:
                return visitor.visitChildren(self)




    def type_parameter_list(self):

        localctx = CSharpParser.Type_parameter_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 100, self.RULE_type_parameter_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 680
            self.match(CSharpParser.T__28)
            self.state = 681
            self.type_parameter()
            self.state = 686
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CSharpParser.T__8:
                self.state = 682
                self.match(CSharpParser.T__8)
                self.state = 683
                self.type_parameter()
                self.state = 688
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 689
            self.match(CSharpParser.T__29)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_parameterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_(self):
            return self.getTypedRuleContext(CSharpParser.Type_Context,0)


        def attributes(self):
            return self.getTypedRuleContext(CSharpParser.AttributesContext,0)


        def type_parameter_modifiers(self):
            return self.getTypedRuleContext(CSharpParser.Type_parameter_modifiersContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_type_parameter

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_parameter" ):
                listener.enterType_parameter(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_parameter" ):
                listener.exitType_parameter(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_parameter" ):
                return visitor.visitType_parameter(self)
            else:
                return visitor.visitChildren(self)




    def type_parameter(self):

        localctx = CSharpParser.Type_parameterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 102, self.RULE_type_parameter)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 692
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,112,self._ctx)
            if la_ == 1:
                self.state = 691
                self.attributes()


            self.state = 695
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CSharpParser.IN or _la==CSharpParser.OUT:
                self.state = 694
                self.type_parameter_modifiers()


            self.state = 697
            self.type_()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_parameter_modifiersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_parameter_modifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.Type_parameter_modifierContext)
            else:
                return self.getTypedRuleContext(CSharpParser.Type_parameter_modifierContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_type_parameter_modifiers

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_parameter_modifiers" ):
                listener.enterType_parameter_modifiers(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_parameter_modifiers" ):
                listener.exitType_parameter_modifiers(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_parameter_modifiers" ):
                return visitor.visitType_parameter_modifiers(self)
            else:
                return visitor.visitChildren(self)




    def type_parameter_modifiers(self):

        localctx = CSharpParser.Type_parameter_modifiersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 104, self.RULE_type_parameter_modifiers)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 700 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 699
                self.type_parameter_modifier()
                self.state = 702 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==CSharpParser.IN or _la==CSharpParser.OUT):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_parameter_modifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IN(self):
            return self.getToken(CSharpParser.IN, 0)

        def OUT(self):
            return self.getToken(CSharpParser.OUT, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_type_parameter_modifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_parameter_modifier" ):
                listener.enterType_parameter_modifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_parameter_modifier" ):
                listener.exitType_parameter_modifier(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_parameter_modifier" ):
                return visitor.visitType_parameter_modifier(self)
            else:
                return visitor.visitChildren(self)




    def type_parameter_modifier(self):

        localctx = CSharpParser.Type_parameter_modifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 106, self.RULE_type_parameter_modifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 704
            _la = self._input.LA(1)
            if not(_la==CSharpParser.IN or _la==CSharpParser.OUT):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Integral_valueContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTEGER_VALUE(self):
            return self.getToken(CSharpParser.INTEGER_VALUE, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_integral_value

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIntegral_value" ):
                listener.enterIntegral_value(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIntegral_value" ):
                listener.exitIntegral_value(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIntegral_value" ):
                return visitor.visitIntegral_value(self)
            else:
                return visitor.visitChildren(self)




    def integral_value(self):

        localctx = CSharpParser.Integral_valueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 108, self.RULE_integral_value)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 706
            self.match(CSharpParser.INTEGER_VALUE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class String_literalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRING_LITERAL(self):
            return self.getToken(CSharpParser.STRING_LITERAL, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_string_literal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString_literal" ):
                listener.enterString_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString_literal" ):
                listener.exitString_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString_literal" ):
                return visitor.visitString_literal(self)
            else:
                return visitor.visitChildren(self)




    def string_literal(self):

        localctx = CSharpParser.String_literalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 110, self.RULE_string_literal)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 708
            self.match(CSharpParser.STRING_LITERAL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Enum_literalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_name(self):
            return self.getTypedRuleContext(CSharpParser.Type_nameContext,0)


        def getRuleIndex(self):
            return CSharpParser.RULE_enum_literal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEnum_literal" ):
                listener.enterEnum_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEnum_literal" ):
                listener.exitEnum_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEnum_literal" ):
                return visitor.visitEnum_literal(self)
            else:
                return visitor.visitChildren(self)




    def enum_literal(self):

        localctx = CSharpParser.Enum_literalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 112, self.RULE_enum_literal)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 710
            self.type_name()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Numeric_literalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTEGER_VALUE(self):
            return self.getToken(CSharpParser.INTEGER_VALUE, 0)

        def DOUBLE_VALUE(self):
            return self.getToken(CSharpParser.DOUBLE_VALUE, 0)

        def FLOAT_VALUE(self):
            return self.getToken(CSharpParser.FLOAT_VALUE, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_numeric_literal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumeric_literal" ):
                listener.enterNumeric_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumeric_literal" ):
                listener.exitNumeric_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNumeric_literal" ):
                return visitor.visitNumeric_literal(self)
            else:
                return visitor.visitChildren(self)




    def numeric_literal(self):

        localctx = CSharpParser.Numeric_literalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 114, self.RULE_numeric_literal)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 712
            _la = self._input.LA(1)
            if not(((((_la - 76)) & ~0x3f) == 0 and ((1 << (_la - 76)) & ((1 << (CSharpParser.INTEGER_VALUE - 76)) | (1 << (CSharpParser.DOUBLE_VALUE - 76)) | (1 << (CSharpParser.FLOAT_VALUE - 76)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Bool_literalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TRUE(self):
            return self.getToken(CSharpParser.TRUE, 0)

        def FALSE(self):
            return self.getToken(CSharpParser.FALSE, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_bool_literal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBool_literal" ):
                listener.enterBool_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBool_literal" ):
                listener.exitBool_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBool_literal" ):
                return visitor.visitBool_literal(self)
            else:
                return visitor.visitChildren(self)




    def bool_literal(self):

        localctx = CSharpParser.Bool_literalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 116, self.RULE_bool_literal)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 714
            _la = self._input.LA(1)
            if not(_la==CSharpParser.TRUE or _la==CSharpParser.FALSE):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ModifiersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def modifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CSharpParser.ModifierContext)
            else:
                return self.getTypedRuleContext(CSharpParser.ModifierContext,i)


        def getRuleIndex(self):
            return CSharpParser.RULE_modifiers

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterModifiers" ):
                listener.enterModifiers(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitModifiers" ):
                listener.exitModifiers(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitModifiers" ):
                return visitor.visitModifiers(self)
            else:
                return visitor.visitChildren(self)




    def modifiers(self):

        localctx = CSharpParser.ModifiersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 118, self.RULE_modifiers)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 717 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 716
                self.modifier()
                self.state = 719 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NEW(self):
            return self.getToken(CSharpParser.NEW, 0)

        def PUBLIC(self):
            return self.getToken(CSharpParser.PUBLIC, 0)

        def PROTECTED(self):
            return self.getToken(CSharpParser.PROTECTED, 0)

        def INTERNAL(self):
            return self.getToken(CSharpParser.INTERNAL, 0)

        def PRIVATE(self):
            return self.getToken(CSharpParser.PRIVATE, 0)

        def READONLY(self):
            return self.getToken(CSharpParser.READONLY, 0)

        def VOLATILE(self):
            return self.getToken(CSharpParser.VOLATILE, 0)

        def VIRTUAL(self):
            return self.getToken(CSharpParser.VIRTUAL, 0)

        def SEALED(self):
            return self.getToken(CSharpParser.SEALED, 0)

        def OVERRIDE(self):
            return self.getToken(CSharpParser.OVERRIDE, 0)

        def ABSTRACT(self):
            return self.getToken(CSharpParser.ABSTRACT, 0)

        def STATIC(self):
            return self.getToken(CSharpParser.STATIC, 0)

        def UNSAFE(self):
            return self.getToken(CSharpParser.UNSAFE, 0)

        def EXTERN(self):
            return self.getToken(CSharpParser.EXTERN, 0)

        def PARTIAL(self):
            return self.getToken(CSharpParser.PARTIAL, 0)

        def ASYNC(self):
            return self.getToken(CSharpParser.ASYNC, 0)

        def PARAMS(self):
            return self.getToken(CSharpParser.PARAMS, 0)

        def THIS(self):
            return self.getToken(CSharpParser.THIS, 0)

        def CONST(self):
            return self.getToken(CSharpParser.CONST, 0)

        def IMPLICIT(self):
            return self.getToken(CSharpParser.IMPLICIT, 0)

        def EXPLICIT(self):
            return self.getToken(CSharpParser.EXPLICIT, 0)

        def getRuleIndex(self):
            return CSharpParser.RULE_modifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterModifier" ):
                listener.enterModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitModifier" ):
                listener.exitModifier(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitModifier" ):
                return visitor.visitModifier(self)
            else:
                return visitor.visitChildren(self)




    def modifier(self):

        localctx = CSharpParser.ModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 120, self.RULE_modifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 721
            _la = self._input.LA(1)
            if not(((((_la - 46)) & ~0x3f) == 0 and ((1 << (_la - 46)) & ((1 << (CSharpParser.NEW - 46)) | (1 << (CSharpParser.PUBLIC - 46)) | (1 << (CSharpParser.PROTECTED - 46)) | (1 << (CSharpParser.INTERNAL - 46)) | (1 << (CSharpParser.PRIVATE - 46)) | (1 << (CSharpParser.READONLY - 46)) | (1 << (CSharpParser.VOLATILE - 46)) | (1 << (CSharpParser.VIRTUAL - 46)) | (1 << (CSharpParser.SEALED - 46)) | (1 << (CSharpParser.OVERRIDE - 46)) | (1 << (CSharpParser.ABSTRACT - 46)) | (1 << (CSharpParser.STATIC - 46)) | (1 << (CSharpParser.UNSAFE - 46)) | (1 << (CSharpParser.EXTERN - 46)) | (1 << (CSharpParser.PARTIAL - 46)) | (1 << (CSharpParser.ASYNC - 46)) | (1 << (CSharpParser.PARAMS - 46)) | (1 << (CSharpParser.THIS - 46)) | (1 << (CSharpParser.CONST - 46)) | (1 << (CSharpParser.IMPLICIT - 46)) | (1 << (CSharpParser.EXPLICIT - 46)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





