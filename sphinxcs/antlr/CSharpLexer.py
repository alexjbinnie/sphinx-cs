# Generated from C:/Users/Alex-Binnie/Projects/Python/sphinxcs/sphinxcs/antlr\CSharp.g4 by ANTLR 4.8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2Q")
        buf.write("\u024f\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\t")
        buf.write("C\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4L\t")
        buf.write("L\4M\tM\4N\tN\4O\tO\4P\tP\3\2\3\2\3\3\3\3\3\4\3\4\3\5")
        buf.write("\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13")
        buf.write("\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21")
        buf.write("\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\23")
        buf.write("\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\25\3\25")
        buf.write("\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\30\3\31\3\31\3\32")
        buf.write("\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37")
        buf.write("\3 \3 \3 \3!\3!\3!\3\"\3\"\3\"\3#\3#\3#\3$\6$\u00fb\n")
        buf.write("$\r$\16$\u00fc\3$\3$\3%\3%\3%\3%\3%\3%\3&\3&\3&\3&\3&")
        buf.write("\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3(\3")
        buf.write("(\3(\3(\3(\3)\3)\3)\3*\3*\3*\3*\3+\3+\3+\3+\3+\3+\3+\3")
        buf.write(",\3,\3,\3,\3,\3,\3,\3,\3,\3,\3-\3-\3-\3-\3-\3-\3-\3-\3")
        buf.write("-\3-\3.\3.\3.\3.\3.\3.\3/\3/\3/\3/\3\60\3\60\3\60\3\60")
        buf.write("\3\60\3\60\3\60\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61")
        buf.write("\3\61\3\61\3\62\3\62\3\62\3\62\3\62\3\62\3\62\3\62\3\62")
        buf.write("\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\64\3\64\3\64")
        buf.write("\3\64\3\64\3\64\3\64\3\64\3\64\3\65\3\65\3\65\3\65\3\65")
        buf.write("\3\65\3\65\3\65\3\65\3\66\3\66\3\66\3\66\3\66\3\66\3\66")
        buf.write("\3\66\3\67\3\67\3\67\3\67\3\67\3\67\3\67\38\38\38\38\3")
        buf.write("8\38\38\38\38\39\39\39\39\39\39\39\39\39\3:\3:\3:\3:\3")
        buf.write(":\3:\3:\3;\3;\3;\3;\3;\3;\3;\3<\3<\3<\3<\3<\3<\3<\3=\3")
        buf.write("=\3=\3=\3=\3=\3=\3=\3>\3>\3>\3>\3>\3>\3?\3?\3?\3?\3?\3")
        buf.write("?\3?\3@\3@\3@\3@\3@\3A\3A\3A\3A\3B\3B\3B\3B\3B\3B\3C\3")
        buf.write("C\3C\3C\3C\3C\3C\3C\3D\3D\3D\3D\3D\3D\3E\3E\3E\3E\3E\3")
        buf.write("E\3E\3E\3E\3F\3F\3F\3F\3F\3F\3F\3F\3F\3G\3G\3G\3G\3H\3")
        buf.write("H\3H\3H\3I\3I\3I\3I\3I\3J\3J\3J\3J\3J\3J\3K\3K\3K\3L\3")
        buf.write("L\3L\3L\3L\5L\u0213\nL\7L\u0215\nL\fL\16L\u0218\13L\3")
        buf.write("M\5M\u021b\nM\3M\6M\u021e\nM\rM\16M\u021f\3N\5N\u0223")
        buf.write("\nN\3N\6N\u0226\nN\rN\16N\u0227\3N\3N\6N\u022c\nN\rN\16")
        buf.write("N\u022d\5N\u0230\nN\3N\5N\u0233\nN\3O\5O\u0236\nO\3O\6")
        buf.write("O\u0239\nO\rO\16O\u023a\3O\3O\6O\u023f\nO\rO\16O\u0240")
        buf.write("\5O\u0243\nO\3O\5O\u0246\nO\3P\5P\u0249\nP\3P\6P\u024c")
        buf.write("\nP\rP\16P\u024d\2\2Q\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21")
        buf.write("\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24")
        buf.write("\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37")
        buf.write("= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64")
        buf.write("g\65i\66k\67m8o9q:s;u<w=y>{?}@\177A\u0081B\u0083C\u0085")
        buf.write("D\u0087E\u0089F\u008bG\u008dH\u008fI\u0091J\u0093K\u0095")
        buf.write("L\u0097M\u0099N\u009bO\u009dP\u009fQ\3\2\b\6\2\f\f\17")
        buf.write("\17$$^^\4\2--//\3\2\62;\4\2FFff\4\2HHhh\6\2\62;C\\aac")
        buf.write("|\2\u0260\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2")
        buf.write("\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2")
        buf.write("\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2")
        buf.write("\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#")
        buf.write("\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2")
        buf.write("\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65")
        buf.write("\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2")
        buf.write("\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2")
        buf.write("\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2")
        buf.write("\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3")
        buf.write("\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e")
        buf.write("\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2")
        buf.write("o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2")
        buf.write("\2y\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2\2\177\3\2\2\2\2\u0081")
        buf.write("\3\2\2\2\2\u0083\3\2\2\2\2\u0085\3\2\2\2\2\u0087\3\2\2")
        buf.write("\2\2\u0089\3\2\2\2\2\u008b\3\2\2\2\2\u008d\3\2\2\2\2\u008f")
        buf.write("\3\2\2\2\2\u0091\3\2\2\2\2\u0093\3\2\2\2\2\u0095\3\2\2")
        buf.write("\2\2\u0097\3\2\2\2\2\u0099\3\2\2\2\2\u009b\3\2\2\2\2\u009d")
        buf.write("\3\2\2\2\2\u009f\3\2\2\2\3\u00a1\3\2\2\2\5\u00a3\3\2\2")
        buf.write("\2\7\u00a5\3\2\2\2\t\u00a7\3\2\2\2\13\u00a9\3\2\2\2\r")
        buf.write("\u00ab\3\2\2\2\17\u00ad\3\2\2\2\21\u00af\3\2\2\2\23\u00b1")
        buf.write("\3\2\2\2\25\u00b3\3\2\2\2\27\u00b5\3\2\2\2\31\u00b7\3")
        buf.write("\2\2\2\33\u00ba\3\2\2\2\35\u00bc\3\2\2\2\37\u00be\3\2")
        buf.write("\2\2!\u00c0\3\2\2\2#\u00c6\3\2\2\2%\u00cb\3\2\2\2\'\u00d3")
        buf.write("\3\2\2\2)\u00d5\3\2\2\2+\u00d7\3\2\2\2-\u00d9\3\2\2\2")
        buf.write("/\u00dc\3\2\2\2\61\u00df\3\2\2\2\63\u00e1\3\2\2\2\65\u00e3")
        buf.write("\3\2\2\2\67\u00e5\3\2\2\29\u00e7\3\2\2\2;\u00e9\3\2\2")
        buf.write("\2=\u00eb\3\2\2\2?\u00ed\3\2\2\2A\u00f0\3\2\2\2C\u00f3")
        buf.write("\3\2\2\2E\u00f6\3\2\2\2G\u00fa\3\2\2\2I\u0100\3\2\2\2")
        buf.write("K\u0106\3\2\2\2M\u010f\3\2\2\2O\u0118\3\2\2\2Q\u011d\3")
        buf.write("\2\2\2S\u0120\3\2\2\2U\u0124\3\2\2\2W\u012b\3\2\2\2Y\u0135")
        buf.write("\3\2\2\2[\u013f\3\2\2\2]\u0145\3\2\2\2_\u0149\3\2\2\2")
        buf.write("a\u0150\3\2\2\2c\u015a\3\2\2\2e\u0163\3\2\2\2g\u016b\3")
        buf.write("\2\2\2i\u0174\3\2\2\2k\u017d\3\2\2\2m\u0185\3\2\2\2o\u018c")
        buf.write("\3\2\2\2q\u0195\3\2\2\2s\u019e\3\2\2\2u\u01a5\3\2\2\2")
        buf.write("w\u01ac\3\2\2\2y\u01b3\3\2\2\2{\u01bb\3\2\2\2}\u01c1\3")
        buf.write("\2\2\2\177\u01c8\3\2\2\2\u0081\u01cd\3\2\2\2\u0083\u01d1")
        buf.write("\3\2\2\2\u0085\u01d7\3\2\2\2\u0087\u01df\3\2\2\2\u0089")
        buf.write("\u01e5\3\2\2\2\u008b\u01ee\3\2\2\2\u008d\u01f7\3\2\2\2")
        buf.write("\u008f\u01fb\3\2\2\2\u0091\u01ff\3\2\2\2\u0093\u0204\3")
        buf.write("\2\2\2\u0095\u020a\3\2\2\2\u0097\u020d\3\2\2\2\u0099\u021a")
        buf.write("\3\2\2\2\u009b\u0222\3\2\2\2\u009d\u0235\3\2\2\2\u009f")
        buf.write("\u0248\3\2\2\2\u00a1\u00a2\7=\2\2\u00a2\4\3\2\2\2\u00a3")
        buf.write("\u00a4\7}\2\2\u00a4\6\3\2\2\2\u00a5\u00a6\7\177\2\2\u00a6")
        buf.write("\b\3\2\2\2\u00a7\u00a8\7?\2\2\u00a8\n\3\2\2\2\u00a9\u00aa")
        buf.write("\7*\2\2\u00aa\f\3\2\2\2\u00ab\u00ac\7+\2\2\u00ac\16\3")
        buf.write("\2\2\2\u00ad\u00ae\7]\2\2\u00ae\20\3\2\2\2\u00af\u00b0")
        buf.write("\7_\2\2\u00b0\22\3\2\2\2\u00b1\u00b2\7.\2\2\u00b2\24\3")
        buf.write("\2\2\2\u00b3\u00b4\7B\2\2\u00b4\26\3\2\2\2\u00b5\u00b6")
        buf.write("\7A\2\2\u00b6\30\3\2\2\2\u00b7\u00b8\7]\2\2\u00b8\u00b9")
        buf.write("\7_\2\2\u00b9\32\3\2\2\2\u00ba\u00bb\7\60\2\2\u00bb\34")
        buf.write("\3\2\2\2\u00bc\u00bd\7b\2\2\u00bd\36\3\2\2\2\u00be\u00bf")
        buf.write("\7<\2\2\u00bf \3\2\2\2\u00c0\u00c1\7p\2\2\u00c1\u00c2")
        buf.write("\7g\2\2\u00c2\u00c3\7y\2\2\u00c3\u00c4\7*\2\2\u00c4\u00c5")
        buf.write("\7+\2\2\u00c5\"\3\2\2\2\u00c6\u00c7\7p\2\2\u00c7\u00c8")
        buf.write("\7w\2\2\u00c8\u00c9\7n\2\2\u00c9\u00ca\7n\2\2\u00ca$\3")
        buf.write("\2\2\2\u00cb\u00cc\7v\2\2\u00cc\u00cd\7{\2\2\u00cd\u00ce")
        buf.write("\7r\2\2\u00ce\u00cf\7g\2\2\u00cf\u00d0\7q\2\2\u00d0\u00d1")
        buf.write("\7h\2\2\u00d1\u00d2\7*\2\2\u00d2&\3\2\2\2\u00d3\u00d4")
        buf.write("\7-\2\2\u00d4(\3\2\2\2\u00d5\u00d6\7/\2\2\u00d6*\3\2\2")
        buf.write("\2\u00d7\u00d8\7#\2\2\u00d8,\3\2\2\2\u00d9\u00da\7-\2")
        buf.write("\2\u00da\u00db\7-\2\2\u00db.\3\2\2\2\u00dc\u00dd\7/\2")
        buf.write("\2\u00dd\u00de\7/\2\2\u00de\60\3\2\2\2\u00df\u00e0\7,")
        buf.write("\2\2\u00e0\62\3\2\2\2\u00e1\u00e2\7\61\2\2\u00e2\64\3")
        buf.write("\2\2\2\u00e3\u00e4\7(\2\2\u00e4\66\3\2\2\2\u00e5\u00e6")
        buf.write("\7~\2\2\u00e68\3\2\2\2\u00e7\u00e8\7`\2\2\u00e8:\3\2\2")
        buf.write("\2\u00e9\u00ea\7>\2\2\u00ea<\3\2\2\2\u00eb\u00ec\7@\2")
        buf.write("\2\u00ec>\3\2\2\2\u00ed\u00ee\7?\2\2\u00ee\u00ef\7?\2")
        buf.write("\2\u00ef@\3\2\2\2\u00f0\u00f1\7#\2\2\u00f1\u00f2\7?\2")
        buf.write("\2\u00f2B\3\2\2\2\u00f3\u00f4\7>\2\2\u00f4\u00f5\7?\2")
        buf.write("\2\u00f5D\3\2\2\2\u00f6\u00f7\7@\2\2\u00f7\u00f8\7?\2")
        buf.write("\2\u00f8F\3\2\2\2\u00f9\u00fb\7\"\2\2\u00fa\u00f9\3\2")
        buf.write("\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00fa\3\2\2\2\u00fc\u00fd")
        buf.write("\3\2\2\2\u00fd\u00fe\3\2\2\2\u00fe\u00ff\b$\2\2\u00ff")
        buf.write("H\3\2\2\2\u0100\u0101\7e\2\2\u0101\u0102\7n\2\2\u0102")
        buf.write("\u0103\7c\2\2\u0103\u0104\7u\2\2\u0104\u0105\7u\2\2\u0105")
        buf.write("J\3\2\2\2\u0106\u0107\7f\2\2\u0107\u0108\7g\2\2\u0108")
        buf.write("\u0109\7n\2\2\u0109\u010a\7g\2\2\u010a\u010b\7i\2\2\u010b")
        buf.write("\u010c\7c\2\2\u010c\u010d\7v\2\2\u010d\u010e\7g\2\2\u010e")
        buf.write("L\3\2\2\2\u010f\u0110\7q\2\2\u0110\u0111\7r\2\2\u0111")
        buf.write("\u0112\7g\2\2\u0112\u0113\7t\2\2\u0113\u0114\7c\2\2\u0114")
        buf.write("\u0115\7v\2\2\u0115\u0116\7q\2\2\u0116\u0117\7t\2\2\u0117")
        buf.write("N\3\2\2\2\u0118\u0119\7g\2\2\u0119\u011a\7p\2\2\u011a")
        buf.write("\u011b\7w\2\2\u011b\u011c\7o\2\2\u011cP\3\2\2\2\u011d")
        buf.write("\u011e\7k\2\2\u011e\u011f\7p\2\2\u011fR\3\2\2\2\u0120")
        buf.write("\u0121\7q\2\2\u0121\u0122\7w\2\2\u0122\u0123\7v\2\2\u0123")
        buf.write("T\3\2\2\2\u0124\u0125\7u\2\2\u0125\u0126\7v\2\2\u0126")
        buf.write("\u0127\7t\2\2\u0127\u0128\7w\2\2\u0128\u0129\7e\2\2\u0129")
        buf.write("\u012a\7v\2\2\u012aV\3\2\2\2\u012b\u012c\7p\2\2\u012c")
        buf.write("\u012d\7c\2\2\u012d\u012e\7o\2\2\u012e\u012f\7g\2\2\u012f")
        buf.write("\u0130\7u\2\2\u0130\u0131\7r\2\2\u0131\u0132\7c\2\2\u0132")
        buf.write("\u0133\7e\2\2\u0133\u0134\7g\2\2\u0134X\3\2\2\2\u0135")
        buf.write("\u0136\7k\2\2\u0136\u0137\7p\2\2\u0137\u0138\7v\2\2\u0138")
        buf.write("\u0139\7g\2\2\u0139\u013a\7t\2\2\u013a\u013b\7h\2\2\u013b")
        buf.write("\u013c\7c\2\2\u013c\u013d\7e\2\2\u013d\u013e\7g\2\2\u013e")
        buf.write("Z\3\2\2\2\u013f\u0140\7g\2\2\u0140\u0141\7x\2\2\u0141")
        buf.write("\u0142\7g\2\2\u0142\u0143\7p\2\2\u0143\u0144\7v\2\2\u0144")
        buf.write("\\\3\2\2\2\u0145\u0146\7p\2\2\u0146\u0147\7g\2\2\u0147")
        buf.write("\u0148\7y\2\2\u0148^\3\2\2\2\u0149\u014a\7r\2\2\u014a")
        buf.write("\u014b\7w\2\2\u014b\u014c\7d\2\2\u014c\u014d\7n\2\2\u014d")
        buf.write("\u014e\7k\2\2\u014e\u014f\7e\2\2\u014f`\3\2\2\2\u0150")
        buf.write("\u0151\7r\2\2\u0151\u0152\7t\2\2\u0152\u0153\7q\2\2\u0153")
        buf.write("\u0154\7v\2\2\u0154\u0155\7g\2\2\u0155\u0156\7e\2\2\u0156")
        buf.write("\u0157\7v\2\2\u0157\u0158\7g\2\2\u0158\u0159\7f\2\2\u0159")
        buf.write("b\3\2\2\2\u015a\u015b\7k\2\2\u015b\u015c\7p\2\2\u015c")
        buf.write("\u015d\7v\2\2\u015d\u015e\7g\2\2\u015e\u015f\7t\2\2\u015f")
        buf.write("\u0160\7p\2\2\u0160\u0161\7c\2\2\u0161\u0162\7n\2\2\u0162")
        buf.write("d\3\2\2\2\u0163\u0164\7r\2\2\u0164\u0165\7t\2\2\u0165")
        buf.write("\u0166\7k\2\2\u0166\u0167\7x\2\2\u0167\u0168\7c\2\2\u0168")
        buf.write("\u0169\7v\2\2\u0169\u016a\7g\2\2\u016af\3\2\2\2\u016b")
        buf.write("\u016c\7t\2\2\u016c\u016d\7g\2\2\u016d\u016e\7c\2\2\u016e")
        buf.write("\u016f\7f\2\2\u016f\u0170\7q\2\2\u0170\u0171\7p\2\2\u0171")
        buf.write("\u0172\7n\2\2\u0172\u0173\7{\2\2\u0173h\3\2\2\2\u0174")
        buf.write("\u0175\7x\2\2\u0175\u0176\7q\2\2\u0176\u0177\7n\2\2\u0177")
        buf.write("\u0178\7c\2\2\u0178\u0179\7v\2\2\u0179\u017a\7k\2\2\u017a")
        buf.write("\u017b\7n\2\2\u017b\u017c\7g\2\2\u017cj\3\2\2\2\u017d")
        buf.write("\u017e\7x\2\2\u017e\u017f\7k\2\2\u017f\u0180\7t\2\2\u0180")
        buf.write("\u0181\7v\2\2\u0181\u0182\7w\2\2\u0182\u0183\7c\2\2\u0183")
        buf.write("\u0184\7n\2\2\u0184l\3\2\2\2\u0185\u0186\7u\2\2\u0186")
        buf.write("\u0187\7g\2\2\u0187\u0188\7c\2\2\u0188\u0189\7n\2\2\u0189")
        buf.write("\u018a\7g\2\2\u018a\u018b\7f\2\2\u018bn\3\2\2\2\u018c")
        buf.write("\u018d\7q\2\2\u018d\u018e\7x\2\2\u018e\u018f\7g\2\2\u018f")
        buf.write("\u0190\7t\2\2\u0190\u0191\7t\2\2\u0191\u0192\7k\2\2\u0192")
        buf.write("\u0193\7f\2\2\u0193\u0194\7g\2\2\u0194p\3\2\2\2\u0195")
        buf.write("\u0196\7c\2\2\u0196\u0197\7d\2\2\u0197\u0198\7u\2\2\u0198")
        buf.write("\u0199\7v\2\2\u0199\u019a\7t\2\2\u019a\u019b\7c\2\2\u019b")
        buf.write("\u019c\7e\2\2\u019c\u019d\7v\2\2\u019dr\3\2\2\2\u019e")
        buf.write("\u019f\7u\2\2\u019f\u01a0\7v\2\2\u01a0\u01a1\7c\2\2\u01a1")
        buf.write("\u01a2\7v\2\2\u01a2\u01a3\7k\2\2\u01a3\u01a4\7e\2\2\u01a4")
        buf.write("t\3\2\2\2\u01a5\u01a6\7w\2\2\u01a6\u01a7\7p\2\2\u01a7")
        buf.write("\u01a8\7u\2\2\u01a8\u01a9\7c\2\2\u01a9\u01aa\7h\2\2\u01aa")
        buf.write("\u01ab\7g\2\2\u01abv\3\2\2\2\u01ac\u01ad\7g\2\2\u01ad")
        buf.write("\u01ae\7z\2\2\u01ae\u01af\7v\2\2\u01af\u01b0\7g\2\2\u01b0")
        buf.write("\u01b1\7t\2\2\u01b1\u01b2\7p\2\2\u01b2x\3\2\2\2\u01b3")
        buf.write("\u01b4\7r\2\2\u01b4\u01b5\7c\2\2\u01b5\u01b6\7t\2\2\u01b6")
        buf.write("\u01b7\7v\2\2\u01b7\u01b8\7k\2\2\u01b8\u01b9\7c\2\2\u01b9")
        buf.write("\u01ba\7n\2\2\u01baz\3\2\2\2\u01bb\u01bc\7c\2\2\u01bc")
        buf.write("\u01bd\7u\2\2\u01bd\u01be\7{\2\2\u01be\u01bf\7p\2\2\u01bf")
        buf.write("\u01c0\7e\2\2\u01c0|\3\2\2\2\u01c1\u01c2\7r\2\2\u01c2")
        buf.write("\u01c3\7c\2\2\u01c3\u01c4\7t\2\2\u01c4\u01c5\7c\2\2\u01c5")
        buf.write("\u01c6\7o\2\2\u01c6\u01c7\7u\2\2\u01c7~\3\2\2\2\u01c8")
        buf.write("\u01c9\7v\2\2\u01c9\u01ca\7j\2\2\u01ca\u01cb\7k\2\2\u01cb")
        buf.write("\u01cc\7u\2\2\u01cc\u0080\3\2\2\2\u01cd\u01ce\7t\2\2\u01ce")
        buf.write("\u01cf\7g\2\2\u01cf\u01d0\7h\2\2\u01d0\u0082\3\2\2\2\u01d1")
        buf.write("\u01d2\7y\2\2\u01d2\u01d3\7j\2\2\u01d3\u01d4\7g\2\2\u01d4")
        buf.write("\u01d5\7t\2\2\u01d5\u01d6\7g\2\2\u01d6\u0084\3\2\2\2\u01d7")
        buf.write("\u01d8\7f\2\2\u01d8\u01d9\7g\2\2\u01d9\u01da\7h\2\2\u01da")
        buf.write("\u01db\7c\2\2\u01db\u01dc\7w\2\2\u01dc\u01dd\7n\2\2\u01dd")
        buf.write("\u01de\7v\2\2\u01de\u0086\3\2\2\2\u01df\u01e0\7e\2\2\u01e0")
        buf.write("\u01e1\7q\2\2\u01e1\u01e2\7p\2\2\u01e2\u01e3\7u\2\2\u01e3")
        buf.write("\u01e4\7v\2\2\u01e4\u0088\3\2\2\2\u01e5\u01e6\7k\2\2\u01e6")
        buf.write("\u01e7\7o\2\2\u01e7\u01e8\7r\2\2\u01e8\u01e9\7n\2\2\u01e9")
        buf.write("\u01ea\7k\2\2\u01ea\u01eb\7e\2\2\u01eb\u01ec\7k\2\2\u01ec")
        buf.write("\u01ed\7v\2\2\u01ed\u008a\3\2\2\2\u01ee\u01ef\7g\2\2\u01ef")
        buf.write("\u01f0\7z\2\2\u01f0\u01f1\7r\2\2\u01f1\u01f2\7n\2\2\u01f2")
        buf.write("\u01f3\7k\2\2\u01f3\u01f4\7e\2\2\u01f4\u01f5\7k\2\2\u01f5")
        buf.write("\u01f6\7v\2\2\u01f6\u008c\3\2\2\2\u01f7\u01f8\7u\2\2\u01f8")
        buf.write("\u01f9\7g\2\2\u01f9\u01fa\7v\2\2\u01fa\u008e\3\2\2\2\u01fb")
        buf.write("\u01fc\7i\2\2\u01fc\u01fd\7g\2\2\u01fd\u01fe\7v\2\2\u01fe")
        buf.write("\u0090\3\2\2\2\u01ff\u0200\7v\2\2\u0200\u0201\7t\2\2\u0201")
        buf.write("\u0202\7w\2\2\u0202\u0203\7g\2\2\u0203\u0092\3\2\2\2\u0204")
        buf.write("\u0205\7h\2\2\u0205\u0206\7c\2\2\u0206\u0207\7n\2\2\u0207")
        buf.write("\u0208\7u\2\2\u0208\u0209\7g\2\2\u0209\u0094\3\2\2\2\u020a")
        buf.write("\u020b\5\u0097L\2\u020b\u020c\7$\2\2\u020c\u0096\3\2\2")
        buf.write("\2\u020d\u0216\7$\2\2\u020e\u0215\n\2\2\2\u020f\u0212")
        buf.write("\7^\2\2\u0210\u0213\13\2\2\2\u0211\u0213\7\2\2\3\u0212")
        buf.write("\u0210\3\2\2\2\u0212\u0211\3\2\2\2\u0213\u0215\3\2\2\2")
        buf.write("\u0214\u020e\3\2\2\2\u0214\u020f\3\2\2\2\u0215\u0218\3")
        buf.write("\2\2\2\u0216\u0214\3\2\2\2\u0216\u0217\3\2\2\2\u0217\u0098")
        buf.write("\3\2\2\2\u0218\u0216\3\2\2\2\u0219\u021b\t\3\2\2\u021a")
        buf.write("\u0219\3\2\2\2\u021a\u021b\3\2\2\2\u021b\u021d\3\2\2\2")
        buf.write("\u021c\u021e\t\4\2\2\u021d\u021c\3\2\2\2\u021e\u021f\3")
        buf.write("\2\2\2\u021f\u021d\3\2\2\2\u021f\u0220\3\2\2\2\u0220\u009a")
        buf.write("\3\2\2\2\u0221\u0223\t\3\2\2\u0222\u0221\3\2\2\2\u0222")
        buf.write("\u0223\3\2\2\2\u0223\u0225\3\2\2\2\u0224\u0226\t\4\2\2")
        buf.write("\u0225\u0224\3\2\2\2\u0226\u0227\3\2\2\2\u0227\u0225\3")
        buf.write("\2\2\2\u0227\u0228\3\2\2\2\u0228\u022f\3\2\2\2\u0229\u022b")
        buf.write("\13\2\2\2\u022a\u022c\t\4\2\2\u022b\u022a\3\2\2\2\u022c")
        buf.write("\u022d\3\2\2\2\u022d\u022b\3\2\2\2\u022d\u022e\3\2\2\2")
        buf.write("\u022e\u0230\3\2\2\2\u022f\u0229\3\2\2\2\u022f\u0230\3")
        buf.write("\2\2\2\u0230\u0232\3\2\2\2\u0231\u0233\t\5\2\2\u0232\u0231")
        buf.write("\3\2\2\2\u0232\u0233\3\2\2\2\u0233\u009c\3\2\2\2\u0234")
        buf.write("\u0236\t\3\2\2\u0235\u0234\3\2\2\2\u0235\u0236\3\2\2\2")
        buf.write("\u0236\u0238\3\2\2\2\u0237\u0239\t\4\2\2\u0238\u0237\3")
        buf.write("\2\2\2\u0239\u023a\3\2\2\2\u023a\u0238\3\2\2\2\u023a\u023b")
        buf.write("\3\2\2\2\u023b\u0242\3\2\2\2\u023c\u023e\13\2\2\2\u023d")
        buf.write("\u023f\t\4\2\2\u023e\u023d\3\2\2\2\u023f\u0240\3\2\2\2")
        buf.write("\u0240\u023e\3\2\2\2\u0240\u0241\3\2\2\2\u0241\u0243\3")
        buf.write("\2\2\2\u0242\u023c\3\2\2\2\u0242\u0243\3\2\2\2\u0243\u0245")
        buf.write("\3\2\2\2\u0244\u0246\t\6\2\2\u0245\u0244\3\2\2\2\u0245")
        buf.write("\u0246\3\2\2\2\u0246\u009e\3\2\2\2\u0247\u0249\7B\2\2")
        buf.write("\u0248\u0247\3\2\2\2\u0248\u0249\3\2\2\2\u0249\u024b\3")
        buf.write("\2\2\2\u024a\u024c\t\7\2\2\u024b\u024a\3\2\2\2\u024c\u024d")
        buf.write("\3\2\2\2\u024d\u024b\3\2\2\2\u024d\u024e\3\2\2\2\u024e")
        buf.write("\u00a0\3\2\2\2\25\2\u00fc\u0212\u0214\u0216\u021a\u021f")
        buf.write("\u0222\u0227\u022d\u022f\u0232\u0235\u023a\u0240\u0242")
        buf.write("\u0245\u0248\u024d\3\2\3\2")
        return buf.getvalue()


class CSharpLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    T__20 = 21
    T__21 = 22
    T__22 = 23
    T__23 = 24
    T__24 = 25
    T__25 = 26
    T__26 = 27
    T__27 = 28
    T__28 = 29
    T__29 = 30
    T__30 = 31
    T__31 = 32
    T__32 = 33
    T__33 = 34
    WHITESPACES = 35
    CLASS = 36
    DELEGATE = 37
    OPERATOR = 38
    ENUM = 39
    IN = 40
    OUT = 41
    STRUCT = 42
    NAMESPACE = 43
    INTERFACE = 44
    EVENT = 45
    NEW = 46
    PUBLIC = 47
    PROTECTED = 48
    INTERNAL = 49
    PRIVATE = 50
    READONLY = 51
    VOLATILE = 52
    VIRTUAL = 53
    SEALED = 54
    OVERRIDE = 55
    ABSTRACT = 56
    STATIC = 57
    UNSAFE = 58
    EXTERN = 59
    PARTIAL = 60
    ASYNC = 61
    PARAMS = 62
    THIS = 63
    REF = 64
    WHERE = 65
    DEFAULT = 66
    CONST = 67
    IMPLICIT = 68
    EXPLICIT = 69
    SET = 70
    GET = 71
    TRUE = 72
    FALSE = 73
    STRING_LITERAL = 74
    UNTERMINATED_STRING_LITERAL = 75
    INTEGER_VALUE = 76
    DOUBLE_VALUE = 77
    FLOAT_VALUE = 78
    IDENTIFIER = 79

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "';'", "'{'", "'}'", "'='", "'('", "')'", "'['", "']'", "','", 
            "'@'", "'?'", "'[]'", "'.'", "'`'", "':'", "'new()'", "'null'", 
            "'typeof('", "'+'", "'-'", "'!'", "'++'", "'--'", "'*'", "'/'", 
            "'&'", "'|'", "'^'", "'<'", "'>'", "'=='", "'!='", "'<='", "'>='", 
            "'class'", "'delegate'", "'operator'", "'enum'", "'in'", "'out'", 
            "'struct'", "'namespace'", "'interface'", "'event'", "'new'", 
            "'public'", "'protected'", "'internal'", "'private'", "'readonly'", 
            "'volatile'", "'virtual'", "'sealed'", "'override'", "'abstract'", 
            "'static'", "'unsafe'", "'extern'", "'partial'", "'async'", 
            "'params'", "'this'", "'ref'", "'where'", "'default'", "'const'", 
            "'implicit'", "'explicit'", "'set'", "'get'", "'true'", "'false'" ]

    symbolicNames = [ "<INVALID>",
            "WHITESPACES", "CLASS", "DELEGATE", "OPERATOR", "ENUM", "IN", 
            "OUT", "STRUCT", "NAMESPACE", "INTERFACE", "EVENT", "NEW", "PUBLIC", 
            "PROTECTED", "INTERNAL", "PRIVATE", "READONLY", "VOLATILE", 
            "VIRTUAL", "SEALED", "OVERRIDE", "ABSTRACT", "STATIC", "UNSAFE", 
            "EXTERN", "PARTIAL", "ASYNC", "PARAMS", "THIS", "REF", "WHERE", 
            "DEFAULT", "CONST", "IMPLICIT", "EXPLICIT", "SET", "GET", "TRUE", 
            "FALSE", "STRING_LITERAL", "UNTERMINATED_STRING_LITERAL", "INTEGER_VALUE", 
            "DOUBLE_VALUE", "FLOAT_VALUE", "IDENTIFIER" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "T__14", "T__15", "T__16", "T__17", "T__18", "T__19", 
                  "T__20", "T__21", "T__22", "T__23", "T__24", "T__25", 
                  "T__26", "T__27", "T__28", "T__29", "T__30", "T__31", 
                  "T__32", "T__33", "WHITESPACES", "CLASS", "DELEGATE", 
                  "OPERATOR", "ENUM", "IN", "OUT", "STRUCT", "NAMESPACE", 
                  "INTERFACE", "EVENT", "NEW", "PUBLIC", "PROTECTED", "INTERNAL", 
                  "PRIVATE", "READONLY", "VOLATILE", "VIRTUAL", "SEALED", 
                  "OVERRIDE", "ABSTRACT", "STATIC", "UNSAFE", "EXTERN", 
                  "PARTIAL", "ASYNC", "PARAMS", "THIS", "REF", "WHERE", 
                  "DEFAULT", "CONST", "IMPLICIT", "EXPLICIT", "SET", "GET", 
                  "TRUE", "FALSE", "STRING_LITERAL", "UNTERMINATED_STRING_LITERAL", 
                  "INTEGER_VALUE", "DOUBLE_VALUE", "FLOAT_VALUE", "IDENTIFIER" ]

    grammarFileName = "CSharp.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


